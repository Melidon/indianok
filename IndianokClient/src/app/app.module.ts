import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppComponent } from './app.component';

import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { HomeComponent } from './components/home/home.component';
import { RegisterComponent } from './components/register/register.component';
import { SignInComponent } from './components/sign-in/sign-in.component';
import { ProfileComponent } from './components/profile/profile.component';
import { ParentComponent } from './components/parent/parent.component';
import { LocationsComponent } from './components/locations/locations.component';
import { LocationDialog } from './components/locations/location-doalog';
import { SessionsComponent } from './components/sessions/sessions.component';
import { SessionDialog } from './components/sessions/session-dialog';
import { ChildrenComponent } from './components/children/children.component';
import { ChildDialog } from './components/children/child-dialog';
import { BookingsComponent } from './components/bookings/bookings.component';
import { BookingComponent } from './components/booking/booking.component';
import { BookingDialog } from './components/booking/booking-dialog';
import { SessionComponent } from './components/session/session.component';

import { LayoutModule } from '@angular/cdk/layout';

import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTableModule } from '@angular/material/table';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatStepperModule } from '@angular/material/stepper';

import { initializeApp, provideFirebaseApp } from '@angular/fire/app';
import { environment } from '../environments/environment';
import { provideAuth, getAuth } from '@angular/fire/auth';
import { provideFirestore, getFirestore } from '@angular/fire/firestore';

import { IAuthService } from './services/i-auth.service';
import { IBookingsService } from './services/i-bookings.service';
import { IChildrenService } from './services/i-children.service';
import { ILocationsService } from './services/i-locations.service';
import { IParentsService } from './services/i-parents.service';
import { ISessionsService } from './services/i-sessions.service';

import { DotnetAuthInterceptor } from './interceptors/dotnet-auth.interceptor';
import { DotnetAuthService } from './services/dotnet-auth.service';
import { DotnetBookingsService } from './services/dotnet-bookings.service';
import { DotnetChildrenService } from './services/dotnet-children.service';
import { DotnetLocationsService } from './services/dotnet-locations.service';
import { DotnetParentsService } from './services/dotnet-parents.service';
import { DotnetSessionsService } from './services/dotnet-sessions.service';

import { FirebaseAuthService } from './services/firebase-auth.service';
import { FirebaseBookingsService } from './services/firebase-bookings.service';
import { FirebaseChildrenService } from './services/firebase-children.service';
import { FirebaseLocationsService } from './services/firebase-locations.service';
import { FirebaseParentsService } from './services/firebase-parents.service';
import { FirebaseSessionsService } from './services/firebase-sessions.service';

@NgModule({
  declarations: [
    AppComponent,

    PageNotFoundComponent,
    HomeComponent,
    SignInComponent,
    RegisterComponent,
    ProfileComponent,
    ParentComponent,
    LocationsComponent,
    LocationDialog,
    SessionsComponent,
    SessionDialog,
    ChildrenComponent,
    ChildDialog,
    BookingsComponent,
    BookingComponent,
    BookingDialog,
    SessionComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,

    LayoutModule,

    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatSidenavModule,
    MatListModule,
    MatInputModule,
    MatFormFieldModule,
    MatSnackBarModule,
    MatTableModule,
    MatDialogModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatCardModule,
    MatCheckboxModule,
    MatStepperModule,

    provideFirebaseApp(() => initializeApp(environment.firebase)),
    provideAuth(() => getAuth()),
    provideFirestore(() => getFirestore()),
  ],
  providers: [
    /*
    {
      provide: HTTP_INTERCEPTORS,
      useClass: DotnetAuthInterceptor,
      multi: true,
    },
    { provide: IAuthService, useExisting: DotnetAuthService },
    { provide: IParentsService, useExisting: DotnetParentsService },
    { provide: ILocationsService, useExisting: DotnetLocationsService },
    { provide: ISessionsService, useExisting: DotnetSessionsService },
    { provide: IChildrenService, useExisting: DotnetChildrenService },
    { provide: IBookingsService, useExisting: DotnetBookingsService },
    */
    { provide: IAuthService, useExisting: FirebaseAuthService },
    { provide: IParentsService, useExisting: FirebaseParentsService },
    { provide: ILocationsService, useExisting: FirebaseLocationsService },
    { provide: ISessionsService, useExisting: FirebaseSessionsService },
    { provide: IChildrenService, useExisting: FirebaseChildrenService },
    { provide: IBookingsService, useExisting: FirebaseBookingsService },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
