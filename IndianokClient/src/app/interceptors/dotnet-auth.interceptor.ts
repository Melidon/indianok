import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { DotnetAuthService } from '../services/dotnet-auth.service';

@Injectable()
export class DotnetAuthInterceptor implements HttpInterceptor {
  constructor(private dotnetAuthService: DotnetAuthService) {}

  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler
  ): Observable<HttpEvent<unknown>> {
    const authToken = this.dotnetAuthService.getAuthToken();
    if (authToken !== null) {
      request = request.clone({
        headers: request.headers.set(
          'Authorization',
          `Bearer ${authToken.accessToken}`
        ),
      });
    }
    return next.handle(request);
  }
}
