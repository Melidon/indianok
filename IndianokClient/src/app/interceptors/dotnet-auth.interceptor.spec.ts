import { TestBed } from '@angular/core/testing';

import { DotnetAuthInterceptor } from './dotnet-auth.interceptor';

describe('DotnetAuthInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      DotnetAuthInterceptor
      ]
  }));

  it('should be created', () => {
    const interceptor: DotnetAuthInterceptor = TestBed.inject(DotnetAuthInterceptor);
    expect(interceptor).toBeTruthy();
  });
});
