import { Component, OnInit, Inject } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Child } from 'src/app/services/generated-dotnet.service';

@Component({
  selector: 'child-dialog',
  templateUrl: './child-dialog.html',
  styleUrls: ['./child-dialog.scss'],
})
export class ChildDialog implements OnInit {
  form = new FormGroup({
    name: new FormControl<string>(undefined!, [Validators.required]),
    dateOfBirth: new FormControl<Date>(undefined!, [Validators.required]),
    socialSecurityNumber: new FormControl<number>(undefined!, [
      Validators.required,
    ]),
    hasBeenAloneInCamp: new FormControl<boolean>(false),
    knowsSomeoneFromTheCamp: new FormControl<boolean>(false),
    additionalPersonalInformation: new FormControl<string>(''),
  });

  constructor(
    private dialogRef: MatDialogRef<ChildDialog>,
    @Inject(MAT_DIALOG_DATA) public child: Child | undefined
  ) {}

  async ngOnInit(): Promise<void> {
    if (this.child) {
      this.form.setValue(this.child);
    }
  }

  cancel(): void {
    this.dialogRef.close();
  }
}
