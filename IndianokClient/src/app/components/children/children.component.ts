import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { firstValueFrom } from 'rxjs';
import {
  Child,
  ChildDocument,
} from 'src/app/services/generated-dotnet.service';
import { IAuthService } from 'src/app/services/i-auth.service';
import { IChildrenService } from 'src/app/services/i-children.service';
import { ChildDialog } from './child-dialog';

@Component({
  selector: 'app-children',
  templateUrl: './children.component.html',
  styleUrls: ['./children.component.scss'],
})
export class ChildrenComponent implements OnInit {
  parentId!: string;
  children!: ChildDocument[];

  constructor(
    private router: Router,
    private dialog: MatDialog,
    private authService: IAuthService,
    private childrenService: IChildrenService
  ) {}

  async ngOnInit(): Promise<void> {
    const isSignedIn = await firstValueFrom(this.authService.isSignedIn());
    if (!isSignedIn) {
      this.router.navigate(['sign-in']);
    }
    this.parentId = this.authService.getUid()!;
    this.children = await this.childrenService.listChildren(this.parentId);
  }

  async add(): Promise<void> {
    const dialogRef = this.dialog.open<ChildDialog, undefined, Child>(
      ChildDialog
    );
    const createdChild = await firstValueFrom(dialogRef.afterClosed());
    if (!createdChild) {
      return;
    }
    try {
      const childDocument = await this.childrenService.createChild(
        this.parentId,
        createdChild
      );
      this.children.push(childDocument);
    } catch (error) {
      console.error(error);
    }
  }

  async edit(child: ChildDocument): Promise<void> {
    const dialogRef = this.dialog.open<ChildDialog, Child, Child>(ChildDialog, {
      data: child.data,
    });
    const editedChild = await firstValueFrom(dialogRef.afterClosed());
    if (!editedChild) {
      return;
    }
    try {
      await this.childrenService.updateChild(
        this.parentId,
        child.id,
        editedChild
      );
      const index = this.children.findIndex((it) => it.id === child.id);
      this.children[index].data = editedChild;
    } catch (error) {
      console.error(error);
    }
  }

  async delete(id: string): Promise<void> {
    try {
      await this.childrenService.deleteChild(this.parentId, id);
      const index = this.children.findIndex((child) => child.id == id);
      this.children.splice(index, 1);
    } catch (error) {
      console.error(error);
    }
  }
}
