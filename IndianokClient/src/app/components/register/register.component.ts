import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormControl,
  Validators,
  ValidatorFn,
  ValidationErrors,
  AbstractControl,
} from '@angular/forms';
import { Router } from '@angular/router';
import { firstValueFrom } from 'rxjs';
import { IAuthService } from 'src/app/services/i-auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  passwordValidator: ValidatorFn = (
    control: AbstractControl
  ): ValidationErrors | null => {
    const password: string = control.value;
    if (!password || password.length < 6) {
      return { password: true };
    }
    return null;
  };

  confirmPasswordValidator: ValidatorFn = (
    control: AbstractControl
  ): ValidationErrors | null => {
    const password: string = control.parent?.get('password')?.value;
    const confirmPassword: string = control.value;
    if (password !== confirmPassword) {
      return { confirmPassword: true };
    }
    return null;
  };

  form = new FormGroup({
    email: new FormControl<string>(undefined!, [
      Validators.required,
      Validators.email,
    ]),
    password: new FormControl<string>(undefined!, [
      Validators.required,
      this.passwordValidator,
    ]),
    confirmPassword: new FormControl<string>(undefined!, [
      Validators.required,
      this.confirmPasswordValidator,
    ]),
  });

  hidePassword = true;

  createUserFailed = false;

  constructor(private router: Router, private authService: IAuthService) {}

  async ngOnInit(): Promise<void> {
    const isSignedIn = await firstValueFrom(this.authService.isSignedIn());
    if (isSignedIn) {
      this.onSuccess();
    }
  }

  onSuccess(): void {
    this.router.navigate(['profile']);
  }

  async submit() {
    const rawValue = this.form.getRawValue();
    const sucess = await this.authService.createUserWithEmailAndPassword(
      rawValue.email!,
      rawValue.password!
    );
    if (sucess) {
      this.onSuccess();
    } else {
      this.createUserFailed = true;
    }
  }
}
