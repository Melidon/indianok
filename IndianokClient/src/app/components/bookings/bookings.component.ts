import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { StepperOrientation } from '@angular/material/stepper';
import { Router } from '@angular/router';
import { map, Observable, shareReplay } from 'rxjs';
import { IAuthService } from 'src/app/services/i-auth.service';
import { IBookingsService } from 'src/app/services/i-bookings.service';

@Component({
  selector: 'app-bookings',
  templateUrl: './bookings.component.html',
  styleUrls: ['./bookings.component.scss'],
})
export class BookingsComponent implements OnInit {
  stepperOrientation: Observable<StepperOrientation> = this.breakpointObserver
    .observe(Breakpoints.XSmall)
    .pipe(map((result) => (result.matches ? 'vertical' : 'horizontal')));

  constructor(
    private breakpointObserver: BreakpointObserver,
    private router: Router,
    private dialog: MatDialog,
    private authService: IAuthService,
    private bookingsService: IBookingsService
  ) {}

  ngOnInit(): void {}

  isSignedIn = this.authService.isSignedIn();
}
