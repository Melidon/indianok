import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { firstValueFrom } from 'rxjs';
import { IAuthService } from 'src/app/services/i-auth.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit {
  constructor(private router: Router, private authService: IAuthService) {}

  async ngOnInit(): Promise<void> {
    const isSignedIn = await firstValueFrom(this.authService.isSignedIn());
    if (!isSignedIn) {
      this.router.navigate(['sign-in']);
      return;
    }
  }

  signOut(): void {
    this.authService.signOut();
    this.router.navigate(['home']);
  }

  async delete(): Promise<void> {
    try {
      await this.authService.delete();
    } catch (error) {
      console.error(error);
    }
    this.router.navigate(['home']);
  }
}
