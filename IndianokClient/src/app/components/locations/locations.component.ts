import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTable } from '@angular/material/table';
import { Router } from '@angular/router';
import { firstValueFrom } from 'rxjs';
import {
  Location,
  LocationDocument,
} from 'src/app/services/generated-dotnet.service';
import { IAuthService } from 'src/app/services/i-auth.service';
import { ILocationsService } from 'src/app/services/i-locations.service';
import { LocationDialog } from './location-doalog';

@Component({
  selector: 'app-locations',
  templateUrl: './locations.component.html',
  styleUrls: ['./locations.component.scss'],
})
export class LocationsComponent implements OnInit {
  displayedColumns: string[] = ['name', 'latitude', 'longitude', 'actions'];

  locations!: LocationDocument[];

  @ViewChild(MatTable) table!: MatTable<LocationDocument>;

  constructor(
    private router: Router,
    private dialog: MatDialog,
    private authService: IAuthService,
    private locationsService: ILocationsService
  ) {}

  async ngOnInit(): Promise<void> {
    const isAdmin = await firstValueFrom(this.authService.isAdmin());
    if (!isAdmin) {
      this.router.navigate(['sign-in']);
    }
    try {
      this.locations = await this.locationsService.listLocations();
    } catch (error) {
      console.error(error);
    }
  }

  async add(): Promise<void> {
    const dialogRef = this.dialog.open<LocationDialog, undefined, Location>(
      LocationDialog
    );
    const location = await firstValueFrom(dialogRef.afterClosed());
    if (!location) {
      return;
    }
    try {
      const locationDocument = await this.locationsService.createLocation(
        location
      );
      this.locations.push(locationDocument);
      this.table.renderRows();
    } catch (error) {
      console.error(error);
    }
  }

  async edit(location: LocationDocument): Promise<void> {
    const dialogRef = this.dialog.open<LocationDialog, Location, Location>(
      LocationDialog,
      { data: location.data }
    );
    const editedLocation = await firstValueFrom(dialogRef.afterClosed());
    if (!editedLocation) {
      return;
    }
    try {
      await this.locationsService.updateLocation(location.id, editedLocation);
      const index = this.locations.findIndex((it) => it.id === location.id);
      this.locations[index].data = editedLocation;
      this.table.renderRows();
    } catch (error) {
      console.error(error);
    }
  }

  async delete(id: string): Promise<void> {
    try {
      await this.locationsService.deleteLocation(id);
      const index = this.locations.findIndex((it) => it.id == id);
      this.locations.splice(index, 1);
      this.table.renderRows();
    } catch (error) {
      console.error(error);
    }
  }
}
