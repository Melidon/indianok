import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Location } from 'src/app/services/generated-dotnet.service';

@Component({
  selector: 'location-dialog',
  templateUrl: './location-dialog.html',
  styleUrls: ['./location-dialog.scss'],
})
export class LocationDialog implements OnInit {
  readonly latitudeMin = -90;
  readonly latitudeMax = 90;
  readonly longitudeMin = -180;
  readonly longitudeMax = 180;

  form = new FormGroup({
    name: new FormControl<string>(undefined!, [Validators.required]),
    latitude: new FormControl<number>(undefined!, [
      Validators.required,
      Validators.min(this.latitudeMin),
      Validators.max(this.latitudeMax),
    ]),
    longitude: new FormControl<number>(undefined!, [
      Validators.required,
      Validators.min(this.longitudeMin),
      Validators.max(this.longitudeMax),
    ]),
  });

  constructor(
    private dialogRef: MatDialogRef<LocationDialog>,
    @Inject(MAT_DIALOG_DATA) public location: Location | undefined
  ) {}

  ngOnInit(): void {
    if (this.location) {
      this.form.setValue(this.location);
    }
  }

  cancel(): void {
    this.dialogRef.close();
  }
}
