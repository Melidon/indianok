import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import {
  ChildDocument,
  Location,
  LocationDocument,
  SessionDocument,
  CampType,
  PaymentStatus,
  BookingDocument,
} from 'src/app/services/generated-dotnet.service';
import { IAuthService } from 'src/app/services/i-auth.service';
import { IChildrenService } from 'src/app/services/i-children.service';
import { ILocationsService } from 'src/app/services/i-locations.service';
import { ISessionsService } from 'src/app/services/i-sessions.service';

export interface Booking {
  parentId: string;
  childId: string;
  campType: CampType;
  paymentStatus: PaymentStatus;
}

@Component({
  selector: 'booking-dialog',
  templateUrl: './booking-dialog.html',
  styleUrls: ['./booking-dialog.scss'],
})
export class BookingDialog implements OnInit {
  form = new FormGroup({
    sessionId: new FormControl<string>(undefined!, [Validators.required]),
    childId: new FormControl<string>(undefined!, [Validators.required]),
    campType: new FormControl<CampType>(undefined!, [Validators.required]),
  });

  parentId!: string;
  children!: ChildDocument[];
  locations!: LocationDocument[];
  sessions!: SessionDocument[];
  campTypes: CampType[] = Object.values(CampType);

  constructor(
    private dialogRef: MatDialogRef<BookingDialog>,
    private authService: IAuthService,
    private childrenService: IChildrenService,
    private locationsService: ILocationsService,
    private sessionsService: ISessionsService
  ) {}

  async ngOnInit(): Promise<void> {
    this.parentId = this.authService.getUid()!;
    const children = this.childrenService
      .listChildren(this.parentId)
      .then((children) => (this.children = children));
    const locations = this.locationsService
      .listLocations()
      .then((locations) => (this.locations = locations));
    const sessions = this.sessionsService
      .listSessions()
      .then((sessions) => (this.sessions = sessions));
    await Promise.all([children, locations, sessions]);
  }

  getLocation(id: string): Location {
    const location = this.locations.find((location) => location.id == id)!;
    return location.data;
  }

  cancel(): void {
    this.dialogRef.close();
  }

  save(): void {
    const booking: BookingDocument = {
      id: undefined!,
      parentDocumentId: this.form.controls.sessionId.value!,
      data: {
        parentId: this.parentId,
        childId: this.form.controls.childId.value!,
        campType: this.form.controls.campType.value!,
        paymentStatus: PaymentStatus.NotPaid,
      },
    };
    this.dialogRef.close(booking);
  }
}
