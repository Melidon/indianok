import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { firstValueFrom } from 'rxjs';
import {
  BookingDocument,
  Child,
  ChildDocument,
  Location,
  LocationDocument,
  Session,
  SessionDocument,
} from 'src/app/services/generated-dotnet.service';
import { IAuthService } from 'src/app/services/i-auth.service';
import { IBookingsService } from 'src/app/services/i-bookings.service';
import { IChildrenService } from 'src/app/services/i-children.service';
import { ILocationsService } from 'src/app/services/i-locations.service';
import { ISessionsService } from 'src/app/services/i-sessions.service';
import { BookingDialog } from './booking-dialog';

@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.scss'],
})
export class BookingComponent implements OnInit {
  bookings!: BookingDocument[];
  children!: ChildDocument[];
  locations!: LocationDocument[];
  sessions!: SessionDocument[];

  constructor(
    private router: Router,
    private dialog: MatDialog,
    private authService: IAuthService,
    private bookingsService: IBookingsService,
    private childrenService: IChildrenService,
    private locationsService: ILocationsService,
    private sessionsService: ISessionsService
  ) {}

  async ngOnInit(): Promise<void> {
    const isSignedIn = await firstValueFrom(this.authService.isSignedIn());
    if (!isSignedIn) {
      this.router.navigate(['sign-in']);
    }
    const parentId = this.authService.getUid()!;
    const bookings = this.bookingsService
      .listBookingsForParent(parentId)
      .then((bookings) => (this.bookings = bookings));
    const children = this.childrenService
      .listChildren(parentId)
      .then((children) => (this.children = children));
    const locations = this.locationsService
      .listLocations()
      .then((locations) => (this.locations = locations));
    const sessions = this.sessionsService
      .listSessions()
      .then((sessions) => (this.sessions = sessions));
    await Promise.all([bookings, children, locations, sessions]);
  }

  getChild(id: string): Child {
    const child = this.children.find((child) => child.id == id)!;
    return child.data;
  }

  getLocation(id: string): Location {
    const location = this.locations.find((location) => location.id == id)!;
    return location.data;
  }

  getSession(id: string): Session {
    const session = this.sessions.find((session) => session.id == id)!;
    return session.data;
  }

  async add(): Promise<void> {
    const dialogRef = this.dialog.open<
      BookingDialog,
      undefined,
      BookingDocument
    >(BookingDialog);
    const booking = await firstValueFrom(dialogRef.afterClosed());
    if (!booking) {
      return;
    }
    try {
      const bookingDocument = await this.bookingsService.createBooking(
        booking.parentDocumentId!,
        booking.data
      );
      this.bookings.push(bookingDocument);
    } catch (error) {
      console.error(error);
    }
  }

  async delete(sessionId: string, id: string): Promise<void> {
    try {
      await this.bookingsService.deleteBooking(sessionId, id);
      const index = this.bookings.findIndex((booking) => booking.id == id);
      this.bookings.splice(index, 1);
    } catch (error) {
      console.error(error);
    }
  }
}
