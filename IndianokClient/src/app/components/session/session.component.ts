import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { firstValueFrom } from 'rxjs';
import {
  BookingDocument,
  Child,
  ChildDocument,
  Parent,
  ParentDocument,
} from 'src/app/services/generated-dotnet.service';
import { IAuthService } from 'src/app/services/i-auth.service';
import { IBookingsService } from 'src/app/services/i-bookings.service';
import { IChildrenService } from 'src/app/services/i-children.service';
import { IParentsService } from 'src/app/services/i-parents.service';

@Component({
  selector: 'app-session',
  templateUrl: './session.component.html',
  styleUrls: ['./session.component.scss'],
})
export class SessionComponent implements OnInit {
  displayedColumns: string[] = [
    'name',
    'dateOfBirth',
    'socialSecurityNumber',
    'hasBeenAloneInCamp',
    'knowsSomeoneFromTheCamp',
    'additionalPersonalInformation',
    'parentName',
    'parentEmail',
    'parentPhone',
    'address',
    'campType',
    'paymentStatus',
  ];

  bookings!: BookingDocument[];
  children: ChildDocument[] = [];
  parents: ParentDocument[] = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authService: IAuthService,
    private bookingsService: IBookingsService,
    private childrenService: IChildrenService,
    private parentsService: IParentsService
  ) {}

  async ngOnInit(): Promise<void> {
    const isAdmin = await firstValueFrom(this.authService.isAdmin());
    if (!isAdmin) {
      this.router.navigate(['sign-in']);
    }
    this.route.params.subscribe(async (params) => {
      const id = params['id'];
      this.bookings = await this.bookingsService.listBookings(id);
      let promises: Promise<any>[] = [];
      for (let booking of this.bookings) {
        const promise = this.childrenService
          .getChild(booking.data.parentId, booking.data.childId)
          .then((child) => this.children.push(child!));
        promises.push(promise);
      }
      for (let booking of this.bookings) {
        const promise = this.parentsService
          .getParent(booking.data.parentId)
          .then((parent) => this.parents.push(parent!));
        promises.push(promise);
      }
      await Promise.all(promises);
    });
  }

  getChild(parentId: string, id: string): Child | undefined {
    const child = this.children.find(
      (child) => child.parentDocumentId === parentId && child.id === id
    );
    return child?.data;
  }

  log(a: any) {
    console.log(a);
  }

  getParent(id: string): Parent | undefined {
    const parent = this.parents.find((parent) => parent.id === id);
    return parent?.data;
  }
}
