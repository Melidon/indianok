import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { firstValueFrom } from 'rxjs';
import { Parent } from 'src/app/services/generated-dotnet.service';
import { IAuthService } from 'src/app/services/i-auth.service';
import { IParentsService } from 'src/app/services/i-parents.service';

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.scss'],
})
export class ParentComponent implements OnInit {
  form = new FormGroup({
    email: new FormControl<string>({ value: undefined!, disabled: true }, [
      Validators.required,
      Validators.email,
    ]),
    name: new FormControl<string>(undefined!, [Validators.required]),
    address: new FormControl<string>(undefined!, [Validators.required]),
    phone: new FormControl<string>(undefined!, [Validators.required]),
  });

  constructor(
    private router: Router,
    private snackBar: MatSnackBar,
    private authService: IAuthService,
    private parentsService: IParentsService
  ) {}

  async ngOnInit(): Promise<void> {
    const isSignedIn = await firstValueFrom(this.authService.isSignedIn());
    if (!isSignedIn) {
      this.router.navigate(['sign-in']);
      return;
    }
    const uid = this.authService.getUid()!;
    try {
      const parent = await this.parentsService.getParent(uid);
      if (parent === null) {
        return;
      }
      this.form.setValue(parent.data);
    } catch (error) {
      console.error(error);
    }
  }

  async save(): Promise<void> {
    const uid = this.authService.getUid()!;
    const parent = this.form.getRawValue() as Parent;
    try {
      await this.parentsService.updateParent(uid, parent);
    } catch (error) {
      console.error(error);
    }
    this.snackBar.open('Changes have been saved', 'Ok', { duration: 3000 });
  }
}
