import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { firstValueFrom } from 'rxjs';
import { IAuthService } from 'src/app/services/i-auth.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss'],
})
export class SignInComponent implements OnInit {
  form = new FormGroup({
    email: new FormControl<string>(undefined!, [
      Validators.required,
      Validators.email,
    ]),
    password: new FormControl<string>(undefined!, [Validators.required]),
  });

  hidePassword = true;

  signInFailed = false;

  constructor(private router: Router, private authService: IAuthService) {}

  async ngOnInit(): Promise<void> {
    const isSignedIn = await firstValueFrom(this.authService.isSignedIn());
    if (isSignedIn) {
      this.onSuccess();
    }
  }

  onSuccess(): void {
    this.router.navigate(['profile']);
  }

  async submit(): Promise<void> {
    const rawValue = this.form.getRawValue();
    const sucess = await this.authService.signInWithEmailAndPassword(
      rawValue.email!,
      rawValue.password!
    );
    if (sucess) {
      this.onSuccess();
    } else {
      this.signInFailed = true;
    }
  }
}
