import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { firstValueFrom } from 'rxjs';
import {
  Location,
  LocationDocument,
  Session,
  SessionDocument,
} from 'src/app/services/generated-dotnet.service';
import { IAuthService } from 'src/app/services/i-auth.service';
import { ILocationsService } from 'src/app/services/i-locations.service';
import { ISessionsService } from 'src/app/services/i-sessions.service';
import { SessionDialog } from './session-dialog';

@Component({
  selector: 'app-sessions',
  templateUrl: './sessions.component.html',
  styleUrls: ['./sessions.component.scss'],
})
export class SessionsComponent implements OnInit {
  locations!: LocationDocument[];
  sessions!: SessionDocument[];

  constructor(
    private router: Router,
    private dialog: MatDialog,
    private authService: IAuthService,
    private locationsService: ILocationsService,
    private sessionsService: ISessionsService
  ) {}

  async ngOnInit(): Promise<void> {
    const isAdmin = await firstValueFrom(this.authService.isAdmin());
    if (!isAdmin) {
      this.router.navigate(['sign-in']);
    }
    const locations = this.locationsService
      .listLocations()
      .then((locations) => (this.locations = locations));
    const sessions = this.sessionsService
      .listSessions()
      .then((sessions) => (this.sessions = sessions));
    await Promise.all([locations, sessions]);
  }

  async add(): Promise<void> {
    const dialogRef = this.dialog.open<SessionDialog, undefined, Session>(
      SessionDialog
    );
    const session = await firstValueFrom(dialogRef.afterClosed());
    if (!session) {
      return;
    }
    try {
      const sessionDocument = await this.sessionsService.createSession(session);
      this.sessions.push(sessionDocument);
    } catch (error) {
      console.error(error);
    }
  }

  async edit(session: SessionDocument): Promise<void> {
    const dialogRef = this.dialog.open<SessionDialog, Session, Session>(
      SessionDialog,
      { data: session.data }
    );
    const editedSession = await firstValueFrom(dialogRef.afterClosed());
    if (!editedSession) {
      return;
    }
    try {
      await this.sessionsService.updateSession(session.id, editedSession);
      const index = this.sessions.findIndex((it) => it.id === session.id);
      this.sessions[index].data = editedSession;
    } catch (error) {
      console.error(error);
    }
  }

  async delete(id: string): Promise<void> {
    try {
      await this.sessionsService.deleteSession(id);
      const index = this.sessions.findIndex((it) => it.id == id);
      this.sessions.splice(index, 1);
    } catch (error) {
      console.error(error);
    }
  }

  getLocation(id: string): Location {
    const location = this.locations.find((location) => location.id == id)!;
    return location.data;
  }
}
