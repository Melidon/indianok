import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {
  LocationDocument,
  Session,
} from 'src/app/services/generated-dotnet.service';
import { ILocationsService } from 'src/app/services/i-locations.service';

@Component({
  selector: 'session-dialog',
  templateUrl: './session-dialog.html',
  styleUrls: ['./session-dialog.scss'],
})
export class SessionDialog implements OnInit {
  form = new FormGroup({
    locationId: new FormControl<string>(undefined!, [Validators.required]),
    priceForDayCamp: new FormControl<number>(undefined!, [
      Validators.required,
      Validators.min(0),
    ]),
    priceForOvernightCamp: new FormControl<number>(undefined!, [
      Validators.required,
      Validators.min(0),
    ]),
    advance: new FormControl<number>(undefined!, [
      Validators.required,
      Validators.min(0),
    ]),
    siblingDiscount: new FormControl<number>(undefined!, [
      Validators.required,
      Validators.min(0),
    ]),
    range: new FormGroup({
      start: new FormControl<Date>(undefined!, [Validators.required]),
      end: new FormControl<Date>(undefined!, [Validators.required]),
    }),
  });

  locations!: LocationDocument[];

  constructor(
    private dialogRef: MatDialogRef<SessionDialog>,
    @Inject(MAT_DIALOG_DATA) public session: Session | undefined,
    private locationsService: ILocationsService
  ) {}

  async ngOnInit(): Promise<void> {
    if (this.session) {
      this.form.setValue({
        locationId: this.session.locationId,
        priceForDayCamp: this.session.priceForDayCamp,
        priceForOvernightCamp: this.session.priceForOvernightCamp,
        advance: this.session.advance,
        siblingDiscount: this.session.siblingDiscount,
        range: {
          start: this.session.startDate,
          end: this.session.endDate,
        },
      });
    }
    this.locations = await this.locationsService.listLocations();
  }

  cancel(): void {
    this.dialogRef.close();
  }

  save(): void {
    const rawValue = this.form.getRawValue();
    const result: Session = {
      locationId: rawValue.locationId!,
      priceForDayCamp: rawValue.priceForDayCamp!,
      priceForOvernightCamp: rawValue.priceForOvernightCamp!,
      advance: rawValue.advance!,
      siblingDiscount: rawValue.siblingDiscount!,
      startDate: rawValue.range.start!,
      endDate: rawValue.range.end!,
    };
    this.dialogRef.close(result);
  }
}
