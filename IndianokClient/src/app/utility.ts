import { Timestamp } from '@angular/fire/firestore';

export class Utility {
  public static fixDateForDotnet(date: string): Date {
    const fixed = new Date(date);
    const offset = fixed.getTimezoneOffset();
    fixed.setMinutes(fixed.getMinutes() - offset);
    return fixed;
  }

  public static fixDateForFirebase(date: Timestamp): Date {
    return date.toDate();
  }
}
