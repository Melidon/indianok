import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './components/home/home.component';
import { RegisterComponent } from './components/register/register.component';
import { SignInComponent } from './components/sign-in/sign-in.component';
import { ProfileComponent } from './components/profile/profile.component';
import { LocationsComponent } from './components/locations/locations.component';
import { SessionsComponent } from './components/sessions/sessions.component';
import { ChildrenComponent } from './components/children/children.component';
import { BookingsComponent } from './components/bookings/bookings.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { SessionComponent } from './components/session/session.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'sign-in', component: SignInComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'locations', component: LocationsComponent }, // TODO: AdminGuard
  { path: 'sessions', component: SessionsComponent }, // TODO: AdminGuard
  { path: 'sessions/:id', component: SessionComponent }, // TODO: AdminGuard
  { path: 'children', component: ChildrenComponent }, // TODO: AuthGuard
  { path: 'bookings', component: BookingsComponent }, // TODO: AuthGuard
  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
