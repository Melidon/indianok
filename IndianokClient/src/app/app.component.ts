import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, map, shareReplay } from 'rxjs';
import { IAuthService } from './services/i-auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  isSmall$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.XSmall)
    .pipe(map((result) => result.matches));

  url$ = this.router.events.pipe(
    map(() => this.router.url.split('?')[0]),
    shareReplay()
  );

  constructor(
    private router: Router,
    private breakpointObserver: BreakpointObserver,
    private authService: IAuthService
  ) {}

  ngOnInit(): void {}

  isSignedIn = this.authService.isSignedIn();

  isAdmin = this.authService.isAdmin();
}
