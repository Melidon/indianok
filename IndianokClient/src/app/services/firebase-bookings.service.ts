import { Injectable } from '@angular/core';
import {
  addDoc,
  collection,
  collectionGroup,
  CollectionReference,
  deleteDoc,
  doc,
  DocumentReference,
  Firestore,
  getDoc,
  getDocs,
  Query,
  query,
  updateDoc,
  where,
} from '@angular/fire/firestore';
import { BookingDocument, Booking } from './generated-dotnet.service';
import { IBookingsService } from './i-bookings.service';

@Injectable({
  providedIn: 'root',
})
export class FirebaseBookingsService implements IBookingsService {
  private static readonly sessions = 'sessions';
  private static readonly bookings = 'bookings';

  constructor(private firestore: Firestore) {}

  public async getBooking(
    sessionId: string,
    id: string
  ): Promise<BookingDocument | null> {
    const bookingDocumentReference = doc(
      this.firestore,
      `${FirebaseBookingsService.sessions}/${sessionId}/${FirebaseBookingsService.bookings}/${id}`
    ) as DocumentReference<Booking>;
    const bookingDocumentSnapshot = await getDoc(bookingDocumentReference);
    if (!bookingDocumentSnapshot.exists()) {
      return null;
    }
    const bookingDocument: BookingDocument = {
      id: bookingDocumentSnapshot.id,
      parentDocumentId: bookingDocumentSnapshot.ref.parent.parent!.id,
      data: bookingDocumentSnapshot.data(),
    };
    return bookingDocument;
  }

  public async listBookings(sessionId: string): Promise<BookingDocument[]> {
    const bookingsCollectionReference = collection(
      this.firestore,
      `${FirebaseBookingsService.sessions}/${sessionId}/${FirebaseBookingsService.bookings}`
    ) as CollectionReference<Booking>;
    const bookingDocumentSnapshots = await getDocs(bookingsCollectionReference);
    const bookingDocuments = bookingDocumentSnapshots.docs.map(
      (bookingDocumentSnapshot): BookingDocument => {
        return {
          id: bookingDocumentSnapshot.id,
          parentDocumentId: bookingDocumentSnapshot.ref.parent.parent!.id,
          data: bookingDocumentSnapshot.data(),
        };
      }
    );
    return bookingDocuments;
  }

  public async listBookingsForParent(
    parentId: string
  ): Promise<BookingDocument[]> {
    const bookingsCollection = collectionGroup(
      this.firestore,
      FirebaseBookingsService.bookings
    ) as Query<Booking>;
    const bookingsQuery = query(
      bookingsCollection,
      where('parentId', '==', parentId)
    );
    const bookingDocumentSnapshots = await getDocs(bookingsQuery);
    const bookingDocuments = bookingDocumentSnapshots.docs.map(
      (bookingDocumentSnapshot): BookingDocument => {
        return {
          id: bookingDocumentSnapshot.id,
          parentDocumentId: bookingDocumentSnapshot.ref.parent.parent!.id,
          data: bookingDocumentSnapshot.data(),
        };
      }
    );
    return bookingDocuments;
  }

  public async createBooking(
    sessionId: string,
    booking: Booking
  ): Promise<BookingDocument> {
    const bookingsCollectionReference = collection(
      this.firestore,
      `${FirebaseBookingsService.sessions}/${sessionId}/${FirebaseBookingsService.bookings}`
    ) as CollectionReference<Booking>;
    const bookingDocumentReference = await addDoc(
      bookingsCollectionReference,
      booking
    );
    const bookingDocument: BookingDocument = {
      id: bookingDocumentReference.id,
      parentDocumentId: sessionId,
      data: booking,
    };
    return bookingDocument;
  }

  public async updateBooking(
    sessionId: string,
    id: string,
    booking: Booking
  ): Promise<void> {
    const bookingDocumentReference = doc(
      this.firestore,
      `${FirebaseBookingsService.sessions}/${sessionId}/${FirebaseBookingsService.bookings}/${id}`
    ) as DocumentReference<Booking>;
    await updateDoc(bookingDocumentReference, booking);
  }

  public async deleteBooking(sessionId: string, id: string): Promise<void> {
    const bookingDocumentReference = doc(
      this.firestore,
      `${FirebaseBookingsService.sessions}/${sessionId}/${FirebaseBookingsService.bookings}/${id}`
    ) as DocumentReference<Booking>;
    await deleteDoc(bookingDocumentReference);
  }
}
