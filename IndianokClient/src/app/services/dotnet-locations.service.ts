import { Injectable } from '@angular/core';
import { firstValueFrom } from 'rxjs';
import {
  GeneratedDotnetLocationsService,
  LocationDocument,
  Location,
} from './generated-dotnet.service';
import { ILocationsService } from './i-locations.service';

@Injectable({
  providedIn: 'root',
})
export class DotnetLocationsService implements ILocationsService {
  constructor(
    private generatedDotnetLocationsService: GeneratedDotnetLocationsService
  ) {}

  public async getLocation(id: string): Promise<LocationDocument | null> {
    const locationDocument = await firstValueFrom(
      this.generatedDotnetLocationsService.locationsGET(id)
    );
    return locationDocument;
  }

  public async listLocations(): Promise<LocationDocument[]> {
    const locationDocuments = await firstValueFrom(
      this.generatedDotnetLocationsService.locationsAll()
    );
    return locationDocuments;
  }

  public async createLocation(location: Location): Promise<LocationDocument> {
    const locationDocument = await firstValueFrom(
      this.generatedDotnetLocationsService.locationsPOST(location)
    );
    return locationDocument;
  }

  public async updateLocation(id: string, location: Location): Promise<void> {
    await firstValueFrom(
      this.generatedDotnetLocationsService.locationsPUT(id, location)
    );
  }

  public async deleteLocation(id: string): Promise<void> {
    await firstValueFrom(
      this.generatedDotnetLocationsService.locationsDELETE(id)
    );
  }
}
