import { Injectable } from '@angular/core';
import {
  addDoc,
  collection,
  CollectionReference,
  deleteDoc,
  doc,
  DocumentReference,
  Firestore,
  getDoc,
  getDocs,
  Timestamp,
  updateDoc,
} from '@angular/fire/firestore';
import { Utility } from '../utility';
import { SessionDocument, Session } from './generated-dotnet.service';
import { ISessionsService } from './i-sessions.service';

@Injectable({
  providedIn: 'root',
})
export class FirebaseSessionsService implements ISessionsService {
  private static readonly sessions = 'sessions';

  constructor(private firestore: Firestore) {}

  public async getSession(id: string): Promise<SessionDocument | null> {
    const sessionDocumentReference = doc(
      this.firestore,
      `${FirebaseSessionsService.sessions}/${id}`
    ) as DocumentReference<Session>;
    const sessionDocumentSnapshot = await getDoc(sessionDocumentReference);
    if (!sessionDocumentSnapshot.exists()) {
      return null;
    }
    const sessionDocument: SessionDocument = {
      id: sessionDocumentSnapshot.id,
      data: sessionDocumentSnapshot.data(),
    };
    return FirebaseSessionsService.fixSessionDocument(sessionDocument);
  }

  public async listSessions(): Promise<SessionDocument[]> {
    const sessionsCollectionReference = collection(
      this.firestore,
      `${FirebaseSessionsService.sessions}`
    ) as CollectionReference<Session>;
    const sessionDocumentSnapshots = await getDocs(sessionsCollectionReference);
    const sessionDocuments = sessionDocumentSnapshots.docs.map(
      (sessionDocumentSnapshot): SessionDocument => {
        return {
          id: sessionDocumentSnapshot.id,
          data: sessionDocumentSnapshot.data(),
        };
      }
    );
    return sessionDocuments.map((sessionDocument) =>
      FirebaseSessionsService.fixSessionDocument(sessionDocument)
    );
  }

  public async createSession(session: Session): Promise<SessionDocument> {
    const sessionsCollectionReference = collection(
      this.firestore,
      `${FirebaseSessionsService.sessions}`
    ) as CollectionReference<Session>;
    const sessionDocumentReference = await addDoc(
      sessionsCollectionReference,
      session
    );
    const sessionDocument: SessionDocument = {
      id: sessionDocumentReference.id,
      data: session,
    };
    return sessionDocument;
  }

  public async updateSession(id: string, session: Session): Promise<void> {
    const sessionDocumentReference = doc(
      this.firestore,
      `${FirebaseSessionsService.sessions}/${id}`
    ) as DocumentReference<Session>;
    await updateDoc(sessionDocumentReference, session);
  }

  public async deleteSession(id: string): Promise<void> {
    const sessionDocumentReference = doc(
      this.firestore,
      `${FirebaseSessionsService.sessions}/${id}`
    ) as DocumentReference<Session>;
    await deleteDoc(sessionDocumentReference);
  }

  private static fixSessionDocument(
    sessionDocument: SessionDocument
  ): SessionDocument {
    sessionDocument.data.startDate = Utility.fixDateForFirebase(
      sessionDocument.data.startDate as unknown as Timestamp
    );
    sessionDocument.data.endDate = Utility.fixDateForFirebase(
      sessionDocument.data.endDate as unknown as Timestamp
    );
    return sessionDocument;
  }
}
