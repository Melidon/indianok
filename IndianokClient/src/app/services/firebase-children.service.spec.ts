import { TestBed } from '@angular/core/testing';

import { FirebaseChildrenService } from './firebase-children.service';

describe('FirebaseChildrenService', () => {
  let service: FirebaseChildrenService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FirebaseChildrenService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
