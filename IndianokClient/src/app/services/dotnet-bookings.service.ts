import { Injectable } from '@angular/core';
import { firstValueFrom } from 'rxjs';
import {
  GeneratedDotnetBookingsService,
  BookingDocument,
  Booking,
} from './generated-dotnet.service';
import { IBookingsService } from './i-bookings.service';

@Injectable({
  providedIn: 'root',
})
export class DotnetBookingsService implements IBookingsService {
  constructor(
    private generatedDotnetBookingsService: GeneratedDotnetBookingsService
  ) {}

  public async getBooking(
    sessionId: string,
    id: string
  ): Promise<BookingDocument | null> {
    const bookingDocument = await firstValueFrom(
      this.generatedDotnetBookingsService.bookingsGET(sessionId, id)
    );
    return bookingDocument;
  }

  public async listBookings(sessionId: string): Promise<BookingDocument[]> {
    const bookingDocuments = await firstValueFrom(
      this.generatedDotnetBookingsService.bookingsAll(sessionId)
    );
    return bookingDocuments;
  }

  public async listBookingsForParent(
    parentId: string
  ): Promise<BookingDocument[]> {
    const bookingDocuments = await firstValueFrom(
      this.generatedDotnetBookingsService.bookingsAll2(parentId)
    );
    return bookingDocuments;
  }

  public async createBooking(
    sessionId: string,
    booking: Booking
  ): Promise<BookingDocument> {
    const bookingDocument = await firstValueFrom(
      this.generatedDotnetBookingsService.bookingsPOST(sessionId, booking)
    );
    return bookingDocument;
  }

  public async updateBooking(
    sessionId: string,
    id: string,
    booking: Booking
  ): Promise<void> {
    await firstValueFrom(
      this.generatedDotnetBookingsService.bookingsPUT(sessionId, id, booking)
    );
  }

  public async deleteBooking(sessionId: string, id: string): Promise<void> {
    await firstValueFrom(
      this.generatedDotnetBookingsService.bookingsDELETE(sessionId, id)
    );
  }
}
