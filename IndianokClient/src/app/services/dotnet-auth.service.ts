import { Injectable } from '@angular/core';
import { BehaviorSubject, firstValueFrom, map, Observable } from 'rxjs';
import {
  GeneratedDotnetAuthService,
  AuthToken,
  AuthCredential,
  AuthEmail,
} from './generated-dotnet.service';
import { IAuthService } from './i-auth.service';

@Injectable({
  providedIn: 'root',
})
export class DotnetAuthService implements IAuthService {
  static readonly dotnetKey = 'dotnet_key';

  isSignedInObservable = new BehaviorSubject<boolean>(
    this.getAuthToken() !== null
  );

  isAdminObservable = new BehaviorSubject<boolean>(false);

  constructor(private generatedDotnetAuthService: GeneratedDotnetAuthService) {}

  public getAuthToken(): AuthToken | null {
    const authTokenString = localStorage.getItem(DotnetAuthService.dotnetKey);
    if (authTokenString === null) {
      return null;
    }
    const authToken: AuthToken = JSON.parse(authTokenString);
    return authToken;
  }

  private setAuthToken(authToken: AuthToken): void {
    localStorage.setItem(
      DotnetAuthService.dotnetKey,
      JSON.stringify(authToken)
    );
  }

  public isSignedIn(): Observable<boolean> {
    return this.isSignedInObservable;
  }

  // https://stackoverflow.com/questions/68014871/decode-jwt-token-in-angular
  private decodeToken(token: string): DecodedIdToken {
    const _decodeToken = (token: string) => {
      try {
        return JSON.parse(atob(token));
      } catch {
        return;
      }
    };
    return token
      .split('.')
      .map((token) => _decodeToken(token))
      .reduce((acc, curr) => {
        if (!!curr) acc = { ...acc, ...curr };
        return acc;
      }, Object.create(null));
  }

  private getClaims(): DecodedIdToken | null {
    const authToken = this.getAuthToken();
    if (authToken === null) {
      return null;
    }
    const accessToken = authToken.accessToken;
    const decodedIdToken = this.decodeToken(accessToken);
    return decodedIdToken;
  }

  public isAdmin(): Observable<boolean> {
    return this.isSignedInObservable.pipe(
      map((isSignedIn) => {
        if (!isSignedIn) {
          return false;
        }
        const claims = this.getClaims()!;
        const admin: boolean = claims[ClaimTypes.Role] === 'admin';
        return admin;
      })
    );
  }

  public getUid(): string | null {
    const claims = this.getClaims();
    if (claims === null) {
      return null;
    }
    const uid = claims[ClaimTypes.NameIdentifier];
    return uid;
  }

  public async signInWithEmailAndPassword(
    email: string,
    password: string
  ): Promise<boolean> {
    const authSignInDto: AuthCredential = {
      email: email,
      password: password,
    };
    const authToken = await firstValueFrom(
      this.generatedDotnetAuthService.login(authSignInDto)
    );
    if (authToken === null) {
      return false;
    }
    this.setAuthToken(authToken);
    this.isSignedInObservable.next(true);
    return true;
  }

  public async createUserWithEmailAndPassword(
    email: string,
    password: string
  ): Promise<boolean> {
    const authCreateUserDto: AuthCredential = {
      email: email,
      password: password,
    };
    const authToken = await firstValueFrom(
      this.generatedDotnetAuthService.register(authCreateUserDto)
    );
    if (authToken === null) {
      return false;
    }
    this.setAuthToken(authToken);
    return true;
  }

  public signOut(): void {
    localStorage.removeItem(DotnetAuthService.dotnetKey);
    this.isSignedInObservable.next(false);
  }

  public async delete(): Promise<void> {
    const claims = this.getClaims();
    if (claims === null) {
      return;
    }
    const email: string = claims[ClaimTypes.Email];
    const authDeleteUserDto: AuthEmail = {
      email: email,
    };
    await firstValueFrom(
      this.generatedDotnetAuthService.delete(authDeleteUserDto)
    );
    this.signOut();
  }
}

interface DecodedIdToken {
  exp: number;
  [key: string]: any;
}

enum ClaimTypes {
  Role = 'http://schemas.microsoft.com/ws/2008/06/identity/claims/role',
  NameIdentifier = 'http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier',
  Email = 'http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress',
}
