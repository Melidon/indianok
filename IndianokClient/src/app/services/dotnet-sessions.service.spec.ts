import { TestBed } from '@angular/core/testing';

import { DotnetSessionsService } from './dotnet-sessions.service';

describe('DotnetSessionsService', () => {
  let service: DotnetSessionsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DotnetSessionsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
