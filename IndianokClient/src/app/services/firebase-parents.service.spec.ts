import { TestBed } from '@angular/core/testing';

import { FirebaseParentsService } from './firebase-parents.service';

describe('FirebaseParentsService', () => {
  let service: FirebaseParentsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FirebaseParentsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
