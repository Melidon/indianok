import { Injectable } from '@angular/core';
import {
  Auth,
  createUserWithEmailAndPassword,
  deleteUser,
  onAuthStateChanged,
  signInWithEmailAndPassword,
  signOut,
} from '@angular/fire/auth';
import { BehaviorSubject, Observable } from 'rxjs';
import { IAuthService } from './i-auth.service';

@Injectable({
  providedIn: 'root',
})
export class FirebaseAuthService implements IAuthService {
  isSignedInObservable = new BehaviorSubject<boolean>(false);
  isAdminObservable = new BehaviorSubject<boolean>(false);

  constructor(private auth: Auth) {
    onAuthStateChanged(this.auth, async (user) => {
      const isSignedIn = user !== null;
      this.isSignedInObservable.next(isSignedIn);
      let isAdmin = false;
      if (isSignedIn) {
        const idToken = await user.getIdTokenResult();
        isAdmin = idToken.claims['role'] === 'Admin';
      }
      this.isAdminObservable.next(isAdmin);
    });
  }

  public isSignedIn(): Observable<boolean> {
    return this.isSignedInObservable;
  }

  public isAdmin(): Observable<boolean> {
    return this.isAdminObservable;
  }

  public getUid(): string | null {
    const user = this.auth.currentUser;
    if (!user) {
      return null;
    }
    return user.uid;
  }

  public async signInWithEmailAndPassword(
    email: string,
    password: string
  ): Promise<boolean> {
    try {
      await signInWithEmailAndPassword(this.auth, email, password);
    } catch {
      return false;
    }
    return true;
  }

  public async createUserWithEmailAndPassword(
    email: string,
    password: string
  ): Promise<boolean> {
    try {
      await createUserWithEmailAndPassword(this.auth, email, password);
    } catch {
      return false;
    }
    return true;
  }

  public signOut(): void {
    signOut(this.auth);
  }

  public async delete(): Promise<void> {
    await deleteUser(this.auth.currentUser!);
  }
}
