import { Observable } from 'rxjs';

export abstract class IAuthService {
  public abstract isSignedIn(): Observable<boolean>;

  public abstract isAdmin(): Observable<boolean>;

  public abstract getUid(): string | null;

  public abstract signInWithEmailAndPassword(
    email: string,
    password: string
  ): Promise<boolean>;

  public abstract createUserWithEmailAndPassword(
    email: string,
    password: string
  ): Promise<boolean>;

  public abstract signOut(): void;

  public abstract delete(): Promise<void>;
}
