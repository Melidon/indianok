import { Injectable } from '@angular/core';
import {
  addDoc,
  collection,
  CollectionReference,
  deleteDoc,
  doc,
  DocumentReference,
  Firestore,
  getDoc,
  getDocs,
  updateDoc,
} from '@angular/fire/firestore';
import { LocationDocument, Location } from './generated-dotnet.service';
import { ILocationsService } from './i-locations.service';

@Injectable({
  providedIn: 'root',
})
export class FirebaseLocationsService implements ILocationsService {
  private static readonly locations = 'locations';

  constructor(private firestore: Firestore) {}

  public async getLocation(id: string): Promise<LocationDocument | null> {
    const locationDocumentReference = doc(
      this.firestore,
      `${FirebaseLocationsService.locations}/${id}`
    ) as DocumentReference<Location>;
    const locationDocumentSnapshot = await getDoc(locationDocumentReference);
    if (!locationDocumentSnapshot.exists()) {
      return null;
    }
    const locationDocument: LocationDocument = {
      id: locationDocumentSnapshot.id,
      data: locationDocumentSnapshot.data(),
    };
    return locationDocument;
  }

  public async listLocations(): Promise<LocationDocument[]> {
    const locationsCollectionReference = collection(
      this.firestore,
      `${FirebaseLocationsService.locations}`
    ) as CollectionReference<Location>;
    const locationDocumentSnapshots = await getDocs(
      locationsCollectionReference
    );
    const locationDocuments = locationDocumentSnapshots.docs.map(
      (locationDocumentSnapshot): LocationDocument => {
        return {
          id: locationDocumentSnapshot.id,
          data: locationDocumentSnapshot.data(),
        };
      }
    );
    return locationDocuments;
  }

  public async createLocation(location: Location): Promise<LocationDocument> {
    const locationsCollectionReference = collection(
      this.firestore,
      `${FirebaseLocationsService.locations}`
    ) as CollectionReference<Location>;
    const locationDocumentReference = await addDoc(
      locationsCollectionReference,
      location
    );
    const locationDocument: LocationDocument = {
      id: locationDocumentReference.id,
      data: location,
    };
    return locationDocument;
  }

  public async updateLocation(id: string, location: Location): Promise<void> {
    const locationDocumentReference = doc(
      this.firestore,
      `${FirebaseLocationsService.locations}/${id}`
    ) as DocumentReference<Location>;
    await updateDoc(locationDocumentReference, location);
  }

  public async deleteLocation(id: string): Promise<void> {
    const locationDocumentReference = doc(
      this.firestore,
      `${FirebaseLocationsService.locations}/${id}`
    ) as DocumentReference<Location>;
    await deleteDoc(locationDocumentReference);
  }
}
