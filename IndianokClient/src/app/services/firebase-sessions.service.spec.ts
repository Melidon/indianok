import { TestBed } from '@angular/core/testing';

import { FirebaseSessionsService } from './firebase-sessions.service';

describe('FirebaseSessionsService', () => {
  let service: FirebaseSessionsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FirebaseSessionsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
