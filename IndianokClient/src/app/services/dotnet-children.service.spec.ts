import { TestBed } from '@angular/core/testing';

import { DotnetChildrenService } from './dotnet-children.service';

describe('DotnetChildrenService', () => {
  let service: DotnetChildrenService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DotnetChildrenService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
