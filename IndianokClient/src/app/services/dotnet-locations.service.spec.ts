import { TestBed } from '@angular/core/testing';

import { DotnetLocationsService } from './dotnet-locations.service';

describe('DotnetLocationsService', () => {
  let service: DotnetLocationsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DotnetLocationsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
