import { TestBed } from '@angular/core/testing';

import { FirebaseBookingsService } from './firebase-bookings.service';

describe('FirebaseBookingsService', () => {
  let service: FirebaseBookingsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FirebaseBookingsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
