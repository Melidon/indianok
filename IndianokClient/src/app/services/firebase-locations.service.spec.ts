import { TestBed } from '@angular/core/testing';

import { FirebaseLocationsService } from './firebase-locations.service';

describe('FirebaseLocationsService', () => {
  let service: FirebaseLocationsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FirebaseLocationsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
