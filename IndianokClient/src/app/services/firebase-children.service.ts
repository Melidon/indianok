import { Injectable } from '@angular/core';
import {
  addDoc,
  collection,
  CollectionReference,
  deleteDoc,
  doc,
  DocumentReference,
  Firestore,
  getDoc,
  getDocs,
  Timestamp,
  updateDoc,
} from '@angular/fire/firestore';
import { Utility } from '../utility';
import { ChildDocument, Child } from './generated-dotnet.service';
import { IChildrenService } from './i-children.service';

@Injectable({
  providedIn: 'root',
})
export class FirebaseChildrenService implements IChildrenService {
  private static readonly parents = 'parents';
  private static readonly children = 'children';

  constructor(private firestore: Firestore) {}

  public async getChild(
    parentId: string,
    id: string
  ): Promise<ChildDocument | null> {
    const childDocumentReference = doc(
      this.firestore,
      `${FirebaseChildrenService.parents}/${parentId}/${FirebaseChildrenService.children}/${id}`
    ) as DocumentReference<Child>;
    const childDocumentSnapshot = await getDoc(childDocumentReference);
    if (!childDocumentSnapshot.exists()) {
      return null;
    }
    const childDocument: ChildDocument = {
      id: childDocumentSnapshot.id,
      parentDocumentId: childDocumentSnapshot.ref.parent.parent!.id,
      data: childDocumentSnapshot.data(),
    };
    return FirebaseChildrenService.fixChildDocument(childDocument);
  }

  public async listChildren(parentId: string): Promise<ChildDocument[]> {
    const childrenCollectionReference = collection(
      this.firestore,
      `${FirebaseChildrenService.parents}/${parentId}/${FirebaseChildrenService.children}`
    ) as CollectionReference<Child>;
    const childDocumentSnapshots = await getDocs(childrenCollectionReference);
    const childDocuments = childDocumentSnapshots.docs.map(
      (childDocumentSnapshot): ChildDocument => {
        return {
          id: childDocumentSnapshot.id,
          parentDocumentId: childDocumentSnapshot.ref.parent.parent!.id,
          data: childDocumentSnapshot.data(),
        };
      }
    );
    return childDocuments.map((childDocument) =>
      FirebaseChildrenService.fixChildDocument(childDocument)
    );
  }

  public async createChild(
    parentId: string,
    child: Child
  ): Promise<ChildDocument> {
    const childrenCollectionReference = collection(
      this.firestore,
      `${FirebaseChildrenService.parents}/${parentId}/${FirebaseChildrenService.children}`
    ) as CollectionReference<Child>;
    const childDocumentReference = await addDoc(
      childrenCollectionReference,
      child
    );
    const childDocument: ChildDocument = {
      id: childDocumentReference.id,
      parentDocumentId: parentId,
      data: child,
    };
    return childDocument;
  }

  public async updateChild(
    parentId: string,
    id: string,
    child: Child
  ): Promise<void> {
    const childDocumentReference = doc(
      this.firestore,
      `${FirebaseChildrenService.parents}/${parentId}/${FirebaseChildrenService.children}/${id}`
    ) as DocumentReference<Child>;
    await updateDoc(childDocumentReference, child);
  }

  public async deleteChild(parentId: string, id: string): Promise<void> {
    const childDocumentReference = doc(
      this.firestore,
      `${FirebaseChildrenService.parents}/${parentId}/${FirebaseChildrenService.children}/${id}`
    ) as DocumentReference<Child>;
    await deleteDoc(childDocumentReference);
  }

  private static fixChildDocument(childDocument: ChildDocument): ChildDocument {
    childDocument.data.dateOfBirth = Utility.fixDateForFirebase(
      childDocument.data.dateOfBirth as unknown as Timestamp
    );
    return childDocument;
  }
}
