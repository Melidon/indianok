import { Child, ChildDocument } from './generated-dotnet.service';

export abstract class IChildrenService {
  public abstract getChild(
    parentId: string,
    id: string
  ): Promise<ChildDocument | null>;

  public abstract listChildren(parentId: string): Promise<ChildDocument[]>;

  public abstract createChild(
    parentId: string,
    child: Child
  ): Promise<ChildDocument>;

  public abstract updateChild(
    parentId: string,
    id: string,
    child: Child
  ): Promise<void>;

  public abstract deleteChild(parentId: string, id: string): Promise<void>;
}
