import { Injectable } from '@angular/core';
import { firstValueFrom } from 'rxjs';
import {
  GeneratedDotnetParentsService,
  ParentDocument,
  Parent,
} from './generated-dotnet.service';
import { IParentsService } from './i-parents.service';

@Injectable({
  providedIn: 'root',
})
export class DotnetParentsService implements IParentsService {
  constructor(
    private generatedDotnetParentsService: GeneratedDotnetParentsService
  ) {}

  public async getParent(id: string): Promise<ParentDocument | null> {
    const parentDocument = await firstValueFrom(
      this.generatedDotnetParentsService.parentsGET(id)
    );
    return parentDocument;
  }

  public async updateParent(id: string, parent: Parent): Promise<void> {
    await firstValueFrom(
      this.generatedDotnetParentsService.parentsPUT(id, parent)
    );
  }
}
