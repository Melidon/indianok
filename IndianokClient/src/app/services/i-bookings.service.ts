import { Booking, BookingDocument } from './generated-dotnet.service';

export abstract class IBookingsService {
  public abstract getBooking(
    sessionId: string,
    id: string
  ): Promise<BookingDocument | null>;

  public abstract listBookings(sessionId: string): Promise<BookingDocument[]>;

  public abstract listBookingsForParent(
    parentId: string
  ): Promise<BookingDocument[]>;

  public abstract createBooking(
    sessionId: string,
    booking: Booking
  ): Promise<BookingDocument>;

  public abstract updateBooking(
    sessionId: string,
    id: string,
    booking: Booking
  ): Promise<void>;

  public abstract deleteBooking(sessionId: string, id: string): Promise<void>;
}
