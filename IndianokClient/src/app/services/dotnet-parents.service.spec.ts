import { TestBed } from '@angular/core/testing';

import { DotnetParentsService } from './dotnet-parents.service';

describe('DotnetParentsService', () => {
  let service: DotnetParentsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DotnetParentsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
