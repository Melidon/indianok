import { TestBed } from '@angular/core/testing';

import { DotnetAuthService } from './dotnet-auth.service';

describe('DotnetAuthService', () => {
  let service: DotnetAuthService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DotnetAuthService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
