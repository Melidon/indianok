import { Location, LocationDocument } from './generated-dotnet.service';

export abstract class ILocationsService {
  public abstract getLocation(id: string): Promise<LocationDocument | null>;

  public abstract listLocations(): Promise<LocationDocument[]>;

  public abstract createLocation(location: Location): Promise<LocationDocument>;

  public abstract updateLocation(id: string, location: Location): Promise<void>;

  public abstract deleteLocation(id: string): Promise<void>;
}
