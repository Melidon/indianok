import { Injectable } from '@angular/core';
import {
  doc,
  DocumentReference,
  Firestore,
  getDoc,
  updateDoc,
} from '@angular/fire/firestore';
import { ParentDocument, Parent } from './generated-dotnet.service';
import { IParentsService } from './i-parents.service';

@Injectable({
  providedIn: 'root',
})
export class FirebaseParentsService implements IParentsService {
  private static readonly parents = 'parents';

  constructor(private firestore: Firestore) {}

  public async getParent(id: string): Promise<ParentDocument | null> {
    const parentDocumentReference = doc(
      this.firestore,
      `${FirebaseParentsService.parents}/${id}`
    ) as DocumentReference<Parent>;
    const parentDocumentSnapshot = await getDoc(parentDocumentReference);
    if (!parentDocumentSnapshot.exists()) {
      return null;
    }
    const parentDocument: ParentDocument = {
      id: parentDocumentSnapshot.id,
      data: parentDocumentSnapshot.data(),
    };
    return parentDocument;
  }

  public async updateParent(id: string, parent: Parent): Promise<void> {
    const parentDocumentReference = doc(
      this.firestore,
      `${FirebaseParentsService.parents}/${id}`
    ) as DocumentReference<Parent>;
    await updateDoc(parentDocumentReference, parent);
  }
}
