import { Injectable } from '@angular/core';
import { firstValueFrom } from 'rxjs';
import { Utility } from '../utility';
import {
  GeneratedDotnetSessionsService,
  SessionDocument,
  Session,
} from './generated-dotnet.service';
import { ISessionsService } from './i-sessions.service';

@Injectable({
  providedIn: 'root',
})
export class DotnetSessionsService implements ISessionsService {
  constructor(
    private generatedDotnetSessionsService: GeneratedDotnetSessionsService
  ) {}

  public async getSession(id: string): Promise<SessionDocument | null> {
    const sessionDocument = await firstValueFrom(
      this.generatedDotnetSessionsService.sessionsGET(id)
    );
    return DotnetSessionsService.fixSessionDocument(sessionDocument);
  }

  public async listSessions(): Promise<SessionDocument[]> {
    const sessionDocuments = await firstValueFrom(
      this.generatedDotnetSessionsService.sessionsAll()
    );
    return sessionDocuments.map((sessionDocument) =>
      DotnetSessionsService.fixSessionDocument(sessionDocument)
    );
  }

  public async createSession(session: Session): Promise<SessionDocument> {
    const sessionDocument = await firstValueFrom(
      this.generatedDotnetSessionsService.sessionsPOST(session)
    );
    return DotnetSessionsService.fixSessionDocument(sessionDocument);
  }

  public async updateSession(id: string, session: Session): Promise<void> {
    await firstValueFrom(
      this.generatedDotnetSessionsService.sessionsPUT(id, session)
    );
  }

  public async deleteSession(id: string): Promise<void> {
    await firstValueFrom(
      this.generatedDotnetSessionsService.sessionsDELETE(id)
    );
  }

  private static fixSessionDocument(
    sessionDocument: SessionDocument
  ): SessionDocument {
    sessionDocument.data.startDate = Utility.fixDateForDotnet(
      sessionDocument.data.startDate as unknown as string
    );
    sessionDocument.data.endDate = Utility.fixDateForDotnet(
      sessionDocument.data.endDate as unknown as string
    );
    return sessionDocument;
  }
}
