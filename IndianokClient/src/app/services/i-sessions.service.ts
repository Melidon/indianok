import { Session, SessionDocument } from './generated-dotnet.service';

export abstract class ISessionsService {
  public abstract getSession(id: string): Promise<SessionDocument | null>;

  public abstract listSessions(): Promise<SessionDocument[]>;

  public abstract createSession(session: Session): Promise<SessionDocument>;

  public abstract updateSession(id: string, session: Session): Promise<void>;

  public abstract deleteSession(id: string): Promise<void>;
}
