import { Injectable } from '@angular/core';
import { firstValueFrom } from 'rxjs';
import { Utility } from '../utility';
import {
  GeneratedDotnetChildrenService,
  ChildDocument,
  Child,
} from './generated-dotnet.service';
import { IChildrenService } from './i-children.service';

@Injectable({
  providedIn: 'root',
})
export class DotnetChildrenService implements IChildrenService {
  constructor(
    private generatedDotnetChildrenService: GeneratedDotnetChildrenService
  ) {}

  public async getChild(
    parentId: string,
    id: string
  ): Promise<ChildDocument | null> {
    const childDocument = await firstValueFrom(
      this.generatedDotnetChildrenService.childrenGET(parentId, id)
    );
    return DotnetChildrenService.fixChildDocument(childDocument);
  }

  public async listChildren(parentId: string): Promise<ChildDocument[]> {
    const childDocuments = await firstValueFrom(
      this.generatedDotnetChildrenService.childrenAll(parentId)
    );
    return childDocuments.map((childDocument) =>
      DotnetChildrenService.fixChildDocument(childDocument)
    );
  }

  public async createChild(
    parentId: string,
    child: Child
  ): Promise<ChildDocument> {
    const childDocument = await firstValueFrom(
      this.generatedDotnetChildrenService.childrenPOST(parentId, child)
    );
    return DotnetChildrenService.fixChildDocument(childDocument);
  }

  public async updateChild(
    parentId: string,
    id: string,
    child: Child
  ): Promise<void> {
    await firstValueFrom(
      this.generatedDotnetChildrenService.childrenPUT(parentId, id, child)
    );
  }

  public async deleteChild(parentId: string, id: string): Promise<void> {
    await firstValueFrom(
      this.generatedDotnetChildrenService.childrenDELETE(parentId, id)
    );
  }

  private static fixChildDocument(childDocument: ChildDocument): ChildDocument {
    childDocument.data.dateOfBirth = Utility.fixDateForDotnet(
      childDocument.data.dateOfBirth as unknown as string
    );
    return childDocument;
  }
}
