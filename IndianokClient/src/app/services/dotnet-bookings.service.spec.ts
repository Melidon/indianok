import { TestBed } from '@angular/core/testing';

import { DotnetBookingsService } from './dotnet-bookings.service';

describe('DotnetBookingsService', () => {
  let service: DotnetBookingsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DotnetBookingsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
