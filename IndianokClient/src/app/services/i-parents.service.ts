import { Parent, ParentDocument } from './generated-dotnet.service';

export abstract class IParentsService {
  public abstract getParent(id: string): Promise<ParentDocument | null>;

  public abstract updateParent(id: string, parent: Parent): Promise<void>;
}
