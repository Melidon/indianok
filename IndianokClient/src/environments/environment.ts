// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebase: {
    projectId: 'indianok-hu',
    appId: '1:405998136083:web:7dffd5ad3c1a4d5eee88e6',
    databaseURL: 'https://indianok-hu.firebaseio.com',
    storageBucket: 'indianok-hu.appspot.com',
    locationId: 'europe-west',
    apiKey: 'AIzaSyAqLcshvRR5PngLKqX-cMn9op0Wy1EQ9fI',
    authDomain: 'indianok-hu.firebaseapp.com',
    messagingSenderId: '405998136083',
    measurementId: 'G-MQ965VGV39',
  },
  production: false,
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
