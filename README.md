# Bevezetés

[Joel Spolsky](https://en.wikipedia.org/wiki/Joel_Spolsky), a [Stack Overflow](https://stackoverflow.com/) egyik társalapítója a következőket írta [egyik cikkében](https://www.joelonsoftware.com/2006/10/12/book-review-beyond-java/):

> _Programming consists of overcoming two things: **accidental difficulties**, things which are difficult because you happen to be using inadequate programming tools, and things which are **actually difficult**, which no programming tool or language is going to solve._

Ezt magyarra a következőképp lehetne fordítani:

> _A programozás két dolog leküzdéséből áll: **véletlen nehézségek**, olyan dolgok, amelyek azért nehezek, mert éppen nem a megfelelő programozási eszközöket használjuk, és olyan dolgok, amelyek **valójában nehezek**, és amelyeket semmilyen programozási eszköz vagy nyelv nem fog megoldani._

Egy szoftverfejlesztő csapat értelemszerűen akkor hatékony, ha csak a valóban nehéz dolgokkal foglalkozik. Természetesen ez a gyakorlatban lehetetlen, de a véletlen nehézségek minimálisra csökkentése kiemelt jelentőséggel bír.

Mindig is foglalkoztatott ez a téma. Különösképpen mióta az egyetemi órák keretein belül megismerkedtem az ASP.NET Core-ral. Korábban csak a Firebase-t ismertem mélyebbre hatóan, és mindig, amikor előadáson valami új ASP.NET Core-os dolgot tanultam, arra gondoltam, hogy ugyanezt a funkcionalitást miképp tudnám megvalósítani Firebase-ben és vajon melyik az amelyik kevesebb nehézséggel jár.
Fordítva sem volt más a helyzet. Egyetemi tanulmányaimmal párhuzamosan dolgoztam mindenféle hobbi projekteken, ahol előszeretettel használtam Firebase-t az adatok tárolására. Közben rengeteg újat tanultam és fejben már azon gondolkoztam, hogy az éppen megszerzett tudásnak mi lehet a ASP.NET Core-os megfelelője.

Nem meglepő módon ezen szakdolgozat az ebben a témában való gondolataim összeírásából született. Ahhoz pedig, hogy az összehasonlításnak valós alapja is legyen, elkészítettem ugyanazt a funkcionalitású backend-et mindkét technológia felhasználásával.
És mivel a klienskódból való használatukat is nagyon fontosnak tartom az összehasonlítás szempontjából, készítettem hozzá egy webalkalmazást is Angular segítségével. Ebben függőség injektálás révén könnyedén meg lehet választani, hogy éppen melyik backendet használja.

Az alkalmazás témáját tekintve egy nyári, indián témájú gyerektábor weboldala, ahol a táborvezetők kiírhatnak turnusokat bizonyos helyszínekre, a szülők meg jelentkezni tudnak ezekre.
Kizárólag a témám szempontjából fontos részre fókuszáltam, azaz a jelentkezések lebonyolítására.

# A készítendő alkalmazás leírása

Létezik egy [indianok.hu](http://indianok.hu/) nevű weboldal. Ez egy nyári gyerektábor honlapja és jelenleg két funkciót lát el.
Elsődlegesen egy reklámfelület, ahol az érdeklődő szülök olvashatnak a táborról és eldönthetik, hogy szeretnék-e ide vinni gyermeküket nyáron.
A második feladat, amit ellát az a jelentkezések lebonyolítása. Ez jelenleg egy beágyazott Google Forms segítségével történik, ami nagyon nem illik oda az amúgy sem modern megjelenésű weboldalra.

Feladatom a régi weboldal alapjainak újraírása modern technológiák felhasználásával.
A két funkciója közül a hangsúly kizárólag a jelentkezések lebonyolításán van.
A reklámfelület elkészítése nem része a feladatomnak. Ennek ellenére az oldalnak könnyen bővíthetőnek kell lennie, hogy később tetszés szerint ezt is elkészíthessem.

## Funkcionális követelmények

Az oldalt látogató szülőknek lehetőségük van jelentkezni a táborba, de ehhez először szükséges, hogy bejelentkezzenek vagy ha még nincsen fiókjuk akkor regisztráljanak. Erre van egy külön **bejelentkezés** fül. A bejelentkezéshez csak egy email cím és jelszó párosra van szükség, míg utóbbi esetben kétszer szükséges beírni a jelszót az esetleges elgépelések végett.

Sikeres bejelentkezést követően a szülő átirányításra kerül a **profil** oldalához.
Itt tudja megadni a jelentkezéshez szükséges személyes adatokat: név, lakcím, telefonszám.
Emellet lehetősége van a kijelentkezésre és a felhasználói fiókja törlésére is.

A bejelentkezett felhasználóknak megjelenik egy **gyerekek** fül is az oldalon. Erre rákattintva átirányításra kerülünk a gyerekek kezelése oldalra. Itt a szülőknek lehetőségük van a saját gyerekeik adatainak kezelésére. Hozzáadhatnak, módosíthatnak és törölhetnek gyerekeket. Egy gyermek felvétele esetén a felugró dialógusablakban a következő adatokat kell megadnia róla: név, születési dátum, TAJ szám, volt-e már egyedült táborban, ismer-e valakit a táborból például egy testvért, illetve egyéb információk, mint például laktózérzékenység.

A **jelentkezés** fülnél a szülőnek először újra át kell néznie, hogy helyesek-e a megadott adatai. Erre azért van szükség, mert lehet, hogy évekkel korábban adta meg őket és aktualizálásra szorulnak.
A következő gomb megnyomásának hatására tovább kerül a gyerekek adatainak ellenőrzéséhez. Erre szintén azért van szükség mert lehet, hogy aktualizálásra szorulnak ezek is.
Még egy következő gomb megnyomásának határása a szülő átkerül a tényleges jelentkezéshez. Itt lehetősége van új jelentkezés létrehozására, illetve már meglévők törlésére. Új jelentkezés hozzáadásakor a felugró dialógusablakon a következőket kell megadnia: melyik turnusba jelentkezik (egy legördülő listából), melyik gyermeke (szintén legördülő lista) és milyen típusú táborba (ottalvós vagy napközis).

Az adminisztrátor joggal rendelkezőknek még van két extra fül az egyik a **helyszínek** kezelése. Lehetőség van helyszínek hozzáadására, módosítására és törlésére. Az itt felugró dialógusablakban a következőket kell megadni: helyszín neve és koordinátái.

A másik kizárólag adminisztrátorok számára elérhető oldal a **turnusok** kezelése, ahol lehetőség van turnusok listázására, módosítására és törlésére. Egy turnus létrehozásakor a felugró dialógusablakban a következőket kell megadni: helyszín, árak (ottalvós és napközis esetben), testvérkedvezmény, előleg, illetve a kezdésnek és végének pontos dátuma.

Egy turnusra rákattintva az admi átkerül a **részletek** fülre. Itt táblázatosan megtekintheti az adott turnusra érkezett összes jelentkezést.

## Nem funkcionális követelmények

A kliens **Angular** segítségével készüljön el.
A felhasználói felület legyen reszponzív és kövesse a material design elveket az **Angular Material** segítségével.

Készüljön két teljesen azonos funkcionalitású backend.
- Az egyik **ASP.NET Core** és **Microsoft SQL Server** felhasználásával készüljön.
- A másik **Firebase** és azon belül **Firestore** használatával készüljön el.

A backendek legyenek alkalmasok a kliens alkalmazás minden funkciójának kiszolgálására. Ideértve a szükséges adatok eltárolásán felül a felhasználókezelést (Authentication and Authorization) is.

# Technológiai áttekintő

Itt egy viszonylag röviden áttekintőt szeretnék adni a használt technológiákról.
Részletekbe menő bemutatásukra az összehasonlítás részben kerül sor.

## ASP.NET Core-os környezet

### REST API

A REST a Representational State Transfer rövidítése. Míg az API az Application Programming Interface-t rövidíti.
Fontos, hogy nem szabvány, hanem inkább "architekturális stílus", azaz inkább szokásokat és irányelveket követ.
Mindenesetre REST által nyújtott megszorításoknak eleget tevő interfészeket, RESTful-nak nevezzük. Egy jól meghatározott RESTful API biztosítja a kliens-szerver kommunikációt implementációtól függetlenül.

### .NET

A [.NET](https://dotnet.microsoft.com/en-us/learn/dotnet/what-is-dotnet) egy ingyenes, több platformot támogató, nyílt forráskódú fejlesztői platform számos különböző típusú alkalmazás létrehozásához. .NET segítségével több nyelvet is lehet használni, ezek közül az egyik legfontosabb a [C#](https://dotnet.microsoft.com/en-us/languages/csharp).

Régebben a .NET kizárólag Windows környezetben működött. Ezután jött a .NET Core, ami már Linux, Windows, macOS, és Docker környezetekben is elfutott. Az 5-ös verziótól kezdve azonban a Microsoft átnevezte. Amit eddig .NET Core-nak hívtunk, az simán .NET lett. A szakdolgozat készítése során .NET 6-ot használtam, úgyhogy a .NET alatt mindig ezt a crossplatform technológiát értem.

### ASP.NET Core

Az [ASP.NET Core](https://dotnet.microsoft.com/en-us/learn/aspnet/what-is-aspnet-core) egy nyílt forráskódú keretrendszer, amelyet a Microsoft hozott létre. Lényege, hogy kifejezetten webalkalmazások készítéséhez szükséges eszközökkel és könyvtárakkal bővíti ki a .NET platformot.

Az ASP.NET Core a rendelkezésre álló tooling és részletes dokumentáció miatt egy remek eszköz egy [REST API](https://learn.microsoft.com/en-us/aspnet/core/web-api/?WT.mc_id=dotnet-35129-website&view=aspnetcore-6.0) elkészítéséhez. Ebből az okból kifolyólag pedig egy manapság nagyon széles körben használt, népszerű rendszerről beszélünk.

### Entity Framework Core

Az [Entity Framework Core](https://learn.microsoft.com/en-us/ef/core/) a népszerű Entity Framework több platformot támogató változata. Ma már minden induló projekt ezt használja, a régi EF6-ba már nem kerülnek bele új funkciók. 

Az EF Core lényegében egy objektum-relációs leképező (O/RM). Azaz lehetővé teszi a fejlesztők számára, hogy C# objektumokat használó adatbázisokkal dolgozzanak. Azaz lehetőséget biztosít az adatbázis sémák C# kódból történő megadására. Ezt hívják [Code First](https://learn.microsoft.com/en-us/ef/ef6/modeling/code-first/workflows/new-database) megközelítésnek.
A fejlesztők legnagyobb örömére megszünteti a legtöbb adatelérési kódot, tehát nem kell SQL lekérdezéseket írni és SQL injection támadások miatt aggódni.

### Microsoft SQL Server

A [Microsoft SQL Server](https://www.microsoft.com/en-us/sql-server/sql-server-2019) egy relációs adatbázis-kezelő rendszer, melyet a Microsoft fejlesztett ki.
Nagyon jó az Entity Framework támogatása és széles körben használják az iparban, pont ezért esett erre a választásom.

## Firebase

A [Firebase](https://firebase.google.com/) egy Google által készített alkalmazásfejlesztő platform.
Részletes dokumentációt és többplatformos SDK-t kínál, melynek segítségével Android, iOS, webes, C++ és Unity környezetben egyszerűen használhatóak a rendelkezésünkre álló termékek.
Ezek közül négyet szeretnék kiemelni.

### Cloud Firestore

A [Cloud Firestore](https://firebase.google.com/products/firestore) egy NoSQL, dokumentum-orientált adatbázis. Az SQL adatbázisokkal ellentétben nincsenek táblák vagy sorok. Ehelyett az adatokat dokumentumokban tárolja, amelyek kollekciókba vannak rendezve.

Minden dokumentum kulcs-érték párokat tartalmaz. A Cloud Firestore kisméretű dokumentumokból álló nagy kollekciók tárolására lett optimalizálva.

Minden dokumentumot egy kollekcióban kell tárolni. A dokumentumok tartalmazhatnak részkollekciókat és beágyazott objektumokat, amelyek mindegyike tartalmazhat primitív mezőket, például sztringeket, vagy összetett objektumokat, sőt akár listákat is.

A dokumentumainkat kollekciókba rendezzük. Egy kollekció egy adatbázis táblának feleltethető meg. Egyelőre tehát az alábbit kell elképzelni.

![](https://firebase.google.com/static/docs/firestore/images/structure-data.png)

Van még pár dolog, amit mindenképpen ki kell emelnem, mert a későbbiekben fontos lesz.
- Az adatbázishoz a klienseknek közvetlen hozzáférésük van.
- Az erőforrásokhoz való hozzáférés korlátozása úgynevezett Security Rule-ok segítségével történik.
- Egy dokumentumot kizárólag teljes egészében lehet olvasni. Amennyiben csak egy részéhez szeretnék korlátozni a hozzáférést, akkor fel kell bontanunk több dokumentumra.

Felmerülhet azonban a kérdés, hogy ugye relációs adatbázisnál kell, hogy legyen egy elsődleges kulcs, hol jelenik ez meg a dokumentumoknál, hogyan lehet őket megkülönböztetni?
Minden dokumentum rendelkezik egy egyedi azonosítóval. Ennek segítségével egyértelműen meg lehet különböztetni a dokumentumokat. Fontos, hogy relációs adatbázisoktól eltérően ez nem a tárolt adatokkal egyenrangúan tárolódik el, és nem is lehet módosítani semmiképp sem. Ennek ellenére persze ugyanúgy használható lekérdezésekhez és a lekérdezés eredményében is benne lesz.
Erről majd bővebben írok az adatok elérése résznél.

### Authentication

Tálcán kapjuk a [Firebase Authentication](https://firebase.google.com/products/auth)-t.
Ez teljes körű felhasználókezelést biztosít, támogatja az e-mail- és jelszófiókokat, a telefonos hitelesítést, a Google-, Twitter-, Facebook- és GitHub-bejelentkezést és még sok mást.
Egyszóval, ha felhasználókezelés kell, akkor Firebase platform esetében ez az egyértelmű választás.

### Cloud Functions

A [Cloud Functions](https://firebase.google.com/products/functions) mottója, hogy fejlesszünk backendet szerverek nélkül. Szerverekre természetesen szükség van, csak szerencsére ezek üzemeltetésének terhét a Google leveszi a vállunkról.
Habár a Firebase mint platform nagyon sok kész megoldást kínál, ezek sajnos nem tudnak mindig elegek lenni.
A Cloud Function-ök segítségével tudunk szerveroldali kódot írni. Ezt arra lehet használni, hogy kiegészítsük a Firebase termékek által nyújtott szolgáltatásokat.
Nagyon sok mindenre használható. Írhatunk velük adatbázis triggereket, de akár közvetlen is meg lehet őket hívni.
Ha egy problémát nem lehet sehogyan sem megoldani a meglévő termékekkel, akkor valószínűleg a Cloud Function lesz a mindig működő, végső megoldás.

### Hosting

A [Firebase Hosting](https://firebase.google.com/products/hosting) által lehetőségünk van webalkalmazások hostolására is. Ez egy nagyon kényelmes funkció mert így lényegében minden üzemeltetési feladatot a Google-re lehet hárítani.

## Angular

Az Angular egy TypeScript-re épülő, ingyenes és nyílt forráskódú webalkalmazás-keretrendszer, melyet a Google Angular csapata fejleszt.
Nagyon jó eszközök állnak rendelkezésre a fejlesztők számára.
Fontos tulajdonsága, hogy egy komponens alapú keretrendszerről van szó, azaz az egyszer elkészített komponensek több helyen is felhasználhatóak a kódunkban.
Rendkívül sok jól integrált komponens könyvtár érhető el hozzá, ezzel jelentősen segítve a fejlesztők munkáját. Ezek közül most egy általam használtat szeretnék kiemelni.

### Angular Material

Az [Angular Material](https://material.angular.io/) egy felhasználói felület komponens könyvtár Angular-höz.
Csomó újra felhasználható komponensből áll, melyek egységes megjelenést biztosítanak az alkalmazásunk számára, ugyanakkor elég jól személyre szabhatóak, hogy tetszőleges alkalmazás stílust lehessen megvalósítani velük.
Használata nagyon megkönnyíti a fejlesztők munkáját.

# Összehasonlítás

## Architektúra

Egy webes kliens számára nagyon hasonlóan működik mindkét backend: küld egy kérést, amire előbb-utóbb majd érkezik egy válasz.

Firestore esetében ez így is marad. A Firebase SDK segítségével a kliensek közvetlen írják és olvassák az adatbázist.

ASP.NET Core-os környezetben viszont valamivel bonyolultabb a dolog. A klienseknek itt nincsen közvetlen hozzáférésük az adatbázishoz. Helyette a fejlesztők készítik el az ASP.NET Core-os REST API-t. Ezzel tudnak kommunikációt kezdeményezni a kliensek, és majd ez tovább kommunikál a Microsoft SQL Serverrel.

## Adatok modellezése

Amikor dönteni szeretnénk, hogy miben írjuk a projektünk backendjét, mindenképpen azzal kell kezdeni, hogy pontosan meghatározzuk az eltárolandó adatokat és azt, hogy milyen lekérdezésekre van szükségünk. Egy pontos specifikációból következnek az objektumok, az eltárolandó attribútumaik, illetve az objektumok között lévő kapcsolatok. Ezek eltárolásának módja azonban már a fejlesztőkre van bízva.

### Normalizálás illetve Denormalizálás

Relációs adatbázis adatbázisok esetében törekszünk az [adatok normalizálásá](https://learn.microsoft.com/hu-hu/office/troubleshoot/access/database-normalization-description)ra.
Firesore esetében viszont ez nem mindig van így. Bizonyos esetekben bevett szokás az adatok denormalizálása.
Elsőre nagyon szokatlanul hangozhat a dolog és úgy tűnhet, hogy a redundáns tárolás csak hátrányokkal járhat.
Firestore esetében azonban vannak előnyei is, nézzük meg miért!

Vegyük a következő problémát:

> Az adminisztrátorok a kizárólag számukra elérhető oldalon tudnak új turnusokat kiírni, a meglévőket szerkeszteni, illetve turnusokat törölni. Szeretnénk, hogy a turnus neve, helyszíne és időpontja mellett az is megjelenjen, hogy eddig hányan jelentkeztek rá, és ehhez ne kelljen a részletes nézetbe menni.

Egy relációs adatbázis esetében összeszámolni a jelentkezések közül azokat amelyikek egy bizonyos turnushoz tartoznak, nem egy drága művelet.
Firestore esetében viszont dokumentumokat csak teljes egészében lehet olvasni. Ez azt jelenti, hogy a fenti problémát csak úgy lehet megoldani, hogy listázzuk a kérdéses turnushoz tartozó összes jelentkezést és megszámoljuk, hogy mennyi van belőle.
Erről ordít, hogy nem egy hatékony megoldás.
- Sokkal több adatot kell átvinni a hálózaton a szükségesnél.
- A számlázás nem lekérdezésenként, hanem dokumentum olvasásonként történik. Azaz, ha egy turnusba 20 fő jelentkezett, akkor ez az 1 lekérdezésünk mellé még pluszba 20-at jelent. Ez nagyon drága tud lenni.

Egy kis gyakorlati tapasztalattal könnyen látható az a lehetőség, hogy a turnus dokumentumokban el lehetne tárolva az is, hogy mennyien jelentkeztek rájuk. Ezzel megspórolnánk a felesleges listázását az egyes jelentkezéseknek. Az egyetlen szépséghibája a dolognak, hogy ha valaki jelentkezik egy turnusba, akkor növelni kell eggyel a számlálót, ha lejelentkezik akkor pedig törölni. Tömören tehát karban kell tartani a denormalizált adatot.
Most van szükség a korábban ismertetett Cloud Function-ökre! Segítségükkel írhatunk triggereket az adatbázisunkhoz. Azaz új foglalás esetén növelhetjük a számlálónkat, egy törlés esetén pedig csökkenthetjük.

Felmerülhet azonban a kérdés, hogy megéri-e ez?
A korábbi példánál maradva, valóban elkerültük az, hogy feleslegesen kelljen listázni 20 jelentkezést. Cserébe viszont minden egyes jelentkezés leadásakor lefutott a trigger, ami frissítette a turnust, amihez leadták, azaz a plusz 20 olvasás helyett lett plusz 20 frissítési művelet. Ha így nézzük, nem nyertünk ezzel semmit. Sőt, az írásért háromszor többet kell fizetni, mint az olvasásért.

Van azonban még pár dolgot, amit még nem vettünk számításba.
- **Eddig** a plusz 20 olvasás **a klienst terhelte**. A **mostani** plusz 20 frissítés viszont már **nem a kliens dolga**.
- Értelemszerűen gyorsabb is lett az alkalmazás, hiszen 1 olvasás mindenképpen gyorsabb mintha ott lenne még mellette 20. Az írás sebessége sem változott a kliensek szemszögéből. A trigger csak azután fut le, hogy a kliens sikeresen írt. A kliensnek nem kell megvárni a trigger lefutását.
- Az utolsó és egyben talán legfontosabb, hogy általában sokkal többször olvassuk az adatokat, mint írjuk, és a denormalizált esetben csak ez a költséges. A példánknál maradva elég valószínű, hogy egy adminisztrátor több mint négyszer fogja lekérdezni egy tábor adatait, azaz jóval olcsóbban fogunk kijönni a denormalizált esetben.

### Hogyan néz ki ez a gyakorlatban

Eddig volt az elméletibb rész, nézzük meg, hogyan néz ki az adatok modellezése a gyakorlatban.

#### ASP.NET-os környezetben

Kétféle megközelítés létezik. A **Database First** és a **Code First**. Most csak az utóbbira szeretnék részletesen kitérni, ugyanis a Database First megközelítést inkább már létező adatbázisok esetén szokták használni, míg a Code First-öt új projektek kezdésekor.
Az új projektet meg feltételezem, hiszen ez az összehasonlítás pont abban kíván segítséget nyújtani, hogy mit érdemes választani egy új projekt kezdésekor.

A **Code First** megközelítést az Entity Framework teszi lehetővé számunkra.
Ez a megközelítés annyit tesz, hogy először kódból hozzuk létre a modelleket, definiáljuk a tárolandó adatokat és a közöttük lévő kapcsolatokat.
Majd ebből már lehetőség van adatbázis sémák generálására.

Következzen egy egyszerű példa. Az első az entitások elkészítése, ez adja, meg, hogy a rekordok milyen attribútumokkal fognak rendelkezni.

```cs
public class LocationEntity
{
    [Key]
    public Guid Id { get; init; }
    public string Name { get; set; }
    public double Latitude { get; set; }
    public double Longitude { get; set; }
}
```

Ez önmagában így még nem elég, kell még egy `DbSet` is. Ez fogja reprezentálni a táblánkat.

```cs
public class ApplicationDbContext : DbContext
{
    // ...

    public DbSet<LocationEntity> Locations { get; set; }
}
```

Ha az alkalmazásunk megfelelően be van konfigurálva, hogy használja a megadott `ApplicationDbContext`-et és egy adatbázishoz is hozzá van kapcsolva, akkor a következő két paranccsal már létre is jön a megfelelő adatbázis tábla.

```bash
dotnet ef migrations add InitialCreate
dotnet ef database update
```

Persze ez még nagyon kevés, általában több adatbázis táblát is szeretnénk. Nagyon egyszerű dolgunk van, elég csak elkészíteni még egy entitást, és az ahhoz tartozó `DbSet`-et is felvenni a `DbContext`-be.
A dolog ott kezd érdekessé válni amikor kapcsolatokat szeretnék definiálni két tábla között.

Definiáljuk a következő **egy-a-sokhoz** kapcsolatot.

> Egy helyszínen több turnus is lehet, viszont egy turnus pontosan egy helyszínen van.

Ebben az esetben csak hozzá kell adnunk a megfelelő navigation property-t a helyszín entitásunkhoz.

```cs
public ICollection<SessionEntity> Sesstions { get; set; }
```

A turnusunkhoz viszont a navigation property-n kívül még hozzá kell adni a helyszín idegen kulcsát is.

```cs
[ForeignKey(nameof(LocationId))]
public Guid LocationId { get; set; }

public LocationEntity Location { get; set; }
```

Erre azért van szükség, mert hiába írunk C# kódot, nem szabad megfeledkezni róla, hogy a háttérben relációs adatbázisokkal dolgozunk, ahol a táblák közötti kapcsolatok idegen kulcsokkal vannak definiálva.

#### Firebase Firestore

A tárolási egység a dokumentum.
Minden dokumentum kulcs-érték párokat tartalmaz és mindegyik dokumentum rendelkezik egy névvel, ami egyértelműen azonosítja.
A dokumentum egy egyszerű rekord megfelelője.

A _Nomádia_ helyszínt ábrázoló dokumentum például így nézhet ki:

```json
{
  "name": "Nomádia",
  "latitude": 47.1266679,
  "longitude": 17.5381207
}
```

A dokumentumban azonban lehetőség van összetett, egymásba ágyazott objektumok tárolására is. Ezeket Firestore kontextusban map-nek hívjuk.
Például a fenti példából kiindulva strukturálhatjuk a koordinátákat egy map segítségével, így:

```json
{
  "name": "Nomádia",
  "geoPoint": {
    "latitude": 47.1266679,
    "longitude": 17.5381207
  }
}
```

Mivel a klienseknek közvetlen hozzáférésük van az adatbázishoz, ezért fontos, hogy úgy tároljuk el az adatokat, hogy azok használata kényelmes legyen a kliens számára és lehetőleg ne kelljen azokat transzformálgatnia.

Érdekességképpen meg szeretném említeni, hogy habár nem olyan gyakran használt, de az ASP.NET Core-os környezetben is létezik ehhez hasonló koncepció. Az `Owned` attribútum segítségével Entity Framework esetében is lehetőség van ugyanilyen egymásba ágyazás megvalósítására.

```cs
public class LocationEntity
{
    public Guid Id { get; init; }
    public string Name { get; set; }
    public GeoPoint GeoPoint { get; set; }
}

[Owned]
public class GeoPoint
{
    public double Latitude { get; set; }
    public double Longitude { get; set; }
}
```

Ezt az osztályt sorosítva a Firestore-oshoz hasonló JSON-t kapunk. De fontos, hogy az adatbázisunkban nem keletkezik új tábla, a `GeoPoint` bele lesz lapítva a `LocationEntity`-hez tartozó táblába.

Térjük vissza Firestore-hoz. Itt dokumentumok kollekciókban vannak, amelyek egyszerűen tárolók a dokumentumok számára. A kollekciók a relációs világ tábláinak megfelelői.

Nem volt szó azonban még a [kapcsolatok modellezésé](https://www.youtube.com/watch?v=haMOUb3KVSo&list=PLl-K7zZEsYLluG5MCVEzXAQ7ACZBCuZgZ&index=5)ről.
A relációs adatbázisokhoz hasonlóan lehetőség van arra, hogy egy dokumentumban tároljuk másik dokumentumok azonosítóját, mint egy idegen kulcsot. Ezt Firestore kontextusban referenciának hívják. Az alkalmazásom elkészítése során ezt a megoldást választottam a helyszínek és turnusok tárolásához.

Van viszont egy másik lehetőség is, amelyik kifejezetten az egy-a-sokhoz kapcsolatok leírására lett kitalálva. Ez az alkollekció, azaz egy adott dokumentumhoz társított kollekció. Az alkalmazásom készítése során ezt a megoldást választottam a turnusok és foglalások tárolására.
Volt egy úgynevezett top-level kollekcióm a turnusoknak, és ezen belül minden turnus dokumentumhoz tartozott egy foglalások alkollekció, amiben az adott turnushoz tartozó foglalások vannak. Ez lekérdezéskor elképesztően kényelmes tud lenni. Nézzünk rá egy példát, hogy miért.

**Firestore lekérdezés**

```ts
const bookingsCollectionReference = collection(
  this.firestore,
  `sessions/${sessionId}/bookings`
)
const bookingDocumentSnapshots = await getDocs(bookingsCollectionReference);
```

**Enitity Framework-ös lekérdezés**

```cs
var bookingEntities = await _context.Bookings
    .Where(booking => booking.SessionId == sessionId)
    .ToListAsync(cancellationToken);
```

Lényegében mindegyik ugyanazt csinálja, de szemantikailag eltérés van közöttük. Egy relációs adatbázisban az összes foglalás egy táblában tárolódik el. A lekérdezés attól adja vissza a helyes eredményeket, hogy megfelelően szűrünk.
Firestore esetében muszáj megadni, hogy melyik turnushoz tartozó jelentkezésekre vagyunk kíváncsiak. Önmagában nem lehet lekérni az összes jelentkezést, de mi pont ezt is szeretnénk, mert nincs is rá szükség és így nem lehet elrontani.

Természetesen amennyiben mást modellezünk és a helyzet úgy kívánja meg, akkor lehetőség van az összes turnushoz tartozó összes dokumentum listázására és azon való szűrésre Firestore esetében is.
Ez az én munkám esetében úgy jelent meg, hogy egy szülő szerette volna megtekinteni az összes általa leadott jelentkezést.

```ts
const bookingsCollection = collectionGroup(
  this.firestore,
  'bookings'
)
const bookingsQuery = query(
  bookingsCollection,
  where('parentId', '==', parentId)
);
const bookingDocumentSnapshots = await getDocs(bookingsQuery);
```

A lekérdezés szerencsére csak minimálisan lett bonyolultabb.
Itt konklúzió tehát az, hogy Firestore esetében is meg lehet valósítani tetszőleges lekérdezéseket, viszont egyesek megfogalmazása könnyebb lehet.

## Adatok elérése

Nagyon nehéz jó összehasonlítást írni. A problémát az jelenti, hogy a Firestore koncepcionálisan eltér egy relációs adatbázistól.
Ha egy webes kliens szempontjából nézem, akkor a Firestore összehasonlítását kell vizsgálnom mondjuk egy REST API-al szemben
Amennyiben viszont az adatbázis szemszögéből nézem, akkor azt kell vizsgálnom, hogy tőle miképp kérhetőek le az adatok. Egy relációs adatbázis szemszögéből a kliens ASP.NET Core-ban írt API-lesz.
Kijelenthetjük tehát, hogy a kliens fogalma nem mindig egyértelmű.

### Adatbázis szemszögéből

Nézzünk egy-egy lekérdezést a különböző adatbázisokhoz!

```ts
const locationDocumentSnapshot = await db
  .collection("locations")
  .doc(id);
```

```cs
var locationEntity = await _context.Locations
    .Where(location => location.Id == id)
    .SingleOrDefaultAsync(cancellationToken);
```

A szintaxis kicsit eltér, de ez nem különösebben fontos. A két lekérdezés ekvivalens.
Az utóbbi esetben viszont a lekérdezés tetszés szerint kiegészíthető egy **Select**-tel.

```cs
var locationEntity = await _context.Locations
    .Where(location => location.Id == id)
    .Select(location => location.name)
    .SingleOrDefaultAsync(cancellationToken);
```

Firestore esetében ilyenre nincsen lehetőség, nem létezik a projekció fogalma.
Egy dokumentumot csak teljes egészében lehet olvasni. Szerencsére ezek jellemzően aprók és teljesítmény szempontjából ma már nem igazán számít pár száz bájt.
Viszont abból a szempontból igenis számít, hogy ha valaki hozzáfér egy dokumentumhoz, akkor az egészhez hozzáfér, nem lehet csak egy részéhez engedélyezni a hozzáférést. A korlátozásról még lesz később részletesen lesz szó, viszont, ha problémának látjuk, hogy valaki emiatt túl sok mindenhez férne hozzá, akkor érdemes visszatérni az adatok modellezéséhez.

Most pedig következzen az adatok elérése egy webes kliens szemszögéből!

### Webes kliens szemszögéből

Meg van már, hogy milyen adatokat és hogyan tárolunk el a backend-ünkön.
Most jön az, hogy a kliensünk kommunikációt szeretne kezdeményezni. Nézzük meg ezt részleteiben!

#### ASP.NET Core

A kommunikációt REST API-on keresztül szeretnénk megvalósítani. Ehhez szerveroldalon minden egyes végpontot nekünk kell definiálni. Célszerű őket megfelelő annotációkkal ellátni. Erre egy egyszerű példa a következő:

```cs
[HttpGet("{id}")]
[ProducesResponseType(StatusCodes.Status200OK, Type = typeof(LocationReadDto))]
[ProducesResponseType(StatusCodes.Status401Unauthorized)]
[ProducesResponseType(StatusCodes.Status403Forbidden)]
[ProducesResponseType(StatusCodes.Status404NotFound)]
public LocationReadDto GetLocation(Guid id)
{
    // ..
}
```

Azért van szükség a `ProducesResponseType` annotációkra, mert ezáltal [Swagger](https://swagger.io/) segítségével pontosabb [OpenAPI](https://swagger.io/specification/) specifikáció generálható.
Az OpenAPI egy API leíró, amely tartalmazza az API műveleteit, adattípusait, dokumentációját.
Ebből már [NSwagStudio](https://github.com/RicoSuter/NSwag/wiki/NSwagStudio) segítségével könnyedén generálhatunk kliens kódot. Ezt mindenképpen szeretnénk, több okból is.
Első és elég egyértelmű, hogy időt spóroljuk. Pár annotáció elhelyezése jóval kevesebb idő, mint a klienskód megírása, ami végtelen hibalehetőséget rejt magában, hiszen az összes előforduló esetleges hibára nekünk kell odafigyelni.
A második indok az, hogy a végpontok módosulása esetén kézzel kellene újra frissíteni a kliens API hívásait, ez megint sok hibalehetőséget rejt. Ha csak egy nem gyakran előforduló hiba kezelése is lemarad, előfordulhat, hogy már csak élesben derül ki.

Habár az annotációk használatával lehetőségünk van minden módosulás után új kódot generálni, lehetőség szerint ezt is jó minimalizálni.
Ha a kliensek közvetlen látják az adatbázis sémákat. Akkor egy apró módosítás esetén is újra kell generálni a kliens kódot, különben nem fog megfelelően működni. Ebben az esetben mondják, hogy **Breaks Contract**.
Ennek kiküszöböléséra a megoldás a [DTO](https://learn.microsoft.com/en-us/aspnet/web-api/overview/data/using-web-api-with-entity-framework/part-5)-k használata. Megtehetjük, hogy a rekordunknak csak egy részét adjuk vissza, ezáltal amikor azt mondjuk bővül a sémánk egy új attribútummal, nem kell újra generálni a kliens kódot.

#### Firebase

Ebben az esetben nagyon egyszerű a dolgunk. Nem kell bajlódni semmi féle generálással, készen kapunk egy SDK-t, aminek segítségével közvetlen hozzáférünk az adatbázisunkhoz.
Erre egy példa:

```ts
public async getLocation(id: string): Promise<DocumentSnapshot<Location>> {
  const locationDocumentReference = doc(
    this.firestore,
    `locations/${id}`
  ) as DocumentReference<Location>;
  const locationDocumentSnapshot = await getDoc(locationDocumentReference);
  return locationDocumentSnapshot;
}
```

Nagyon szeretném kiemelni, hogy itt közvetlen hozzáférés van az adatbázishoz és nincsenek meghatározott sémák.
Tehát amennyiben egy új mezőt szeretnénk hozzáadni a dokumentumhoz, akkor azt csak hozzáírjuk a kliensünkhöz és már írhatjuk is az adatokat az adatbázisba.
Szintén meg kell említenem, hogy egy dokumentumot csak teljes egészében lehet olvasni. Pont ezek miatt Firebase esetében nem használnak DTO-kat.

Szót ejtenék a dolgozatom során felmerült tervezési nehézségekről is. Ugyebár hosszú távon backend-del való kommunikálást a kliensünk kódjában érdemes service-ekbe kiszervezni. Még jobb, ha a service-ektől nem függ senki sem közvetlen, hanem csak egy interfésztől, amit a service-ek megvalósítanak. Ezt hívják ISP-nek ([Interface Segregation Principle](https://en.wikipedia.org/wiki/Interface_segregation_principle)).

De hogyan is nézzen ki pontosan ez az interfész?
Mindenhol azt tanítják, hogy egy jól megtervezett interfész mögött tetszőlegesen cserélhető az implementáció. A nehézség ott rejlik, hogy ASP.NET Core esetében az id-k általában az összes több adattal egyenrangúként érkeznek, míg Firabase esetében egy DocumentSnapshot érkezik, amiben külön található az id és a kristálytiszta adatok.

Így nézhet ki mondjuk a beérkező adat ASP.NET Core esetében:

```json
{
  "id": "cafbfc6d-200f-4169-57ce-08daa7760dea",
  "name": "Nomádia",
  "latitude": 47.1266679,
  "longitude": 17.5381207
}
```

És ehhez hasonlóan Firesotre használatakor:

```json
{
  "id": "cafbfc6d-200f-4169-57ce-08daa7760dea",
  "data": {
    "name": "Nomádia",
    "latitude": 47.1266679,
    "longitude": 17.5381207
  }
}
```

Ha ezek fölé szeretnénk egy közös interfészt, akkor az alábbi lehetőségek közül választhatunk.

Első lehetőség, hogy az interfészünk teljesen mértékben a REST API-hoz alkalmazkodik. Ebben az esetben az ASP.NET Core-hoz generált kliens kódunk mindenféle különösebb nehézség nélkül használható. Firebase esetében viszont még transzformálnunk kell az interfész által elvárt DTO-k és az Firestore-ban tárolt modellek között.
- Törlés értelemszerűen ugyanaz, hiszen csak egy id-t kell megadni.
- Létrehozás esetén azt írunk bele az adatbázisba, amit szeretnénk, ez első ránézésre egyszerűen hangzik. Az egyetlen kérdés, amit ez felvet, hogy mi van akkor, ha el szeretnénk tárolni valamilyen kliensoldalon generálandó adatot is, például az adott dokumentum létrehozásának dátumát. Ez egyszerűen megvalósítható egy adatbázis trigger használatával. A probléma abban rejlik, hogy a trigger azután fut csak le valamikor, hogy a hozzáadás sikeresen megtörtént. Alapvetően a létrehozás jellegű függvények viszont jó, ha visszaadják a frissen létrehozott dokumentumot, mert amennyiben a felhasználónak van egy listája és azt szeretné bővíteni, akkor az adatbázis hívása után szeretnénk, ha az újonnan létrejött elem is megjelenne a listában. Ha ilyen működést szeretnénk, akkor az `onSnapshot` függvényt kell használnunk, ami folyamatosan követi a dokumentum vagy akár egy teljes kollekció változásait. Ez a fajta működés viszont sajnos már nem valósítható meg olyan könnyen ASP.NET Core-ban.
- Olvasásnál kicsit transzformálnunk kell a visszaadott adatokat, hogy az előbb mutatott két JSON közül az alsóból előállítsuk a felsőt.
- Módosításról ugyanaz mondható el, mint a létrehozásról.

A második lehetőség az, hogy az interfészünknek kikötjük, hogy inkább a Firestore-osra hasonlítson. Itt két teljesen független dolgot is meg kell határoznunk.
- Szükségünk van-e valóban DTO-kra a létrehozáshoz, olvasáshoz és íráshoz, vagy elég egy is.
- A korábbihoz hasonlóan a transzformációt a kliensnek kelljen végezni, vagy már a REST API is a korábban említett két JSON közül az alsóhoz hasonlítva szolgáltassa az adatokat. Ez persze nem szokás, de semmi sem köti meg a kezünket, hogy ne így legyen.

A második lehetőségen belüli összes kombináció egy teljesen járható út lehet, de nagyon fontos, hogy a döntést már a fejlesztés korai szakaszában meg kell hoznunk a későbbi óriási átalakítások elkerülése végett.

A konklúzió tehát az, hogy igen, lehetséges közös interfészt rakni az ASP.NET Core és a Firestore fölé, viszont akarva-akaratlanul is meg fog rajta látszani egyik-másik sajátossága.

## Felhasználókezelés

### Authentication

Kezdjük a felhasználók bejelentkeztetésével. Firebase esetén ezt szinte készen kapjuk. Nem csak az email-jelszó párost, hanem egy csomó harmadik fél által biztosított (például Google vagy Facebook) bejelentkezést nagyon egyszerű használni.

Az ASP.NET Core-os környezetben a Firebase Authentication megfelelője az [Azure Active Directory](https://azure.microsoft.com/en-us/products/active-directory/).
A [harmadik fél által támogatott bejelentkezés](https://learn.microsoft.com/en-us/aspnet/core/security/authentication/social/?view=aspnetcore-6.0&tabs=visual-studio-code)ek megvalósítása is viszonylag könnyedén megtehető.

Léteznek még azonban más, népszerű megoldások is, mint például az [IdentiyServer](https://docs.duendesoftware.com/identityserver/v6/quickstarts/0_overview/). Ezt kissé bonyolultabb bekonfigurálni, de nehéznek semmiképp sem mondható.

Sőt ASP.NET Core esetében lehetőségünk van teljesen személyre szabni a felhasználókezelést, mi magunk is megírhatjuk azt.
Mindig azt tanítják, hogy a kriptográfiai algoritmusokat tilos kézzel implementálni, mert azt csak elrontani lehet. Szerencsére a `System.Security.Cryptography` névtéren belül ezek rendelkezésünkre állnak, használatuk viszonylag egyszerű.

```cs
private (byte[] passwordSalt, byte[] passwordHash) CreatePasswordSaltAndHash(string password)
{
    using (var hmac = new HMACSHA512())
    {
        byte[] passwordSalt = hmac.Key;
        byte[] passwordHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));
        return (passwordSalt, passwordHash);
    }
}
```

Ugyanígy segítségünkre van a `System.IdentityModel.Tokens.Jwt` a tokenek generálásához.

### Authorization

A legtöbb esetben nem tud elég lenni a felhasználók bejelentkeztetése, hanem szükség van a különböző jogosultsági szintek megállapítására is.

[Azure AD](https://learn.microsoft.com/en-us/azure/active-directory/roles/custom-overview) beépítetten támogatja a szerepkörök felhasználókhoz való hozzárendelését.
De ha még mi is írjuk meg a token generálását, akkor csak egy apró kiegészítésre van szükség a `System.Security.Claims` segítségével.

Firebase esetében valamivel nehezebb dolgunk van az Azure AD-hoz képest.
A megoldást a Cloud Function-ök hozzák el. Segítségükkel írhatunk triggereket, amikben már tetszőleges kód futtatható szerveroldalon.
Tehát, szükségünk van egy kollekcióra, amiben minden felhasználóhoz eltároljuk, hogy milyen szerepkörrel rendelkezik (és korlátozzuk a hozzáférést, hogy csak az adminisztrátorok állíthassák mások jogait). Amennyiben ennek a kollekciónak módosul egy eleme, lefut a triggerünk és beállítja, hogy az adott felhasználó legközelebbi token generálásakor már a frissült jogok legyenek benne.
Ennek egy egyszerű megvalósítása az alábbi:

```ts
export const onUserWrite = functions.firestore
  .document("users/{userId}")
  .onWrite((change, context) => {
    const uid = context.params.userId;
    const data = change.after.data();
    return admin.auth().setCustomUserClaims(uid, data ?? null);
  });
```

Nem triviális, de azért szerencsére viszonylag kevés idő szükséges az implementálásához és beüzemeléséhez.

## Erőforrásokhoz való hozzáférés korlátozása és adatok validálása

### ASP.NET Core

ASP.NET Core esetében ott van az API az adatbázis és a kliens között. Itt tudunk megírni minden ellenőrzést. De pontosan mit is akarunk ellenőrizni?

Az első, hogy a kérést küldő felhasználónak van-e egyáltalán joga ahhoz, amit csinálni szeretne. Ebben az esetben a [végpontjainkra helyezhetőek el különböző annotációk](https://learn.microsoft.com/en-us/aspnet/core/security/authorization/roles?view=aspnetcore-6.0):

```cs
[HttpGet]
[Authorize]
public IEnumerable<LocationReadDto> ListLocations()
{
    // ...
}

[HttpPost]
[Authorize(Roles = "admin")]
public LocationReadDto CreateLocation(LocationCreateDto location)
{
    // ...
}
```

A fenti kódon egyértelműen látszik, hogy helyszíneket bármelyik bejelentkezett felhasználó listázhat, de új létrehozásához csak az adminisztrátoroknak van joguk.

Az második, hogy az adatbázisba írandó adat megfelelő formátumának validálása. Ennek legegyszerűbb módja a [DTO-k ellátása annotációkkal](https://learn.microsoft.com/en-us/aspnet/core/mvc/models/validation?view=aspnetcore-6.0) a C# kódunkban:

```cs
public record LocationCreateDto
{
    [MinLength(1)]
    public string Name { get; init; }

    [Range(-90, +90)]
    public double Latitude { get; init; }

    [Range(-180, +180)]
    public double Longitude { get; init; }
}
```

A fentiekhez hasonló annotációk használata ugyan nagyon egyszerű, de sajnos nem mindig elég. Nézzünk egy példát!

> Azt szeretnénk, hogy minden szülőnek kizárólag a saját gyermekeinek listázásához legyen joga.

Miben más ez, mint a korábbiak?
Abban, hogy nem tudunk olyan annotációt írni, hogy adatbázisunkban lévő `Child`-ban a `ParentId` megegyezzen a lekérdezést küldő személy azonosítójával. Ezt a problémát nem lehet megoldani pár egyszerű annotáció használatával, hiszen ahhoz, hogy eldönthessük, hogy egy szülő a saját gyerekeit akarja-e listázni először le kell kérni a gyerekeket az adatbázisból, majd megnézni, hogy a `ParentId` idegenkulcsok megegyeznek-e a kérést küldő felhasználó azonosítójával.
Erre a megoldás a [Resource-based authorization](https://learn.microsoft.com/en-us/aspnet/core/security/authorization/resourcebased?view=aspnetcore-6.0). Különböző irányelveket (policy-ket) tudunk definiálni, ezeket beregisztrálni, majd függőséginjektálás révén el tudjuk őket érni és ellenük tesztelni, hogy a kérést küldő felhasználónak van-e joga végrehajtani a kívánt műveletet az adott dokumentumon vagy sem.

### Firestore

Firestore esetében a klienseknek közvetlen hozzáférése van az adatbázishoz, nincsen semmilye köztes API.
Itt úgynevezett [Security Rule](https://firebase.google.com/docs/firestore/security/get-started)-ok segítségével történik mind az erőforrásokhoz való hozzáférés korlátozása, mind az adatok validálása.
Az itt megfogalmazott szabályokkal lehet eldönteni, hogy a kérést engedélyezni kell-e vagy sem.
Ezek többre képesek az ASP.NET Core-ban elérhető szimpla annotációknál, mert a felhasználó tokenje mellett olvasás esetén rendelkezésünkre áll a már meglévő dokumentum, illetve írás esetén az is, hogy hogyan nézne ki a dokumentum egy sikeres írás esetén.
Vegyük észre, hogy ezek segítségével (és helyes modellezéssel, ahol a minden parent-hez tartozik egy children alkollekció) már könnyedén meg is oldható a korábbi gyerek listázás probléma.

```rules
service cloud.firestore {
  match /parents/{parentId} {
    match /children/{childId} {
      allow read: if request.auth.uid == parentId;
    }
  }
}
```

Nagyon szeretném kiemelni, hogy míg **ASP.NET Core esetében** a C# kódban **három különböző helyen** történt az erőforrások hozzáférésének kezelése, addig **Firestore esetében** mindent **egy helyen** a security rule-ok kezelnek. Ez egyszerre jó és rossz is.
Kis alkalmazások esetében nagyon könnyű, hogy csak egy helyen kell ezzel foglalkozni.
Nagy projektek esetében azonban hatalmasra nyúlhat a szabályok leírását tartalmazó fájl. Ezt sajnos már nem biztos, hogy olyan egyszerű karbantartani.
Egy viszont biztos, ha megtagadásra kerül egy kérés, akkor egészen biztosan tudjuk, hogy melyik fájlban kell keresni a hibát. Az egyetlen szépséghibája a dolognak, hogy biztonsági okokra hivatkozva nem tudjuk meg az elutasítás pontos okát, azaz, hogy több feltétel közül melyik nem teljesült. Ez egyrészt megnehezíti a szabályokat kijátszani próbálók dolgát, de sajnos ugyanúgy megnehezíti a fejlesztők munkáját is hibakeresés során.

Ezen felül még lehetőség van másik dokumentumok lekérdezésére is amennyiben szükségünk van rájuk, hogy meghatározzuk, hogy meg kell-e tagadni a kérést. Ennek használatát lehetőleg kerülni kell, mert Firestore esetében lekérdezésenként kell fizetni és bizony ez is beleszámít.

Fontos még, hogy amennyiben egy kollekciót szeretnék olvasni, amiben például minden dokumentumhoz tartozik egy `startDate`, de nekem csak azokhoz van hozzáférésem, amik még nem kezdődtek el, akkor hiába találhatóak csak olyan dokumentumok a kollekcióban, amihez hozzáférek, akkor a kérés el lesz utasítva kivéve, ha én külön meg nem mondom, hogy csak azokra vagyok kíváncsi, amik még nem kezdődtek el.
A Firebase azt nézi, hogy lehetnének benne olyanok is, amik miatt el kéne utasítania a kérést. Ez elsőre furán hangzik, de belegondolva nagyon erőforrás igényes lenne, ha a kollekció összes elemén végig menne.
Röviden tehát a megadott [szabályok nem szűrők](https://firebase.google.com/docs/firestore/security/rules-conditions#rules_are_not_filters), szűrni nekünk kell.

Nézzünk egy példát az előbb említett szabályra:

```rules
service cloud.firestore {
  match /databases/{database}/documents {
    match /sessions/{sessionId} {
      allow read: if request.time < resource.data.startDate;
    }
  }
}
```

**Megtagadott**: A fenti szabály elutasítja a következő lekérdezést, mert az eredményhalmaz tartalmazhat olyan dokumentumokat, amelyek `startDate`-je nem jövőbeli:

```ts
db.collection("sessions")
  .get()
  .then((querySnapshot) => {
    querySnapshot.forEach((doc) => {
      console.log(doc.id, " => ", doc.data());
    });
  });
```

**Engedélyezett**: A fenti szabály lehetővé teszi a következő lekérdezést, mert a `where(Date.now(), "<", "startDate")` kikötés garantálja, hogy az eredményhalmaz megfelel a szabály feltételének:

```ts
db.collection("sessions")
  .where(Date.now(), "<", "startDate")
  .get()
  .then((querySnapshot) => {
    querySnapshot.forEach((doc) => {
      console.log(doc.id, " => ", doc.data());
    });
  });
```

Abban az esetben viszont, amikor egy adott dokumentumra vagyunk kíváncsiak, akkor a Firestore igenis veszi a fáradtságot és megnézi, hogy konkrét mezői alapján engedélyezze-e vagy sem.
Nem kell (és amúgy egy dokumentumra vonatkozó get kérésnél nem is lehet) megmondani semmilyen feltételt sem.
Tehát az előző példánál maradva ez egy helyes lekérdezés:

```ts
db.collection("sessions")
  .doc("someSessionId")
  .get()
  .then((doc) => {
    if (doc.exists()) {
      console.log(doc.id, " => ", doc.data());
    }
  });
```

## Üzemeltetés

### Firebase

A Firebase üzemeltetéséért a Google felelős, nem kell vele semmit sem bajlódnia a fejlesztőknek. Ennek egyik legnagyobb előnye, hogy a horizontális skálázást tálcán kapjuk.

A Firestore ugyanúgy rendelkezésünkre áll, mint fejlesztés során.

Amennyiben használunk Cloud Function-öket, ezek telepítése könnyedén megtehető a [Firebase CLI](https://firebaseopensource.com/projects/firebase/firebase-tools/) megfelelő használatával.

```bash
firebase deploy --only functions
```

A frontend hosztolása sem bonyolultabb, mint bármi más.

```bash
firebase deploy --only hosting
```

Az általam készített alkalmazás például [itt](https://indianok-hu.web.app/home) érhető el.

Sőt, itt még lehetőség van arra is, hogy ne az éles környezetbe telepítsünk, hanem csak egy pár napig elérhető előnézetre.

```bash
firebase hosting:channel:deploy preview --expires 7d
```

Ha ezeket a parancsokat beírjuk egy CI/CD pipeline-ba, akkor már készen is vagyunk és ezen kívül semmilyen üzemeltetési feladattal nem kell foglalkoznunk.

### ASP.NET Core

Ebben az esetében a legegyszerűbb megoldás a Microsoft-ra bízni mindent és használni [Azure SQL Database](https://azure.microsoft.com/en-us/products/azure-sql/database/#overview)-t és az [Azure App Service](https://azure.microsoft.com/en-us/products/app-service/#overview)-t.
Az utóbbi telepítése [Azure CLI](https://learn.microsoft.com/en-us/azure/app-service/quickstart-dotnetcore?tabs=net60&pivots=development-environment-cli) használatával szintén csak egy parancs kiadásából áll.

```bash
az webapp up --sku F1 --name Indianok --os-type linux
```

Itt sem kell aggódnunk a skálázhatóság miatt, sőt semmilyen egyéb üzemeltetési feladattal sem kell foglalkoznunk és a CI/CD pipeline-ok elkészítése is ugyanúgy triviális.

#### Kubernetes

Firebase-zel ellenététben sokkal nagyobb szabadságunk van, lehetőségünk van még arra is, hogy saját magunk üzemeltessük az alkalmazásunkat.
Erre egy nagyon elterjedt megoldás a [Kubernetes](https://kubernetes.io/). Ezt lehet, csak egy felhőszolgáltatónál szeretnék bérelni, de az is lehet, hogy saját magunk üzemeltetjük. Utóbbi különösen sok időt vihet el a fejlesztés során, hiszen a [Docker](https://www.docker.com/) konténerek elkészítése mellett a skálázással is nekünk kell bajlódni. Mármint persze, a Kubernetes valóban indít több példányt egy konténerből, ha kell, de több RAM és CPU erőforrást nem kap, a klaszter bővítése időigényes tud lenni.

Az ok, ami miatt mégis érdemes lehet a saját üzemeltetés mellett dönteni, hogy ha **érzékeny adatok**at tárolunk és azokat nem szeretnénk valamelyik nagy felhőszolgáltatóra bízni, akkor a saját magunk által üzemeltetett klaszter egy biztos megoldás.
Firebase esetében ez teljesen lehetetlen, ASP.NET Core-os környezetben szerencsére van választási lehetőség.

## Költségek

### Firebase

Kezdjük a Firebase költségeinek meghatározásával. Alapból egy úgynevezett **Spark plan** áll rendelkezésünkre, ehhez nem kell semmilyen bankkártya adatot megadni és ingyenesen használhatjuk benne a szolgáltatások nagy részét egy meghatározott limitig. Ez amúgy szerencsére elég nagy, tehát fejlesztés során nem kell aggódni amiatt, hogy túllépnénk, de kisebb projektek esetében még élesben is meg lehet úszni anélkül, hogy fizetnénk érte.

Sajnos viszont van egy kivétel, amire nem elé a Spark plan. Ez pedig nem más, mint a Cloud Function. Használatuk a legegyszerűbb alkalmazások kivételével nem elkerülhető. Úgyhogy az ember kénytelen előfizetni a **Blaze plan**re. Szerencsére azért a Cloud Function-ökre is igaz, hogy ingyenesen használhatóak egy meghatározott limitig, de azért mégiscsak kellemetlenség megadni a bankkártya adatokat, hiszen a fejlesztés során úgysem lépi át az ember ezt a limitet.

Készítettem egy számolást arról, hogy mennyibe fog kerülni amikor élesben fogják használni az elkészített alkalmazásomat.
Egy apró táborról van szó, úgyhogy a következő forgalommal számoltam a tábort megelőző egy hónapban:
- 1000 szülő látogatja meg a weboldalt.
- 100 szülő lesz az, aki tényleges jelentkezést ad le.

A [Firebase árazás](https://firebase.google.com/pricing/)a alapján a következőket mondhatjuk el:
- Authentikáció
  - 10000 regisztráció/hónapig ingyenes, ennek csak az 1%-át használom ki.
  - A be- és kijelentkezés, valamit a fiók törlés teljesen ingyenes bármeddig.
- Firestore
  - 1 GB adat tárolható ingyenesen, ennek még az ezrede sem lesz kihasználva.
  - Írásra, olvasásra és törlésből is legalább 20000 ingyenes naponta, amennyiben mindegyik feltételezett felhasználónk egy napon érkezne, még akkor is csak az ingyenes keret 10%-át érintené alulról a kihasználtság.
- Cloud Functions
  - Habár ehhez már Blaze plan kell, havi 2 millió hívásig ezek is ingyenesek. Ehhez képes elhanyagolhatóan keveset használ az alkalmazás.
- Hosting
  - 10 GB méretű oldal tárolása ingyenes, az alkalmazás meg sem közelíti ezt.
  - Van viszont még egy 360 MB / nap-os adatátviteli limit is. Mivel egy SPA alkalmazásról van szó, ezért reális, hogy egy betöltése az oldalnak 1 MB is lehet. Ebben az esetben már elég közel tud érni az alkalmazásunk az ingyenességi limitig. A forgalom növekedése esetén biztosan ezt lépné át először.

Összességében tehát elmondható, hogy éles környezet esetén sem kell majd fizetnem a szolgáltatásért amennyiben nem szeretnék saját domain nevet, de ezt már nem szeretném beleszámolni.

Be kell vallanom, a teljesen ingyenes nem teljesen igaz. Októberben például kemény 4 Ft adminisztrációs költség azért ki lett számlázva.

### ASP.NET Core

A választott üzemeltetéstől függően nagyon eltérőek lehetnek a költségek.
Ha Kubernetes használata mellett döntünk, akkor a költséget nem csak az határozhatja meg, hogy mennyibe kerül a klaszter. Ha nem Azure-t vagy valamelyik nagy felhőszolgáltatót választjuk, akkor számolnunk kell a hardware és annak karbantartásának költségével. Nem beszélve arról, hogy egy klaszter megfelelő konfigurálása idő, a mérnököknek ezért sokat kell fizetni.

Amennyiben viszont az SQL Database és App Service használata mentén döntünk, viszonylag olcsón ki lehet jönni.

Ehhez készítettem is számításokat, hogy mennyit kéne fizetnem amennyiben Azure-t szeretnék használni. Értelemszerűen ugyanazzal a kihasználtsággal számoltam, mint Firebase esetében:
- [App Service](https://azure.microsoft.com/en-us/pricing/details/app-service/linux/#pricing)
  - 10 alkalmazás üzemeltetéséig ingyenes, nekünk most csak egy van.
  -  60 CPU perc áll rendelkezésünkre naponta. Mivel egy kis alkalmazásról van szó és a kérések kiszolgálása gyors, ez sem fogjuk túllépni.
  - Legfeljebb 1 GB RAM-ot használhat az alkalmazásunk, ebbe is bele fog férni.
  - Maga az alkalmazás 1 GB tárhelyet foglalhat. A lefordított kód ennek az 5%-át sem éri el.
- [Azure Active Directory](https://www.microsoft.com/en-us/security/business/identity-access/azure-active-directory-pricing)
  - Létezik ingyenes verziója, ez havi 50000 aktív felhasználóig ingyenes. Közelébe sem kerülünk a kihasználtságnak.
- SQL Database
  - Létezik [egy évig kipróbálható ingyenes próbaverzió](https://learn.microsoft.com/en-us/azure/azure-sql/database/free-sql-db-free-account-how-to-deploy?view=azuresql).
  - Utána már sajnos fizetni kell érte, ez már nagyban függ attól, hogy milyen szolgáltatásminőséget szeretnénk.

Összességében tehát elmondható, hogy itt is szinte nulla költségen üzemeltethető egy kis forgalmú weboldal.

# Megvalósítás

A szakdolgozat lényegi része az összehasonlítás. Ebben a felhozott példák szinte mindig a ténylegesen elkészült alkalmazásból voltak.
Sokszor volt azonban olyan is, hogy egy felmerülő problémára többféle megoldási lehetőséget is javaslok.
Eddig szándékosan nem volt szó arról, hogy én melyik használata mellett döntöttem. Erről szólni ez a fejezet.

## Adatok modellezése

ASP.NET Core esetében öt olyan táblám van, amit Firestore-ban is le kellett modelleznem. Ezek a következők: szülők, gyerekek, helyszínek, turnusok és jelentkezések.

A szülő egyértelműen top-level kollekció, és viszonylag könnyen adja magát az is, hogy a gyerekek egy alkollekcióban legyenek a szülők alatt.
Ugyanígy egy egyszerű döntés az is, hogy a jelentkezések egy alkollekciójában legyenek a turnusoknak.

A tényleges kérdés az volt, hogy miképp nézzen ki a helyszínek és a turnusok kapcsolata. Ez egy egy-a-sokhoz kapcsolat, azaz minden turnushoz pontosan egy helyszín tartozik, viszont egy helyszínhez tetszőlege számú turnus tartozhat.
Ezek alapján azt lehet mondani, hogy legyen minden helyszínnek egy turnusok alkollekciója. Én viszont mégsem ezt tettem. Helyette azt mondtam, hogy a turnusok és a helyszínek is legyen egy-egy top-level kollekció és minden turnus tartalmazzon egy referenciát egy helyszínre.

Miért döntöttem így? Azért, mert ebben az esetben sokkal kényelmesebben használható az adatbázis.
Egy szülő általában az összes turnust szeretné listázni, ekkor pedig nagyon egyszerű egy lekérdezés.
Amennyiben viszont a helyszíneknek lenne egy turnus alkollekció, akkor, habár megoldható a lekérdezés, egy összetett index használatára van szükség, és igazából nem szép, nem használható kényelmesen.
Teljesítménybeli különbség nincsen a két lehetőség között, mindenképpen két lekérdezés fog kelleni. Egy a turnushoz és egy a hozzá tartozó helyszínhez, ezen nem változtat az, hogy mindkettő top-level kollekció vagy sem.

A mindenképpen két lekérdezés azért nem teljesen igaz. Felmerült bennem ötletként az adatok denormalizálása.
Meg lehetne csinálni, hogy a turnus dokumentum a referencia mellett eltárolja a teljes helyszín dokumentumot.
Ezzel elérnénk, hogy elég legyen csak a turnus olvasása és ne kelljen utána még a referencia alapján lekérdezni hozzá a helyszínt is.
Azért nem készítettem el ezt végül, mert a korábbi denormalizálásos példával ellentétben itt csak egy olvasást spórolunk meg és nem potenciálisan bármennyit.
Nem beszélve arról, hogy az adatok szinkronban tartásának implementálása bonyolult, sőt ebben az esetben már szükség lenne a DTO-k használatára is, hiszen olvasni többet kell, mint beleírni.
Teljesítménykritius esetben megcsinálná, de most úgy éreztem, hogy szükségtelenül bonyolítaná az alkalmazást.

Az adatok modellezésekor volt szó még Firebase esetében a map-ról, aminet az ASP.NET Core-os megfelelője az Owned attribútumot használata. Az összehasonlítás résznél mindenképpen fontosnak éreztem a megemlítésüket mert előfordulhatnak olyan esetek, ahol jelentősen javítják a kód olvashatóságát
Az elkészül alkalmazásban viszont nem használtam egyiket sem. Nem igazán fordult elő, hogy tényleges szükségem lett volna rájuk.

## Adatok elérése

### Interfészek tervezése

Felmerült kérdésként, hogy a kliensben miképp nézzen ki az adatelérést biztosító interfész.
Mivel a fejlesztést az ASP.NET Core-os backend elkészítésével kezdtem, DTO-kat használtam. A kliensben az interfészeknél pedig az objektumok azonosítója egyenrangúként szerepelt a lényegi adattal.

Amikor viszont elkezdtem megcsinálni a Firebase-es backedet is, láttam, hogy ez így nem lesz egy könnyen megvalósítható.
A service interfészek Firebase implementációja túlságosan bonyolult lett volna, nagyon sokat kellett volna transzformálni az adatokat.
Ezen felül pedig a DTO-k igazából feleslegesek voltak, hiszen nem voltak olyan megkötéseim, ami miatt mondjuk egy Create- és egy UpdateDTO eltért volna.

Ez a két ok miatt elhatároztam, hogy a kliensben a service-ek a Firebase-hez hasonló interfészt fognak nyújtani, hogy egyszerű legyen a Firebase service-ek implementálása.
Ez még önmagában keveset javított volna a helyzeten hiszen így csak megcseréltem volna, hogy az ASP.NET Core-os implementáció lesz a bonyolult a Firebase-es meg az egyszerű.

Végül azt a döntést hoztam meg, hogy átdolgozom az ASP.NET Core-os REST API által nyújtott interfészt, hogy jobban hasonlítson a Firebase által nyújtottre és ezáltal egyszerű legyen mindkettő implementációja a kliens kódban.

Nem tagadom, a kelleténél sokkal több időt kellett emiatt rászánnom az alkalmazás elkészítésére mintha megfelelő tapasztalattal rendelkeztem volna.
De cserébe most már rendelkezem a megfelelő tapasztalatokkal, úgyhogy mindenképpen megérte.

Az persze nem egy valós dolog, hogy valaki kétszer elkészítse ugyanazt a funkcionalitású backendet.
Az viszont nagyon is egy valós lehetőség, hogy valaki át szeretne állni az egyik használatáról a másikra.
Most már tudom, hogy amennyiben valaki Firebase-t használ, akkor később könnyedén át tud állni ASP.NET Core-ra.
Viszont fordítva ez már nem mondható el annyira. A DTO-k egy olyan absztrakciós réteget adnak hozzá az alkalmazáshoz, aminek nagyon nehéz tud lenni az átfogalmazása Firebase környezetre.

### Interfészek megvalósítása

Angular-t használva is létezik a függőséginjektálás fogalma, de alapból ott máshogyan néz ki, mint .NET esetében.
Angular-ben amennyiben létrehozunk egy szolgáltatás és megjelöljük injektálhatóként, akkor őt használhatják másik komponensek.

```ts
@Injectable({
  providedIn: 'root',
})
export class AuthService {
  /* ... */
}
```

Aki használni szeretné az közvetlen őt használja, nincsen semmilyen interfész sem.
Ez valamilyen szinten teljesen érthető hiszen a TypeScript kód átfordul JavaScript kódra és ott nem léteznek interfészek.

Hogyan lehet akkor mégis a .NET-eshez hasonló működést megvalósítani?
A megoldás az absztrakt osztály, ezek nem tűnnek el a TypeScript kód átfordítása közben.

```ts
export abstract class IAuthService {
  /* ... */
}

@Injectable({
  providedIn: 'root',
})
export class DotnetAuthService implements IAuthService {
  /* ... */
}
```

Ezután a .NET-hez hasonlóan még meg kell mondani azt is, hogy aki egy ilyen interfészt szeretne használni, az melyik konkrét implementációt kapja. Erre az **app.module.ts**-ben van lehetőség.

```ts
providers: [
  { provide: IAuthService, useExisting: DotnetAuthService },
],
```

## Authentication

Firebase esetében a Firebase Authentication-t az egyetlen könnyen járható út, ezt is választottam.
ASP.NET Core esetében viszont már nem a triviális megoldást választottam. Azokat már korábbi projektjeim, illetve a szakmai gyakorlat során volt alkalmam kipróbálni.
A saját kézzel való implementálás mellett döntöttem, hogy azzal is legyen tapasztalatom. És elismerem, ez csak tapasztalatra volt jó. Jelen projekthez nem adott hozzá semmiféle pluszt, ugyanezt a funkcionalitást könnyedén megvalósíthattam volna Azure AD vagy Identity Server használatával.

# Összefoglalás

A dolgozat írása közben direkt tartózkodtam attól, hogy lerakjam a voksomat valamelyik konkrét platform használata mellet. Véleményem szerint nem emelhető ki egyik platform sem, hogy az minden szituációban jobb, mint a másik.

Amennyiben Firebase SDK érhető el azon a kliens platformon, ahova nekünk kellene, a döntés egyértelmű az ASP.NET Core javára.

Abban az esetben, ha viszont elérhető és nem egyértelmű, hogy szükség lesz az ASP.NET Core által nyújtott könnyebb személyre szabhatóságra, akkor azt mondanám, hogy a Firebase-t érdemes választani.
Nagyságrendekkel gyorsabban el lehet vele készíteni egy demó alkalmazást. A szakdolgozatom készítése alatt egy nap alatt készen voltam a Firebase implementációjával. Itt persze közrejátszott, hogy azt az ASP.NET Core után csináltam, és addigra már pontosan tudtam, hogy mit szeretnék csinálni, de ha ezzel kezdtem volna, akkor is jóval hamarabb elkészültem volna vele.
A Cloud Functionök által bármit meg lehet vele csinálni. A kérdés igazából az, hogy ezek használata meddig egyszerűbb, mint egy ASP.NET Core-os API elkészítése (általában szerencsére elég sokáig).
Startupoknak, illetve kis projektek számára mindenképpen a Firebase-t mondanám nyerőnek.
Már csak azért is, mert Firebase használatáról viszonylag egyszerű áttérni ASP.NET Core használatára, fordítva viszont, ha kiderül, hogy csak fejlesztési többlet az ASP.NET Core, akkor nehéz lehet átállni Firebase-re.

Az egyetlen eset amikor talán mégsem érdemes Firebase-t használni, ha a modellezés során kiderül, hogy az adatok nagyon nem illenek bele. Erre mondjuk nem igazán tudok példát hozni, de elképzelhetőnek tartom.
Vagy igazából már az is elégséges indok lehet, hogy könnyű a modellezés, csak nagyon sok olyan lekérdezés kell írni a kliensekben, amit csak több lekérdezésből kell összeollózni.
Erre persze megoldást nyújthat az adatok denormalizálása, de ha nagyon sok mindent kell denormalizálni, akkor szintén meggondolandó, hogy jó úton haladunk-e.

A végső ítélet tehát, hogy legyen inkább Firebase. Sokkal személyre szabhatóbb, mint elsőre látszik, váltani lehet később is szükség szerint.

## Továbbfejlesztési lehetőségek

Az első egyértelmű továbbfejlesztési lehetőség az alkalmazás továbbfejlesztése. Új funkciók hozzáadására mindig van lehetőség. Ezt az utat nagyon nem tartom valószínűnek. Még elkészítem az oldal "reklámfelület" részét és onnantól kezdve a projektet lezártnak tekintem.

A második dolog az **összehasonlítás kibővítése**. Ebbe a szakdolgozatba csak az elérhető szolgáltatások egy apró töredéke fért bele.
Firebase esetében még van egy csomó érdekes [termék, illetve szolgáltatás](https://firebase.google.com/products-build), nem beszélve arról, hogy mindezek a [Google Cloud](https://cloud.google.com/products/) részét képzik, ahol még több minden érhető el.
Ugyanúgy igaz, hogy [Microsoft Azure-ban is megszámlálhatatlanul sok termék](https://azure.microsoft.com/en-us/products/) közül lehet választani.
Arról nem is beszélve, hogy nem csak a fentebb említett két felhőszolgáltató létezik, hanem ott van még például az [Amazon Web Services](https://aws.amazon.com/products/) is.
A jövőben mindenképpen szeretném majd mélyebben beleásni magamat a témába, hiszen egyértelműen látszik, hogy a jövő a felhőszolgáltatások felé tart.
