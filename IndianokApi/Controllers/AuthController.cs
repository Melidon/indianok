using System.ComponentModel.DataAnnotations;
using IndianokApi.Models;
using IndianokApi.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace IndianokApi.Controllers;

[ApiController]
[Route("api/[controller]")]
[Authorize]
public class AuthController : ControllerBase
{
    private readonly IAuthService _authService;

    public AuthController(IAuthService authService)
    {
        _authService = authService;
    }

    [HttpPost("register"), AllowAnonymous]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<AuthToken>> Register([Required] AuthCredential authCredential, CancellationToken cancellationToken)
    {
        var token = await _authService.CreateUserWithEmailAndPasswordAsync(authCredential, cancellationToken);
        if (token is null)
        {
            BadRequest("Account with such email already exsits.");
        }
        return Ok(token);
    }

    [HttpPost("login"), AllowAnonymous]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<AuthToken>> Login([Required] AuthCredential authCredential, CancellationToken cancellationToken)
    {
        var token = await _authService.SignInWithEmailAndPasswordAsync(authCredential, cancellationToken);
        if (token is null)
        {
            BadRequest("Incorrect login credential.");
        }
        return Ok(token);
    }

    [HttpPut("update")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status403Forbidden)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult> UpdatePasswordAsync([Required] AuthCredential authCredential, CancellationToken cancellationToken)
    {
        var success = await _authService.UpdatePasswordAsync(authCredential, cancellationToken);
        if (!success)
        {
            return NotFound();
        }
        return NoContent();
    }

    [HttpDelete("delete")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status403Forbidden)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult> DeleteAsync([Required] AuthEmail authEmail, CancellationToken cancellationToken)
    {
        var success = await _authService.DeleteAsync(authEmail, cancellationToken);
        if (!success)
        {
            return NotFound();
        }
        return NoContent();
    }
}
