using System.ComponentModel.DataAnnotations;
using IndianokApi.Dtos;
using IndianokApi.Models;
using IndianokApi.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace IndianokApi.Controllers;

[ApiController]
[Route("api/[controller]")]
[Authorize(Roles = "admin")]
public class LocationsController : ControllerBase
{
    private readonly ILocationsService _locationsService;

    public LocationsController(ILocationsService locationsService)
    {
        _locationsService = locationsService;
    }

    [HttpGet("{id}")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Document<Location>))]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status403Forbidden)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<Document<Location>>> GetLocation(Guid id, CancellationToken cancellationToken)
    {
        var locationDocument = await _locationsService.GetLocationAsync(id, cancellationToken);
        if (locationDocument is null)
        {
            return NotFound();
        }
        return Ok(locationDocument);
    }

    [HttpGet]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<Document<Location>>))]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status403Forbidden)]
    public async Task<ActionResult<IEnumerable<Document<Location>>>> ListLocations(CancellationToken cancellationToken)
    {
        var locationDocuments = await _locationsService.ListLocationsAsync(cancellationToken);
        return Ok(locationDocuments);
    }

    [HttpPost]
    [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(Document<Location>))]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status403Forbidden)]
    public async Task<ActionResult<Document<Location>>> CreateLocation([Required] Location location, CancellationToken cancellationToken)
    {
        var locationDocument = await _locationsService.CreateLocationAsync(location, cancellationToken);
        return CreatedAtAction(nameof(GetLocation), new { id = locationDocument.Id }, locationDocument);
    }

    [HttpPut("{id}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status403Forbidden)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult> UpdateLocation(Guid id, [Required] Location location, CancellationToken cancellationToken)
    {
        var success = await _locationsService.UpdateLocationAsync(id, location, cancellationToken);
        if (!success)
        {
            return NotFound();
        }
        return NoContent();
    }

    [HttpDelete("{id}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status403Forbidden)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult> DeleteLocation(Guid id, CancellationToken cancellationToken)
    {
        var success = await _locationsService.DeleteLocationAsync(id, cancellationToken);
        if (!success)
        {
            return NotFound();
        }
        return NoContent();
    }
}
