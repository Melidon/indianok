using System.ComponentModel.DataAnnotations;
using IndianokApi.Dtos;
using IndianokApi.Models;
using IndianokApi.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace IndianokApi.Controllers;

[ApiController]
[Route("api/[controller]")]
[Authorize(Roles = "admin")]
public class SessionsController : ControllerBase
{
    private readonly ISessionsService _sessionsService;

    public SessionsController(ISessionsService sessionsService)
    {
        _sessionsService = sessionsService;
    }

    [HttpGet("{id}")]
    [AllowAnonymous]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Document<Session>))]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<Document<Session>>> GetSession(Guid id, CancellationToken cancellationToken)
    {
        var sessionDocument = await _sessionsService.GetSessionAsync(id, cancellationToken);
        if (sessionDocument is null)
        {
            return NotFound();
        }
        return Ok(sessionDocument);
    }

    [HttpGet]
    [AllowAnonymous]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<Document<Session>>))]
    public async Task<ActionResult<IEnumerable<Document<Session>>>> ListSessions(CancellationToken cancellationToken)
    {
        var sessionDocuments = await _sessionsService.ListSessionsAsync(cancellationToken);
        return Ok(sessionDocuments);
    }

    [HttpPost]
    [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(Document<Session>))]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status403Forbidden)]
    public async Task<ActionResult<Document<Session>>> CreateSession([Required] Session sessionCreateDto, CancellationToken cancellationToken)
    {
        var sessionDocument = await _sessionsService.CreateSessionAsync(sessionCreateDto, cancellationToken);
        return CreatedAtAction(nameof(GetSession), new { id = sessionDocument.Id }, sessionDocument);
    }

    [HttpPut("{id}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status403Forbidden)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult> UpdateSession(Guid id, [Required] Session session, CancellationToken cancellationToken)
    {
        var success = await _sessionsService.UpdateSessionAsync(id, session, cancellationToken);
        if (!success)
        {
            return NotFound();
        }
        return NoContent();
    }

    [HttpDelete("{id}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status403Forbidden)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult> DeleteSession(Guid id, CancellationToken cancellationToken)
    {
        var success = await _sessionsService.DeleteSessionAsync(id, cancellationToken);
        if (!success)
        {
            return NotFound();
        }
        return NoContent();
    }
}
