using System.ComponentModel.DataAnnotations;
using IndianokApi.Dtos;
using IndianokApi.Models;
using IndianokApi.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace IndianokApi.Controllers;

[ApiController]
[Route("api/parents/{parentId}/[controller]")]
[Authorize]
public class ChildrenController : ControllerBase
{
    private readonly IChildrenService _childrenService;

    public ChildrenController(IChildrenService childrenService)
    {
        _childrenService = childrenService;
    }

    [HttpGet("{id}")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Document<Child>))]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status403Forbidden)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<Document<Child>>> GetChild(Guid parentId, Guid id, CancellationToken cancellationToken)
    {
        var childDocument = await _childrenService.GetChildAsync(parentId, id, cancellationToken);
        if (childDocument is null)
        {
            return NotFound();
        }
        return Ok(childDocument);
    }

    [HttpGet]
    [Authorize(Roles = "admin")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<Document<Child>>))]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status403Forbidden)]
    public async Task<ActionResult<IEnumerable<Document<Child>>>> ListChildren(Guid parentId, CancellationToken cancellationToken)
    {
        var childDocuments = await _childrenService.ListChildrenAsync(parentId, cancellationToken);
        return Ok(childDocuments);
    }

    [HttpPost]
    [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(Document<Child>))]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status403Forbidden)]
    public async Task<ActionResult<Document<Child>>> CreateChild(Guid parentId, [Required] Child child, CancellationToken cancellationToken)
    {
        var childDocument = await _childrenService.CreateChildAsync(parentId, child, cancellationToken);
        return CreatedAtAction(nameof(GetChild), new { id = childDocument.Id }, childDocument);
    }

    [HttpPut("{id}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status403Forbidden)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult> UpdateChild(Guid parentId, Guid id, [Required] Child child, CancellationToken cancellationToken)
    {
        var success = await _childrenService.UpdateChildAsync(parentId, id, child, cancellationToken);
        if (!success)
        {
            return NotFound();
        }
        return NoContent();
    }

    [HttpDelete("{id}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status403Forbidden)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult> DeleteChild(Guid parentId, Guid id, CancellationToken cancellationToken)
    {
        var success = await _childrenService.DeleteChildAsync(parentId, id, cancellationToken);
        if (!success)
        {
            return NotFound();
        }
        return NoContent();
    }
}
