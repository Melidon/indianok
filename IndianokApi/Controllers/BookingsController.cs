using System.ComponentModel.DataAnnotations;
using IndianokApi.Dtos;
using IndianokApi.Models;
using IndianokApi.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace IndianokApi.Controllers;

[ApiController]
[Route("api/sessions/{sessionId}/[controller]")]
[Authorize]
public class BookingsController : ControllerBase
{
    private readonly IBookingsService _bookingsService;

    public BookingsController(IBookingsService bookingsService)
    {
        _bookingsService = bookingsService;
    }

    [HttpGet("{id}")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Document<Booking>))]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status403Forbidden)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<Document<Booking>>> GetBooking(Guid sessionId, Guid id, CancellationToken cancellationToken)
    {
        var bookingDocument = await _bookingsService.GetBookingAsync(sessionId, id, cancellationToken);
        if (bookingDocument is null)
        {
            return NotFound();
        }
        return Ok(bookingDocument);
    }

    [HttpGet]
    [Authorize(Roles = "admin")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<Document<Booking>>))]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status403Forbidden)]
    public async Task<ActionResult<IEnumerable<Document<Booking>>>> ListBookings(Guid sessionId, CancellationToken cancellationToken)
    {
        var bookingDocuments = await _bookingsService.ListBookingsAsync(sessionId, cancellationToken);
        return Ok(bookingDocuments);
    }

    [Route("/api/parents/{parentId}/[controller]")]
    [HttpGet]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<Document<Booking>>))]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status403Forbidden)]
    public async Task<ActionResult<IEnumerable<Document<Booking>>>> ListBookingsForParent(Guid parentId, CancellationToken cancellationToken)
    {
        var bookingDocuments = await _bookingsService.ListBookingsForParentAsync(parentId, cancellationToken);
        return Ok(bookingDocuments);
    }

    [HttpPost]
    [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(Document<Booking>))]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status403Forbidden)]
    public async Task<ActionResult<Document<Booking>>> CreateBooking(Guid sessionId, [Required] Booking booking, CancellationToken cancellationToken)
    {
        var bookingDocument = await _bookingsService.CreateBookingAsync(sessionId, booking, cancellationToken);
        return CreatedAtAction(nameof(GetBooking), new { id = bookingDocument.Id }, bookingDocument);
    }

    [HttpPut("{id}")]
    [Authorize(Roles = "admin")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status403Forbidden)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult> UpdateBooking(Guid sessionId, Guid id, [Required] Booking booking, CancellationToken cancellationToken)
    {
        var success = await _bookingsService.UpdateBookingAsync(sessionId, id, booking, cancellationToken);
        if (!success)
        {
            return NotFound();
        }
        return NoContent();
    }

    [HttpDelete("{id}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status403Forbidden)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult> DeleteBooking(Guid sessionId, Guid id, CancellationToken cancellationToken)
    {
        var success = await _bookingsService.DeleteBookingAsync(sessionId, id, cancellationToken);
        if (!success)
        {
            return NotFound();
        }
        return NoContent();
    }
}
