using System.ComponentModel.DataAnnotations;
using IndianokApi.Dtos;
using IndianokApi.Models;
using IndianokApi.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace IndianokApi.Controllers;

[ApiController]
[Route("api/[controller]")]
[Authorize]
public class ParentsController : ControllerBase
{
    private readonly IParentsService _parentsService;

    public ParentsController(IParentsService parentsService)
    {
        _parentsService = parentsService;
    }

    [HttpGet("{id}")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Document<Parent>))]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status403Forbidden)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<Document<Parent>>> GetParent(Guid id, CancellationToken cancellationToken)
    {
        var parentDocument = await _parentsService.GetParentAsync(id, cancellationToken);
        if (parentDocument is null)
        {
            return NotFound();
        }
        return Ok(parentDocument);
    }

    [HttpPut("{id}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status403Forbidden)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult> UpdateParent(Guid id, [Required] Parent parent, CancellationToken cancellationToken)
    {
        var success = await _parentsService.UpdateParentAsync(id, parent, cancellationToken);
        if (!success)
        {
            return NotFound();
        }
        return NoContent();
    }
}
