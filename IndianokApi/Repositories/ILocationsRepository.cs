using IndianokApi.Entities;

namespace IndianokApi.Repositories;

public interface ILocationsRepository
{
    // Read
    Task<LocationEntity?> GetLocationAsync(Guid id, CancellationToken cancellationToken);
    Task<IEnumerable<LocationEntity>> ListLocationsAsync(CancellationToken cancellationToken);

    // Write
    void AddLocation(LocationEntity locationEntity);
    void UpdateLocation(LocationEntity locationEntity);
    void RemoveLocation(LocationEntity locationEntity);

    // Save
    Task SaveChangesAsync(CancellationToken cancellationToken);
}
