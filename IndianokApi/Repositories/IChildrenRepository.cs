using IndianokApi.Entities;

namespace IndianokApi.Repositories;

public interface IChildrenRepository
{
    // Read
    Task<ChildEntity?> GetChildAsync(Guid parentId, Guid id, CancellationToken cancellationToken);
    Task<IEnumerable<ChildEntity>> ListChildrenAsync(Guid parentId, CancellationToken cancellationToken);

    // Write
    void AddChild(ChildEntity childEntity);
    void UpdateChild(ChildEntity childEntity);
    void RemoveChild(ChildEntity childEntity);

    // Save
    Task SaveChangesAsync(CancellationToken cancellationToken);
}
