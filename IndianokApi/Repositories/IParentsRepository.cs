using IndianokApi.Entities;

namespace IndianokApi.Repositories;

public interface IParentsRepository
{
    // Read
    Task<ParentEntity?> GetParentAsync(Guid id, CancellationToken cancellationToken);

    // Write
    void UpdateParent(ParentEntity parentEntity);

    // Save
    Task SaveChangesAsync(CancellationToken cancellationToken);
}
