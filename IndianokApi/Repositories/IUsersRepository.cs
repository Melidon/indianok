using IndianokApi.Entities;

namespace IndianokApi.Repositories;

public interface IUsersRepository
{
    Task<UserEntity?> GetUserAsync(string email, CancellationToken cancellationToken);
    void AddUser(UserEntity userEntity);
    void UpdateUser(UserEntity userEntity);
    void RemoveUser(UserEntity userEntity);
    Task SaveChangesAsync(CancellationToken cancellationToken);
}
