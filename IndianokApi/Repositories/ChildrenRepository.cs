using IndianokApi.Data;
using IndianokApi.Entities;
using Microsoft.EntityFrameworkCore;

namespace IndianokApi.Repositories;

public class ChildrenRepository : IChildrenRepository
{
    private readonly ApplicationDbContext _context;

    public ChildrenRepository(ApplicationDbContext context)
    {
        _context = context;
    }

    public async Task<ChildEntity?> GetChildAsync(Guid parentId, Guid id, CancellationToken cancellationToken)
    {
        var childEntity = await _context.Children
            .Where(child => child.ParentId == parentId)
            .Where(child => child.Id == id)
            .SingleOrDefaultAsync(cancellationToken);
        return childEntity;
    }

    public async Task<IEnumerable<ChildEntity>> ListChildrenAsync(Guid parentId, CancellationToken cancellationToken)
    {
        var childEntityEntities = await _context.Children
            .Where(child => child.ParentId == parentId)
            .ToListAsync(cancellationToken);
        return childEntityEntities;
    }

    public void AddChild(ChildEntity childEntity)
    {
        _context.Children.Add(childEntity);
    }

    public void UpdateChild(ChildEntity childEntity)
    {
        _context.Children.Update(childEntity);
    }

    public void RemoveChild(ChildEntity childEntity)
    {
        _context.Children.Remove(childEntity);
    }

    public async Task SaveChangesAsync(CancellationToken cancellationToken)
    {
        await _context.SaveChangesAsync(cancellationToken);
    }
}
