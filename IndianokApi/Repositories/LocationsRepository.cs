using IndianokApi.Data;
using IndianokApi.Entities;
using Microsoft.EntityFrameworkCore;

namespace IndianokApi.Repositories;

public class LocationsRepository : ILocationsRepository
{
    private readonly ApplicationDbContext _context;

    public LocationsRepository(ApplicationDbContext context)
    {
        _context = context;
    }

    public async Task<LocationEntity?> GetLocationAsync(Guid id, CancellationToken cancellationToken)
    {
        var locationEntity = await _context.Locations
            .Where(location => location.Id == id)
            .SingleOrDefaultAsync(cancellationToken);
        return locationEntity;
    }

    public async Task<IEnumerable<LocationEntity>> ListLocationsAsync(CancellationToken cancellationToken)
    {
        var locationEntityEntities = await _context.Locations
            .ToListAsync(cancellationToken);
        return locationEntityEntities;
    }

    public void AddLocation(LocationEntity locationEntity)
    {
        _context.Locations.Add(locationEntity);
    }

    public void UpdateLocation(LocationEntity locationEntity)
    {
        _context.Locations.Update(locationEntity);
    }

    public void RemoveLocation(LocationEntity locationEntity)
    {
        _context.Locations.Remove(locationEntity);
    }

    public async Task SaveChangesAsync(CancellationToken cancellationToken)
    {
        await _context.SaveChangesAsync(cancellationToken);
    }
}
