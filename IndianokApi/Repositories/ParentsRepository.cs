using IndianokApi.Data;
using IndianokApi.Entities;
using Microsoft.EntityFrameworkCore;

namespace IndianokApi.Repositories;

public class ParentsRepository : IParentsRepository
{
    private readonly ApplicationDbContext _context;

    public ParentsRepository(ApplicationDbContext context)
    {
        _context = context;
    }

    public async Task<ParentEntity?> GetParentAsync(Guid id, CancellationToken cancellationToken)
    {
        var parentEntity = await _context.Parents
            .Where(parent => parent.Id == id)
            .SingleOrDefaultAsync(cancellationToken);
        return parentEntity;
    }

    public void UpdateParent(ParentEntity parentEntity)
    {
        _context.Parents.Update(parentEntity);
    }

    public async Task SaveChangesAsync(CancellationToken cancellationToken)
    {
        await _context.SaveChangesAsync(cancellationToken);
    }
}
