using IndianokApi.Data;
using IndianokApi.Entities;
using Microsoft.EntityFrameworkCore;

namespace IndianokApi.Repositories;

public class UsersRepository : IUsersRepository
{
    private readonly ApplicationDbContext _context;

    public UsersRepository(ApplicationDbContext context)
    {
        _context = context;
    }

    public async Task<UserEntity?> GetUserAsync(string email, CancellationToken cancellationToken)
    {
        var userEntity = await _context.Users
            .Where(user => user.Email == email)
            .Include(user => user.Parent)
            .SingleOrDefaultAsync();
        return userEntity;
    }

    public void AddUser(UserEntity userEntity)
    {
        _context.Users.Add(userEntity);
    }

    public void UpdateUser(UserEntity userEntity)
    {
        _context.Users.Update(userEntity);
    }

    public void RemoveUser(UserEntity userEntity)
    {
        _context.Users.Remove(userEntity);
    }

    public async Task SaveChangesAsync(CancellationToken cancellationToken)
    {
        await _context.SaveChangesAsync(cancellationToken);
    }
}
