using IndianokApi.Entities;

namespace IndianokApi.Repositories;

public interface IBookingsRepository
{
    // Read
    Task<BookingEntity?> GetBookingAsync(Guid sessionId, Guid id, CancellationToken cancellationToken);
    Task<IEnumerable<BookingEntity>> ListBookingsAsync(Guid sessionId, CancellationToken cancellationToken);
    Task<IEnumerable<BookingEntity>> ListBookingsForParentAsync(Guid parentId, CancellationToken cancellationToken);

    // Write
    void AddBooking(BookingEntity bookingEntity);
    void UpdateBooking(BookingEntity bookingEntity);
    void RemoveBooking(BookingEntity bookingEntity);

    // Save
    Task SaveChangesAsync(CancellationToken cancellationToken);
}
