using IndianokApi.Entities;

namespace IndianokApi.Repositories;

public interface ISessionsRepository
{
    // Read
    Task<SessionEntity?> GetSessionAsync(Guid id, CancellationToken cancellationToken);
    Task<IEnumerable<SessionEntity>> ListSessionsAsync(CancellationToken cancellationToken);

    // Write
    void AddSession(SessionEntity sessionEntity);
    void UpdateSession(SessionEntity sessionEntity);
    void RemoveSession(SessionEntity sessionEntity);

    // Save
    Task SaveChangesAsync(CancellationToken cancellationToken);
}
