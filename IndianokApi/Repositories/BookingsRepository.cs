using IndianokApi.Data;
using IndianokApi.Entities;
using Microsoft.EntityFrameworkCore;

namespace IndianokApi.Repositories;

public class BookingsRepository : IBookingsRepository
{
    private readonly ApplicationDbContext _context;

    public BookingsRepository(ApplicationDbContext context)
    {
        _context = context;
    }

    public async Task<BookingEntity?> GetBookingAsync(Guid sessionId, Guid id, CancellationToken cancellationToken)
    {
        var bookingEntity = await _context.Bookings
            .Where(booking => booking.SessionId == sessionId)
            .Where(booking => booking.Id == id)
            .Include(booking => booking.Child)
            .SingleOrDefaultAsync(cancellationToken);
        return bookingEntity;
    }

    public async Task<IEnumerable<BookingEntity>> ListBookingsAsync(Guid sessionId, CancellationToken cancellationToken)
    {
        var bookingEntityEntities = await _context.Bookings
            .Where(booking => booking.SessionId == sessionId)
            .Include(booking => booking.Child)
            .ToListAsync(cancellationToken);
        return bookingEntityEntities;
    }

    public async Task<IEnumerable<BookingEntity>> ListBookingsForParentAsync(Guid parentId, CancellationToken cancellationToken)
    {
        var bookingEntityEntities = await _context.Bookings
            .Include(booking => booking.Child)
            .Where(booking => booking.Child.ParentId == parentId)
            .ToListAsync(cancellationToken);
        return bookingEntityEntities;
    }

    public void AddBooking(BookingEntity bookingEntity)
    {
        _context.Bookings.Add(bookingEntity);
    }

    public void UpdateBooking(BookingEntity bookingEntity)
    {
        _context.Bookings.Update(bookingEntity);
    }

    public void RemoveBooking(BookingEntity bookingEntity)
    {
        _context.Bookings.Remove(bookingEntity);
    }

    public async Task SaveChangesAsync(CancellationToken cancellationToken)
    {
        await _context.SaveChangesAsync(cancellationToken);
    }
}
