using IndianokApi.Data;
using IndianokApi.Entities;
using Microsoft.EntityFrameworkCore;

namespace IndianokApi.Repositories;

public class SessionsRepository : ISessionsRepository
{
    private readonly ApplicationDbContext _context;

    public SessionsRepository(ApplicationDbContext context)
    {
        _context = context;
    }

    public async Task<SessionEntity?> GetSessionAsync(Guid id, CancellationToken cancellationToken)
    {
        var sessionEntity = await _context.Sessions
            .Where(session => session.Id == id)
            .SingleOrDefaultAsync(cancellationToken);
        return sessionEntity;
    }

    public async Task<IEnumerable<SessionEntity>> ListSessionsAsync(CancellationToken cancellationToken)
    {
        var sessionEntityEntities = await _context.Sessions
            .ToListAsync(cancellationToken);
        return sessionEntityEntities;
    }

    public void AddSession(SessionEntity sessionEntity)
    {
        _context.Sessions.Add(sessionEntity);
    }

    public void UpdateSession(SessionEntity sessionEntity)
    {
        _context.Sessions.Update(sessionEntity);
    }

    public void RemoveSession(SessionEntity sessionEntity)
    {
        _context.Sessions.Remove(sessionEntity);
    }

    public async Task SaveChangesAsync(CancellationToken cancellationToken)
    {
        await _context.SaveChangesAsync(cancellationToken);
    }
}
