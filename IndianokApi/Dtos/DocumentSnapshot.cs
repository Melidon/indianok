using System.ComponentModel.DataAnnotations;

namespace IndianokApi.Dtos;

public record class Document<T>
{
    [Required]
    public Guid Id { get; init; }

    public Guid? ParentDocumentId { get; init; }
    
    [Required]
    public T data { get; init; } = default!;
}
