using System.ComponentModel.DataAnnotations;

namespace IndianokApi.Models;

public record Session
{
    [Required]
    public Guid LocationId { get; init; }

    [Required]
    public int PriceForDayCamp { get; init; }

    [Required]
    public int PriceForOvernightCamp { get; init; }

    [Required]
    public int Advance { get; init; }

    [Required]
    public int SiblingDiscount { get; init; }

    [Required]
    public DateTime StartDate { get; init; }

    [Required]
    public DateTime EndDate { get; init; }
}
