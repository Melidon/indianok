using System.ComponentModel.DataAnnotations;

namespace IndianokApi.Models;

public record AuthEmail
{
    [Required]
    [EmailAddress]
    public string Email { get; init; } = string.Empty;
}
