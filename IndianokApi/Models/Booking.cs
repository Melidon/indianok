using System.ComponentModel.DataAnnotations;

namespace IndianokApi.Models;

public enum CampType
{
    DayCamp,
    OvernightCamp,
}

public enum PaymentStatus
{
    NotPaid,
    AdvancePaid,
    FullyPaid,
}

public record Booking
{
    [Required]
    public Guid ParentId { get; init; }

    [Required]
    public Guid ChildId { get; init; }

    [Required]
    public CampType CampType { get; init; }

    [Required]
    public PaymentStatus PaymentStatus { get; init; }
}
