using System.ComponentModel.DataAnnotations;

namespace IndianokApi.Models;

public record Location
{
    [Required]
    [MinLength(1)]
    public string Name { get; init; } = string.Empty;

    [Required]
    [Range(-90, +90)]
    public double Latitude { get; init; }

    [Required]
    [Range(-180, +180)]
    public double Longitude { get; init; }
}
