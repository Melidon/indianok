using System.ComponentModel.DataAnnotations;

namespace IndianokApi.Models;

public record AuthToken
{
    [Required]
    public string AccessToken { get; init; } = string.Empty;
}
