using System.ComponentModel.DataAnnotations;

namespace IndianokApi.Models;

public record Child
{
    [Required]
    public string Name { get; init; } = string.Empty;

    [Required]
    public DateTime DateOfBirth { get; init; }

    [Required]
    public int SocialSecurityNumber { get; init; }

    [Required]
    public bool HasBeenAloneInCamp { get; init; }

    [Required]
    public bool KnowsSomeoneFromTheCamp { get; init; }

    [Required]
    public string AdditionalPersonalInformation { get; init; } = string.Empty;
}
