using System.ComponentModel.DataAnnotations;

namespace IndianokApi.Models;

public record AuthCredential
{
    [Required]
    [EmailAddress]
    public string Email { get; init; } = string.Empty;

    [Required]
    [MinLength(6)]
    public string Password { get; init; } = string.Empty;
}
