namespace IndianokApi.Extensions;

public static class ScopeFunctions
{
    public static T also<T>(this T t, Action<T> block)
    {
        block(t);
        return t;
    }

    public static R let<T, R>(this T t, Func<T, R> block)
    {
        return block(t);
    }

}