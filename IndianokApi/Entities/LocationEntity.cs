using System.ComponentModel.DataAnnotations;

namespace IndianokApi.Entities;

public class LocationEntity
{
    [Key]
    public Guid Id { get; init; }
    public string Name { get; set; } = string.Empty;
    public double Latitude { get; set; }
    public double Longitude { get; set; }

    public virtual ICollection<SessionEntity> Sesstions { get; set; } = null!;
}
