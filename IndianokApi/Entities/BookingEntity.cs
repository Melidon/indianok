using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using IndianokApi.Models;

namespace IndianokApi.Entities;

public class BookingEntity
{
    [ForeignKey(nameof(SessionId))]
    public Guid SessionId { get; set; }
    [Key]
    public Guid Id { get; init; }
    [ForeignKey(nameof(ChildId))]
    public Guid ChildId { get; set; }
    public CampType CampType { get; set; }
    public PaymentStatus PaymentStatus { get; set; }

    public virtual SessionEntity Session { get; set; } = null!;
    public virtual ChildEntity Child { get; set; } = null!;
}
