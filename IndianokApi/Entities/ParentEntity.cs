using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IndianokApi.Entities;

public class ParentEntity
{
    [Key]
    public Guid Id { get; init; }
    public string Email { get; set; } = string.Empty;
    public string Name { get; set; } = string.Empty;
    public string Address { get; set; } = string.Empty;
    public string Phone { get; set; } = string.Empty;

    [ForeignKey(nameof(Email))]
    public virtual UserEntity User { get; set; } = null!;
    public virtual ICollection<ChildEntity> Children { get; set; } = null!;
}
