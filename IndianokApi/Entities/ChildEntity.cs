using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IndianokApi.Entities;

public class ChildEntity
{
    [ForeignKey(nameof(ParentId))]
    public Guid ParentId { get; set; }
    [Key]
    public Guid Id { get; init; }
    public string Name { get; set; } = string.Empty;
    public DateTime DateOfBirth { get; set; }
    public int SocialSecurityNumber { get; set; }
    public bool HasBeenAloneInCamp { get; set; }
    public bool KnowsSomeoneFromTheCamp { get; set; }
    public string AdditionalPersonalInformation { get; set; } = string.Empty;

    public virtual ParentEntity Parent { get; set; } = null!;
    public virtual ICollection<BookingEntity> Bookings { get; set; } = null!;
}
