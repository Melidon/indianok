using System.ComponentModel.DataAnnotations;

namespace IndianokApi.Entities;

public class UserEntity
{
    [Key]
    public string Email { get; set; } = string.Empty;
    public byte[] PasswordSalt { get; set; } = null!;
    public byte[] PasswordHash { get; set; } = null!;
    public bool IsAdmin { get; set; }

    public virtual ParentEntity Parent { get; set; } = null!;
}
