using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IndianokApi.Entities;

public class SessionEntity
{
    [Key]
    public Guid Id { get; init; }
    [ForeignKey(nameof(LocationId))]
    public Guid LocationId { get; set; }
    public int PriceForDayCamp { get; set; }
    public int PriceForOvernightCamp { get; set; }
    public int Advance { get; set; }
    public int SiblingDiscount { get; set; }
    public DateTime StartDate { get; set; }
    public DateTime EndDate { get; set; }

    public virtual LocationEntity Location { get; set; } = null!;
    public virtual ICollection<BookingEntity> Bookings { get; set; } = null!;
}
