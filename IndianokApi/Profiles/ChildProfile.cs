using AutoMapper;
using IndianokApi.Models;
using IndianokApi.Entities;
using IndianokApi.Dtos;

namespace IndianokApi.Profiles;

class ChildProfile : Profile
{
    public ChildProfile()
    {
        CreateMap<Child, ChildEntity>();
        CreateMap<ChildEntity, Child>();
        CreateMap<ChildEntity, Document<Child>>()
            .ForMember(childDocument => childDocument.ParentDocumentId, opt => opt.MapFrom(child => child.ParentId))
            .ForMember(childDocument => childDocument.data, opt => opt.MapFrom(child => child));
    }
}
