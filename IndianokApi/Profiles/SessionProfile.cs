using AutoMapper;
using IndianokApi.Models;
using IndianokApi.Entities;
using IndianokApi.Dtos;

namespace IndianokApi.Profiles;

class SessionProfile : Profile
{
    public SessionProfile()
    {
        CreateMap<Session, SessionEntity>();
        CreateMap<SessionEntity, Session>();
        CreateMap<SessionEntity, Document<Session>>()
            .ForMember(sessionDocument => sessionDocument.data, opt => opt.MapFrom(session => session));
    }
}
