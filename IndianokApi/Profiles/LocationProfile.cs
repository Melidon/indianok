using AutoMapper;
using IndianokApi.Models;
using IndianokApi.Entities;
using IndianokApi.Dtos;

namespace IndianokApi.Profiles;

class LocationProfile : Profile
{
    public LocationProfile()
    {
        CreateMap<Location, LocationEntity>();
        CreateMap<LocationEntity, Location>();
        CreateMap<LocationEntity, Document<Location>>()
            .ForMember(locationDocument => locationDocument.data, opt => opt.MapFrom(location => location));
    }
}
