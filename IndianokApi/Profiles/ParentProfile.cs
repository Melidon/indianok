using AutoMapper;
using IndianokApi.Models;
using IndianokApi.Entities;
using IndianokApi.Dtos;

namespace IndianokApi.Profiles;

class ParentProfile : Profile
{
    public ParentProfile()
    {
        CreateMap<Parent, ParentEntity>();
        CreateMap<ParentEntity, Parent>();
        CreateMap<ParentEntity, Document<Parent>>()
            .ForMember(parentDocument => parentDocument.data, opt => opt.MapFrom(parent => parent));
    }
}
