using AutoMapper;
using IndianokApi.Models;
using IndianokApi.Entities;
using IndianokApi.Dtos;

namespace IndianokApi.Profiles;

class BookingProfile : Profile
{
    public BookingProfile()
    {
        CreateMap<Booking, BookingEntity>();
        CreateMap<BookingEntity, Booking>()
            .ForMember(booking => booking.ParentId, opt => opt.MapFrom(booking => booking.Child.ParentId));
        CreateMap<BookingEntity, Document<Booking>>()
            .ForMember(bookingDocument => bookingDocument.ParentDocumentId, opt => opt.MapFrom(booking => booking.SessionId))
            .ForMember(bookingDocument => bookingDocument.data, opt => opt.MapFrom(booking => booking));
    }
}
