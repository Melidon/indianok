using IndianokApi.Entities;
using Microsoft.EntityFrameworkCore;

namespace IndianokApi.Data;

public class ApplicationDbContext : DbContext
{
    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
    {
    }

    public DbSet<BookingEntity> Bookings { get; set; } = null!;
    public DbSet<ChildEntity> Children { get; set; } = null!;
    public DbSet<LocationEntity> Locations { get; set; } = null!;
    public DbSet<ParentEntity> Parents { get; set; } = null!;
    public DbSet<SessionEntity> Sessions { get; set; } = null!;
    public DbSet<UserEntity> Users { get; set; } = null!;
}
