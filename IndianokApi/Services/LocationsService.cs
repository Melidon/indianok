using System.Security.Claims;
using AutoMapper;
using IndianokApi.Models;
using IndianokApi.Entities;
using IndianokApi.Repositories;
using IndianokApi.Dtos;

namespace IndianokApi.Services;

public class LocationsService : ILocationsService
{
    private static readonly string _notFound = "Not found";
    private static readonly string _accessDenied = "Access denied";

    private readonly IMapper _mapper;
    private readonly ILogger<LocationsService> _logger;
    private readonly IHttpContextAccessor _httpContextAccessor;
    private readonly ILocationsRepository _locationsRepository;

    public LocationsService(
        IMapper mapper,
        ILogger<LocationsService> logger,
        IHttpContextAccessor httpContextAccessor,
        ILocationsRepository locationsRepository)
    {
        _mapper = mapper;
        _logger = logger;
        _httpContextAccessor = httpContextAccessor;
        _locationsRepository = locationsRepository;
    }

    public async Task<Document<Location>?> GetLocationAsync(Guid id, CancellationToken cancellationToken)
    {
        _logger.LogDebug("Getting location");
        var locationEntity = await _locationsRepository.GetLocationAsync(id, cancellationToken);
        if (locationEntity is null)
        {
            _logger.LogInformation(_notFound);
            return null;
        }
        var isAdmin = IsAdmin();
        if (!isAdmin)
        {
            _logger.LogWarning(_accessDenied);
            throw new UnauthorizedAccessException();
        }
        var locationDocument = _mapper.Map<Document<Location>>(locationEntity);
        return locationDocument;
    }

    public async Task<IEnumerable<Document<Location>>> ListLocationsAsync(CancellationToken cancellationToken)
    {
        _logger.LogDebug("Listing locations");
        var isAdmin = IsAdmin();
        if (!isAdmin)
        {
            _logger.LogWarning(_accessDenied);
            throw new UnauthorizedAccessException();
        }
        var locationEntities = await _locationsRepository.ListLocationsAsync(cancellationToken);
        var locationDocuments = _mapper.Map<IEnumerable<Document<Location>>>(locationEntities);
        return locationDocuments;
    }

    public async Task<Document<Location>> CreateLocationAsync(Location location, CancellationToken cancellationToken)
    {
        _logger.LogDebug("Creating location");
        var isAdmin = IsAdmin();
        if (!isAdmin)
        {
            _logger.LogWarning(_accessDenied);
            throw new UnauthorizedAccessException();
        }
        var locationEntity = _mapper.Map<LocationEntity>(location);
        _locationsRepository.AddLocation(locationEntity);
        await _locationsRepository.SaveChangesAsync(cancellationToken);
        var locationDocument = _mapper.Map<Document<Location>>(locationEntity);
        return locationDocument;
    }

    public async Task<bool> UpdateLocationAsync(Guid id, Location location, CancellationToken cancellationToken)
    {
        _logger.LogDebug("Updating location");
        var locationEntity = await _locationsRepository.GetLocationAsync(id, cancellationToken);
        if (locationEntity is null)
        {
            _logger.LogInformation(_notFound);
            return false;
        }
        var isAdmin = IsAdmin();
        if (!isAdmin)
        {
            _logger.LogWarning(_accessDenied);
            throw new UnauthorizedAccessException();
        }
        _mapper.Map(location, locationEntity);
        _locationsRepository.UpdateLocation(locationEntity);
        await _locationsRepository.SaveChangesAsync(cancellationToken);
        return true;
    }

    public async Task<bool> DeleteLocationAsync(Guid id, CancellationToken cancellationToken)
    {
        _logger.LogDebug("Deleting location");
        var locationEntity = await _locationsRepository.GetLocationAsync(id, cancellationToken);
        if (locationEntity is null)
        {
            _logger.LogInformation(_notFound);
            return false;
        }
        var isAdmin = IsAdmin();
        if (!isAdmin)
        {
            _logger.LogWarning(_accessDenied);
            throw new UnauthorizedAccessException();
        }
        _locationsRepository.RemoveLocation(locationEntity);
        await _locationsRepository.SaveChangesAsync(cancellationToken);
        return true;
    }

    private bool IsAdmin()
    {
        var isAdmin = _httpContextAccessor.HttpContext?.User.FindFirstValue(ClaimTypes.Role) == "admin";
        return isAdmin;
    }
}
