using IndianokApi.Dtos;
using IndianokApi.Models;

namespace IndianokApi.Services;

public interface IParentsService
{
    // Read
    Task<Document<Parent>?> GetParentAsync(Guid id, CancellationToken cancellationToken);

    // Write
    Task<bool> UpdateParentAsync(Guid id, Parent parent, CancellationToken cancellationToken);
}
