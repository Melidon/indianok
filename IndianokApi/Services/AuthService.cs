using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using IndianokApi.Models;
using IndianokApi.Entities;
using IndianokApi.Repositories;
using Microsoft.IdentityModel.Tokens;

namespace IndianokApi.Services;

public class AuthService : IAuthService
{
    private static readonly string _notFound = "Not found";
    private static readonly string _accessDenied = "Access denied";

    private readonly ILogger<AuthService> _logger;
    private readonly IUsersRepository _usersRepository;
    private readonly IHttpContextAccessor _httpContextAccessor;
    private readonly IConfiguration _configuration;

    public AuthService(
        ILogger<AuthService> logger,
        IUsersRepository usersRepository,
        IHttpContextAccessor httpContextAccessor,
        IConfiguration configuration)
    {
        _logger = logger;
        _usersRepository = usersRepository;
        _httpContextAccessor = httpContextAccessor;
        _configuration = configuration;
    }

    // TODO: Check for password strength.
    public async Task<AuthToken?> CreateUserWithEmailAndPasswordAsync(AuthCredential authCredential, CancellationToken cancellationToken)
    {
        _logger.LogDebug("Creating user with email and password");
        var userEntity = await _usersRepository.GetUserAsync(authCredential.Email, cancellationToken);
        if (userEntity is not null)
        {
            _logger.LogInformation("Account with such email already exsits");
            return null;
        }
        var (passwordSalt, passwordHash) = CreatePasswordSaltAndHash(authCredential.Password);
        userEntity = new UserEntity
        {
            Email = authCredential.Email,
            PasswordSalt = passwordSalt,
            PasswordHash = passwordHash,
            IsAdmin = false,
            Parent = new ParentEntity
            {
                Email = authCredential.Email,
                Name = NameFromEmail(authCredential.Email),
            }
        };
        _usersRepository.AddUser(userEntity);
        await _usersRepository.SaveChangesAsync(cancellationToken);
        var accessToken = CreateAccessToken(userEntity);
        return accessToken?.let(it => new AuthToken { AccessToken = it });
    }

    public async Task<AuthToken?> SignInWithEmailAndPasswordAsync(AuthCredential authCredential, CancellationToken cancellationToken)
    {
        _logger.LogDebug("Signing in with email and password");
        var userEntity = await _usersRepository.GetUserAsync(authCredential.Email, cancellationToken);
        if (userEntity is null)
        {
            _logger.LogInformation(_notFound);
            return null;
        }
        var passwordIsCorrect = VerifyPasswordHash(authCredential.Password, userEntity.PasswordSalt, userEntity.PasswordHash);
        if (!passwordIsCorrect)
        {
            _logger.LogWarning(_accessDenied);
            return null;
        }
        var accessToken = CreateAccessToken(userEntity);
        return accessToken?.let(it => new AuthToken { AccessToken = it });
    }

    // TODO: Check for password strength.
    public async Task<bool> UpdatePasswordAsync(AuthCredential authCredential, CancellationToken cancellationToken)
    {
        _logger.LogDebug("Updating password");
        var userEntity = await _usersRepository.GetUserAsync(authCredential.Email, cancellationToken);
        if (userEntity is null)
        {
            _logger.LogInformation(_notFound);
            return false;
        }
        if (authCredential.Email != GetEmail())
        {
            _logger.LogWarning(_accessDenied);
            throw new UnauthorizedAccessException();
        }
        var (passwordSalt, passwordHash) = CreatePasswordSaltAndHash(authCredential.Password);
        userEntity.PasswordSalt = passwordSalt;
        userEntity.PasswordHash = passwordHash;
        _usersRepository.UpdateUser(userEntity);
        await _usersRepository.SaveChangesAsync(cancellationToken);
        return true;
    }

    // TODO: Delete the corresponding parent
    public async Task<bool> DeleteAsync(AuthEmail authEmail, CancellationToken cancellationToken)
    {
        _logger.LogDebug("Deleting user");
        var userEntity = await _usersRepository.GetUserAsync(authEmail.Email, cancellationToken);
        if (userEntity is null)
        {
            _logger.LogInformation(_notFound);
            return false;
        }
        if (authEmail.Email != GetEmail())
        {
            _logger.LogWarning(_accessDenied);
            throw new UnauthorizedAccessException();
        }
        _usersRepository.RemoveUser(userEntity);
        await _usersRepository.SaveChangesAsync(cancellationToken);
        return true;
    }

    private (byte[] passwordSalt, byte[] passwordHash) CreatePasswordSaltAndHash(string password)
    {
        using (var hmac = new HMACSHA512())
        {
            byte[] passwordSalt = hmac.Key;
            byte[] passwordHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));
            return (passwordSalt, passwordHash);
        }
    }

    private string NameFromEmail(string email)
    {
        var names = email.Split('@')[0].Split('.').Select(name => Char.ToUpper(name[0]) + name.Substring(1));
        return string.Join(' ', names);
    }

    private bool VerifyPasswordHash(string password, byte[] passwordSalt, byte[] passwordHash)
    {
        using (var hmac = new HMACSHA512(passwordSalt))
        {
            var computedHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));
            return computedHash.SequenceEqual(passwordHash);
        }
    }

    private string CreateAccessToken(UserEntity userEntity)
    {
        List<Claim> claims = new List<Claim>
        {
            new Claim(ClaimTypes.Email, userEntity.Email),
            new Claim(ClaimTypes.Role, userEntity.IsAdmin ? "admin" : "parent"),
            new Claim(ClaimTypes.NameIdentifier, userEntity.Parent.Id.ToString(), ClaimValueTypes.Integer),
            new Claim(ClaimTypes.Name, userEntity.Parent.Name),
        };
        var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration.GetSection("AppSettings:Token").Value));
        var signingCredentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);
        var accessToken = new JwtSecurityToken(
            claims: claims,
            expires: DateTime.Now.AddDays(1),
            signingCredentials: signingCredentials);
        var jwt = new JwtSecurityTokenHandler().WriteToken(accessToken);
        return jwt;
    }

    private string GetEmail()
    {
        var email = _httpContextAccessor.HttpContext!.User.FindFirstValue(ClaimTypes.Email);
        return email;
    }
}
