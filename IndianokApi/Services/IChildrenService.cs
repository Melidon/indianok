using IndianokApi.Dtos;
using IndianokApi.Models;

namespace IndianokApi.Services;

public interface IChildrenService
{
    // Read
    Task<Document<Child>?> GetChildAsync(Guid parentId, Guid id, CancellationToken cancellationToken);
    Task<IEnumerable<Document<Child>>> ListChildrenAsync(Guid parentId, CancellationToken cancellationToken);

    // Write
    Task<Document<Child>> CreateChildAsync(Guid parentId, Child child, CancellationToken cancellationToken);
    Task<bool> UpdateChildAsync(Guid parentId, Guid id, Child child, CancellationToken cancellationToken);
    Task<bool> DeleteChildAsync(Guid parentId, Guid id, CancellationToken cancellationToken);
}
