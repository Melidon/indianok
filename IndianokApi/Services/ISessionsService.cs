using IndianokApi.Dtos;
using IndianokApi.Models;

namespace IndianokApi.Services;

public interface ISessionsService
{
    // Read
    Task<Document<Session>?> GetSessionAsync(Guid id, CancellationToken cancellationToken);
    Task<IEnumerable<Document<Session>>> ListSessionsAsync(CancellationToken cancellationToken);

    // Write
    Task<Document<Session>> CreateSessionAsync(Session session, CancellationToken cancellationToken);
    Task<bool> UpdateSessionAsync(Guid id, Session session, CancellationToken cancellationToken);
    Task<bool> DeleteSessionAsync(Guid id, CancellationToken cancellationToken);
}
