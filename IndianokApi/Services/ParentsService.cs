using System.Security.Claims;
using AutoMapper;
using IndianokApi.Models;
using IndianokApi.Entities;
using IndianokApi.Repositories;
using IndianokApi.Dtos;

namespace IndianokApi.Services;

public class ParentsService : IParentsService
{
    private static readonly string _notFound = "Not found";
    private static readonly string _accessDenied = "Access denied";

    private readonly IMapper _mapper;
    private readonly ILogger<ParentsService> _logger;
    private readonly IHttpContextAccessor _httpContextAccessor;
    private readonly IParentsRepository _parentsRepository;

    public ParentsService(
        IMapper mapper,
        ILogger<ParentsService> logger,
        IHttpContextAccessor httpContextAccessor,
        IParentsRepository parentsRepository)
    {
        _mapper = mapper;
        _logger = logger;
        _httpContextAccessor = httpContextAccessor;
        _parentsRepository = parentsRepository;
    }

    public async Task<Document<Parent>?> GetParentAsync(Guid id, CancellationToken cancellationToken)
    {
        _logger.LogDebug("Getting parent");
        var parentEntity = await _parentsRepository.GetParentAsync(id, cancellationToken);
        if (parentEntity is null)
        {
            _logger.LogInformation(_notFound);
            return null;
        }
        var isAdmin = IsAdmin();
        var ownsParent = OwnsParent(parentEntity);
        var accessGranted = isAdmin || ownsParent;
        if (!accessGranted)
        {
            _logger.LogWarning(_accessDenied);
            throw new UnauthorizedAccessException();
        }
        var parentDocument = _mapper.Map<Document<Parent>>(parentEntity);
        return parentDocument;
    }

    public async Task<bool> UpdateParentAsync(Guid id, Parent parent, CancellationToken cancellationToken)
    {
        _logger.LogDebug("Updating parent");
        var parentEntity = await _parentsRepository.GetParentAsync(id, cancellationToken);
        if (parentEntity is null)
        {
            _logger.LogInformation(_notFound);
            return false;
        }
        var ownsParent = OwnsParent(parentEntity);
        if (!ownsParent)
        {
            _logger.LogWarning(_accessDenied);
            throw new UnauthorizedAccessException();
        }
        _mapper.Map(parent, parentEntity);
        _parentsRepository.UpdateParent(parentEntity);
        await _parentsRepository.SaveChangesAsync(cancellationToken);
        return true;
    }

    private bool IsAdmin()
    {
        var isAdmin = _httpContextAccessor.HttpContext?.User.FindFirstValue(ClaimTypes.Role) == "admin";
        return isAdmin;
    }

    private bool OwnsParent(ParentEntity parentEntity)
    {
        var userId = GetUserId();
        if (userId is null)
        {
            return false;
        }
        var ownsParent = parentEntity.Id == userId;
        return ownsParent;
    }

    private Guid? GetUserId()
    {
        var userId = _httpContextAccessor.HttpContext?.User.FindFirstValue(ClaimTypes.NameIdentifier)?.let(it => Guid.Parse(it));
        return userId;
    }
}
