using IndianokApi.Dtos;
using IndianokApi.Models;

namespace IndianokApi.Services;

public interface ILocationsService
{
    // Read
    Task<Document<Location>?> GetLocationAsync(Guid id, CancellationToken cancellationToken);
    Task<IEnumerable<Document<Location>>> ListLocationsAsync(CancellationToken cancellationToken);

    // Write
    Task<Document<Location>> CreateLocationAsync(Location location, CancellationToken cancellationToken);
    Task<bool> UpdateLocationAsync(Guid id, Location location, CancellationToken cancellationToken);
    Task<bool> DeleteLocationAsync(Guid id, CancellationToken cancellationToken);
}
