using IndianokApi.Dtos;
using IndianokApi.Models;

namespace IndianokApi.Services;

public interface IBookingsService
{
    // Read
    Task<Document<Booking>?> GetBookingAsync(Guid sessionId, Guid id, CancellationToken cancellationToken);
    Task<IEnumerable<Document<Booking>>> ListBookingsAsync(Guid sessionId, CancellationToken cancellationToken);
    Task<IEnumerable<Document<Booking>>> ListBookingsForParentAsync(Guid parentId, CancellationToken cancellationToken);

    // Write
    Task<Document<Booking>> CreateBookingAsync(Guid sessionId, Booking booking, CancellationToken cancellationToken);
    Task<bool> UpdateBookingAsync(Guid sessionId, Guid id, Booking booking, CancellationToken cancellationToken);
    Task<bool> DeleteBookingAsync(Guid sessionId, Guid id, CancellationToken cancellationToken);
}
