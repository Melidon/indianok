using IndianokApi.Models;

namespace IndianokApi.Services;

public interface IAuthService
{
    Task<AuthToken?> CreateUserWithEmailAndPasswordAsync(AuthCredential authCredential, CancellationToken cancellationToken);
    Task<AuthToken?> SignInWithEmailAndPasswordAsync(AuthCredential authCredential, CancellationToken cancellationToken);
    Task<bool> UpdatePasswordAsync(AuthCredential authCredential, CancellationToken cancellationToken);
    Task<bool> DeleteAsync(AuthEmail authEmail, CancellationToken cancellationToken);
}
