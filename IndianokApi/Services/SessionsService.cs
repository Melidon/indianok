using System.Security.Claims;
using AutoMapper;
using IndianokApi.Models;
using IndianokApi.Entities;
using IndianokApi.Repositories;
using IndianokApi.Dtos;

namespace IndianokApi.Services;

public class SessionsService : ISessionsService
{
    private static readonly string _notFound = "Not found";
    private static readonly string _accessDenied = "Access denied";

    private readonly IMapper _mapper;
    private readonly ILogger<SessionsService> _logger;
    private readonly IHttpContextAccessor _httpContextAccessor;
    private readonly ISessionsRepository _sessionsRepository;

    public SessionsService(
        IMapper mapper,
        ILogger<SessionsService> logger,
        IHttpContextAccessor httpContextAccessor,
        ISessionsRepository sessionsRepository)
    {
        _mapper = mapper;
        _logger = logger;
        _httpContextAccessor = httpContextAccessor;
        _sessionsRepository = sessionsRepository;
    }

    public async Task<Document<Session>?> GetSessionAsync(Guid id, CancellationToken cancellationToken)
    {
        _logger.LogDebug("Getting session");
        var sessionEntity = await _sessionsRepository.GetSessionAsync(id, cancellationToken);
        if (sessionEntity is null)
        {
            _logger.LogInformation(_notFound);
            return null;
        }
        var sessionDocument = _mapper.Map<Document<Session>>(sessionEntity);
        return sessionDocument;
    }

    public async Task<IEnumerable<Document<Session>>> ListSessionsAsync(CancellationToken cancellationToken)
    {
        _logger.LogDebug("Listing sessions");
        var sessionEntities = await _sessionsRepository.ListSessionsAsync(cancellationToken);
        var sessionDocuments = _mapper.Map<IEnumerable<Document<Session>>>(sessionEntities);
        return sessionDocuments;
    }

    public async Task<Document<Session>> CreateSessionAsync(Session session, CancellationToken cancellationToken)
    {
        _logger.LogDebug("Creating session");
        var isAdmin = IsAdmin();
        if (!isAdmin)
        {
            _logger.LogWarning(_accessDenied);
            throw new UnauthorizedAccessException();
        }
        var sessionEntity = _mapper.Map<SessionEntity>(session);
        _sessionsRepository.AddSession(sessionEntity);
        await _sessionsRepository.SaveChangesAsync(cancellationToken);
        var sessionDocument = _mapper.Map<Document<Session>>(sessionEntity);
        return sessionDocument;
    }

    public async Task<bool> UpdateSessionAsync(Guid id, Session session, CancellationToken cancellationToken)
    {
        _logger.LogDebug("Updating session");
        var sessionEntity = await _sessionsRepository.GetSessionAsync(id, cancellationToken);
        if (sessionEntity is null)
        {
            _logger.LogInformation(_notFound);
            return false;
        }
        var isAdmin = IsAdmin();
        if (!isAdmin)
        {
            _logger.LogWarning(_accessDenied);
            throw new UnauthorizedAccessException();
        }
        _mapper.Map(session, sessionEntity);
        _sessionsRepository.UpdateSession(sessionEntity);
        await _sessionsRepository.SaveChangesAsync(cancellationToken);
        return true;
    }

    public async Task<bool> DeleteSessionAsync(Guid id, CancellationToken cancellationToken)
    {
        _logger.LogDebug("Deleting session");
        var sessionEntity = await _sessionsRepository.GetSessionAsync(id, cancellationToken);
        if (sessionEntity is null)
        {
            _logger.LogInformation(_notFound);
            return false;
        }
        var isAdmin = IsAdmin();
        if (!isAdmin)
        {
            _logger.LogWarning(_accessDenied);
            throw new UnauthorizedAccessException();
        }
        _sessionsRepository.RemoveSession(sessionEntity);
        await _sessionsRepository.SaveChangesAsync(cancellationToken);
        return true;
    }

    private bool IsAdmin()
    {
        var isAdmin = _httpContextAccessor.HttpContext?.User.FindFirstValue(ClaimTypes.Role) == "admin";
        return isAdmin;
    }
}
