using System.Security.Claims;
using AutoMapper;
using IndianokApi.Models;
using IndianokApi.Entities;
using IndianokApi.Repositories;
using IndianokApi.Dtos;

namespace IndianokApi.Services;

public class BookingsService : IBookingsService
{
    private static readonly string _notFound = "Not found";
    private static readonly string _accessDenied = "Access denied";

    private readonly IMapper _mapper;
    private readonly ILogger<BookingsService> _logger;
    private readonly IHttpContextAccessor _httpContextAccessor;
    private readonly IBookingsRepository _bookingsRepository;
    private readonly IChildrenRepository _childrenRepository;

    public BookingsService(
        IMapper mapper,
        ILogger<BookingsService> logger,
        IHttpContextAccessor httpContextAccessor,
        IBookingsRepository bookingsRepository,
        IChildrenRepository childrenRepository)
    {
        _mapper = mapper;
        _logger = logger;
        _httpContextAccessor = httpContextAccessor;
        _bookingsRepository = bookingsRepository;
        _childrenRepository = childrenRepository;
    }

    public async Task<Document<Booking>?> GetBookingAsync(Guid sessionId, Guid id, CancellationToken cancellationToken)
    {
        _logger.LogDebug("Getting booking");
        var bookingEntity = await _bookingsRepository.GetBookingAsync(sessionId, id, cancellationToken);
        if (bookingEntity is null)
        {
            _logger.LogInformation(_notFound);
            return null;
        }
        var isAdmin = IsAdmin();
        var ownsBooking = OwnsBookingAsync(bookingEntity, cancellationToken);
        var accessGranted = isAdmin || ownsBooking;
        if (!accessGranted)
        {
            _logger.LogWarning(_accessDenied);
            throw new UnauthorizedAccessException();
        }
        var bookingDocument = _mapper.Map<Document<Booking>>(bookingEntity);
        return bookingDocument;
    }

    public async Task<IEnumerable<Document<Booking>>> ListBookingsAsync(Guid sessionId, CancellationToken cancellationToken)
    {
        _logger.LogDebug("Listing bookings");
        var isAdmin = IsAdmin();
        if (!isAdmin)
        {
            _logger.LogWarning(_accessDenied);
            throw new UnauthorizedAccessException();
        }
        var bookingEntities = await _bookingsRepository.ListBookingsAsync(sessionId, cancellationToken);
        var bookingDocuments = _mapper.Map<IEnumerable<Document<Booking>>>(bookingEntities);
        return bookingDocuments;
    }

    public async Task<IEnumerable<Document<Booking>>> ListBookingsForParentAsync(Guid parentId, CancellationToken cancellationToken)
    {
        _logger.LogDebug("Listing bookings for parent");
        // TODO: Check parentId
        var bookingEntities = await _bookingsRepository.ListBookingsForParentAsync(parentId, cancellationToken);
        var bookingDocuments = _mapper.Map<IEnumerable<Document<Booking>>>(bookingEntities);
        return bookingDocuments;
    }

    // TODO: Only allow booking for furure sessions
    public async Task<Document<Booking>> CreateBookingAsync(Guid sessionId, Booking booking, CancellationToken cancellationToken)
    {
        _logger.LogDebug("Creating booking");
        // TODO: Check booking.PaymentStatus
        // TODO: Check booking.ParentId
        var childEntity = await _childrenRepository.GetChildAsync(booking.ParentId, booking.ChildId, cancellationToken);
        var ownsChild = childEntity is not null && OwnsChild(childEntity);
        if (!ownsChild)
        {
            _logger.LogWarning(_accessDenied);
            throw new UnauthorizedAccessException();
        }
        var bookingEntity = _mapper.Map<BookingEntity>(booking);
        bookingEntity.SessionId = sessionId;
        _bookingsRepository.AddBooking(bookingEntity);
        await _bookingsRepository.SaveChangesAsync(cancellationToken);
        var bookingDocument = _mapper.Map<Document<Booking>>(bookingEntity);
        return bookingDocument;
    }

    public async Task<bool> UpdateBookingAsync(Guid sessionId, Guid id, Booking booking, CancellationToken cancellationToken)
    {
        _logger.LogDebug("Updating booking");
        var bookingEntity = await _bookingsRepository.GetBookingAsync(sessionId, id, cancellationToken);
        if (bookingEntity is null)
        {
            _logger.LogInformation(_notFound);
            return false;
        }
        // TODO: Same checks as CreateBookingAsync
        var isAdmin = IsAdmin(); // TODO: Admins can only change PaymentStatus
        if (!isAdmin)
        {
            _logger.LogWarning(_accessDenied);
            throw new UnauthorizedAccessException();
        }
        _mapper.Map(booking, bookingEntity);
        _bookingsRepository.UpdateBooking(bookingEntity);
        await _bookingsRepository.SaveChangesAsync(cancellationToken);
        return true;
    }

    // TODO: Only allow delete if payment status is NotPaid
    public async Task<bool> DeleteBookingAsync(Guid sessionId, Guid id, CancellationToken cancellationToken)
    {
        _logger.LogDebug("Deleting booking");
        var bookingEntity = await _bookingsRepository.GetBookingAsync(sessionId, id, cancellationToken);
        if (bookingEntity is null)
        {
            _logger.LogInformation(_notFound);
            return false;
        }
        var ownsBooking = OwnsBookingAsync(bookingEntity, cancellationToken);
        if (!ownsBooking)
        {
            _logger.LogWarning(_accessDenied);
            throw new UnauthorizedAccessException();
        }
        _bookingsRepository.RemoveBooking(bookingEntity);
        await _bookingsRepository.SaveChangesAsync(cancellationToken);
        return true;
    }

    private bool IsAdmin()
    {
        var isAdmin = _httpContextAccessor.HttpContext?.User.FindFirstValue(ClaimTypes.Role) == "admin";
        return isAdmin;
    }

    private bool OwnsBookingAsync(BookingEntity bookingEntity, CancellationToken cancellationToken)
    {
        var ownsBooking = OwnsChild(bookingEntity.Child);
        return ownsBooking;
    }

    private bool OwnsChild(ChildEntity childEntity)
    {
        var userId = GetUserId();
        if (userId is null)
        {
            return false;
        }
        var ownsChild = childEntity.ParentId == userId;
        return ownsChild;
    }

    private Guid? GetUserId()
    {
        var userId = _httpContextAccessor.HttpContext?.User.FindFirstValue(ClaimTypes.NameIdentifier)?.let(it => Guid.Parse(it));
        return userId;
    }
}
