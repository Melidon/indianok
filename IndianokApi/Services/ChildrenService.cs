using System.Security.Claims;
using AutoMapper;
using IndianokApi.Models;
using IndianokApi.Entities;
using IndianokApi.Repositories;
using IndianokApi.Dtos;

namespace IndianokApi.Services;

public class ChildrenService : IChildrenService
{
    private static readonly string _notFound = "Not found";
    private static readonly string _accessDenied = "Access denied";

    private readonly IMapper _mapper;
    private readonly ILogger<ChildrenService> _logger;
    private readonly IHttpContextAccessor _httpContextAccessor;
    private readonly IChildrenRepository _childrenRepository;

    public ChildrenService(
        IMapper mapper,
        ILogger<ChildrenService> logger,
        IHttpContextAccessor httpContextAccessor,
        IChildrenRepository childrenRepository)
    {
        _mapper = mapper;
        _logger = logger;
        _httpContextAccessor = httpContextAccessor;
        _childrenRepository = childrenRepository;
    }

    public async Task<Document<Child>?> GetChildAsync(Guid parentId, Guid id, CancellationToken cancellationToken)
    {
        _logger.LogDebug("Getting child");
        var childEntity = await _childrenRepository.GetChildAsync(parentId, id, cancellationToken);
        if (childEntity is null)
        {
            _logger.LogInformation(_notFound);
            return null;
        }
        var isAdmin = IsAdmin();
        var ownsChild = OwnsChild(childEntity);
        var accessGranted = isAdmin || ownsChild;
        if (!accessGranted)
        {
            _logger.LogWarning(_accessDenied);
            throw new UnauthorizedAccessException();
        }
        var childDocument = _mapper.Map<Document<Child>>(childEntity);
        return childDocument;
    }

    public async Task<IEnumerable<Document<Child>>> ListChildrenAsync(Guid parentId, CancellationToken cancellationToken)
    {
        _logger.LogDebug("Listing children");
        var isAdmin = IsAdmin();
        if (!isAdmin)
        {
            _logger.LogWarning(_accessDenied);
            throw new UnauthorizedAccessException();
        }
        var childEntities = await _childrenRepository.ListChildrenAsync(parentId, cancellationToken);
        var childDocuments = _mapper.Map<IEnumerable<Document<Child>>>(childEntities);
        return childDocuments;
    }

    public async Task<Document<Child>> CreateChildAsync(Guid parentId, Child child, CancellationToken cancellationToken)
    {
        _logger.LogDebug("Creating child");
        var childEntity = _mapper.Map<ChildEntity>(child);
        var ownsChild = OwnsChild(childEntity);
        if (!ownsChild)
        {
            _logger.LogWarning(_accessDenied);
            throw new UnauthorizedAccessException();
        }
        _childrenRepository.AddChild(childEntity);
        await _childrenRepository.SaveChangesAsync(cancellationToken);
        var childDocument = _mapper.Map<Document<Child>>(childEntity);
        return childDocument;
    }

    public async Task<bool> UpdateChildAsync(Guid parentId, Guid id, Child child, CancellationToken cancellationToken)
    {
        _logger.LogDebug("Updating child");
        var childEntity = await _childrenRepository.GetChildAsync(parentId, id, cancellationToken);
        if (childEntity is null)
        {
            _logger.LogInformation(_notFound);
            return false;
        }
        var ownsChild = OwnsChild(childEntity);
        if (!ownsChild)
        {
            _logger.LogWarning(_accessDenied);
            throw new UnauthorizedAccessException();
        }
        _mapper.Map(child, childEntity);
        _childrenRepository.UpdateChild(childEntity);
        await _childrenRepository.SaveChangesAsync(cancellationToken);
        return true;
    }

    public async Task<bool> DeleteChildAsync(Guid parentId, Guid id, CancellationToken cancellationToken)
    {
        _logger.LogDebug("Deleting child");
        var childEntity = await _childrenRepository.GetChildAsync(parentId, id, cancellationToken);
        if (childEntity is null)
        {
            _logger.LogInformation(_notFound);
            return false;
        }
        var ownsChild = OwnsChild(childEntity);
        if (!ownsChild)
        {
            _logger.LogWarning(_accessDenied);
            throw new UnauthorizedAccessException();
        }
        _childrenRepository.RemoveChild(childEntity);
        await _childrenRepository.SaveChangesAsync(cancellationToken);
        return true;
    }

    private bool IsAdmin()
    {
        var isAdmin = _httpContextAccessor.HttpContext?.User.FindFirstValue(ClaimTypes.Role) == "admin";
        return isAdmin;
    }

    private bool OwnsChild(ChildEntity childEntity)
    {
        var userId = GetUserId();
        if (userId is null)
        {
            return false;
        }
        var ownsChild = childEntity.ParentId == userId;
        return ownsChild;
    }

    private Guid? GetUserId()
    {
        var userId = _httpContextAccessor.HttpContext?.User.FindFirstValue(ClaimTypes.NameIdentifier)?.let(it => Guid.Parse(it));
        return userId;
    }
}
