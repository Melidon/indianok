import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import { Parent, Roles } from "./models";
admin.initializeApp();

const db = admin.firestore();

const region = "europe-west1";

const users = "users";
const parents = "parents";

function nameFromEmail(email: string | undefined): string | undefined {
  if (!email) {
    return undefined;
  }
  const names = email
    .split("@")[0]
    .split(".")
    .map((name) => name[0].toUpperCase() + name.substring(1));
  return names.join(" ");
}

export const onAccountCreate = functions
  .region(region)
  .auth.user()
  .onCreate(async (user) => {
    const userDocumentCreatePromise = db
      .collection(users)
      .doc(user.uid)
      .create({ role: Roles.Parent });
    const parent: Parent = {
      email: user.email ?? "",
      name: user.displayName ?? nameFromEmail(user.email) ?? "",
      address: "",
      phone: user.phoneNumber ?? "",
    };
    const parentDocumentCreatePromise = db
      .collection(parents)
      .doc(user.uid)
      .create(parent);
    await Promise.all([userDocumentCreatePromise, parentDocumentCreatePromise]);
  });

export const onAccountDelete = functions
  .region(region)
  .auth.user()
  .onDelete(async (user) => {
    const userDocumentDeletePromise = db
      .collection(users)
      .doc(user.uid)
      .delete();
    const parentDocumentDeletePromise = db
      .collection(parents)
      .doc(user.uid)
      .delete();
    await Promise.all([userDocumentDeletePromise, parentDocumentDeletePromise]);
  });

export const onUserWrite = functions
  .region(region)
  .firestore.document(`${users}/{userId}`)
  .onWrite((change, context) => {
    const uid = context.params.userId;
    const data = change.after.data();
    return admin.auth().setCustomUserClaims(uid, data ?? null);
  });
