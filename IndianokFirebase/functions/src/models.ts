export interface Parent {
  email: string;
  name: string;
  address: string;
  phone: string;
}

export enum Roles {
  Parent = "Parent",
  Admin = "Admin",
}
